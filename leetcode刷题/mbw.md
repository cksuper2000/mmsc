## 2021.8.11
 - leetcode刷题
  - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
  - [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
## 2021.8.12
 - leetcode刷题
  - [最大子序和](https://leetcode-cn.com/problems/maximum-subarray/)
  - [搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
  - [在排序数组中查找元素的第一个和最后一个位置](https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/)
## 2021.8.13
 - leetcode刷题
  - [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)
  - [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
## 2021.8.15
 - leetcode刷题
  - [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
  - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
## 2021.8.16
 - leetcode刷题
  - [最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
  - [实现 strStr](https://leetcode-cn.com/problems/implement-strstr/)
## 2021.8.17
 - leetcode刷题
  - [69. x 的平方根](https://leetcode-cn.com/problems/sqrtx/)
  - [剑指 Offer II 072. 求平方根](https://leetcode-cn.com/problems/jJ0w9p/)
## 2021.8.18
 - leetcode刷题
  - [一维数组的动态和](https://leetcode-cn.com/problems/running-sum-of-1d-array/)
  - [剑指 Offer 16. 数值的整数次方](https://leetcode-cn.com/problems/shu-zhi-de-zheng-shu-ci-fang-lcof/)
## 2021.8.19
 - leetcode刷题
  - [丢失的数字](https://leetcode-cn.com/problems/missing-number/)
  - [Nim 游戏](https://leetcode-cn.com/problems/nim-game/)
## 2021.8.20
 - leetcode刷题
  - [有效的括号](https://leetcode-cn.com/problems/valid-parentheses/)
## 2021.8.22
 - leetcode刷题
  - [赎金信](https://leetcode-cn.com/problems/ransom-note/)
## 2021.8.23
 - leetcode刷题
  - [分发饼干](https://leetcode-cn.com/problems/assign-cookies/)
## 2021.8.24
 - leetcode刷题
  - [有效的完全平方数](https://leetcode-cn.com/problems/valid-perfect-square/)
## 2021.8.25
 - leetcode刷题
  - [剑指 Offer 11. 旋转数组的最小数字](https://leetcode-cn.com/problems/xuan-zhuan-shu-zu-de-zui-xiao-shu-zi-lcof/)
## 2021.8.26
 - leetcode刷题
  - [各位相加](https://leetcode-cn.com/problems/add-digits/)
## 2021.8.30
 - leetcode刷题
  - [剑指 Offer 03. 数组中重复的数字](https://leetcode-cn.com/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof/)
## 2021.8.31
 - leetcode刷题
  - [剑指 Offer 65. 不用加减乘除做加法](https://leetcode-cn.com/problems/bu-yong-jia-jian-cheng-chu-zuo-jia-fa-lcof/)
## 2021.9.1
 - leetcode刷题
  - [剑指 Offer 50. 第一个只出现一次的字符](https://leetcode-cn.com/problems/di-yi-ge-zhi-chu-xian-yi-ci-de-zi-fu-lcof/)
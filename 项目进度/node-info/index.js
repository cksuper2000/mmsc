const http = require("http");
const nodemailer = require("nodemailer");
const COS = require('cos-nodejs-sdk-v5');
const fs = require('fs');
const AlipaySdk = require('alipay-sdk').default;
const AlipayFormData =  require('alipay-sdk/lib/form').default;

// 普通公钥模式
const alipaySdk = new AlipaySdk({
  // 参考下方 SDK 配置
  appId: '2021000118615550',
  privateKey:"MIIEowIBAAKCAQEAvuwglN0YKVOGau2qVk9gQW+BAQCI4Eoos8H8LY9izpwsuteESlKrRWPmV/+7c4gsW4sgg9ZpLL92NDWtn5l/xd9bi4D+GqdmHr8ssyl4GiXmI7MjuYcSbsyhziPP+eLBU+EDi4JbWg+hZlixeAM07r/q5TZy8oOaiysjmKXcLTkxOR7CJ9+pNiGSg7vfIKIN7VE/HTxlTyxAGpxRNkb7UqncpP87jcZe1ImLxkviL6x+AqZRLpNbUWjiOLLOZKBz8pVHIsxjtiraVK6JiROAEyrjV41ukgfPZfeJPHmSG8A8gytsDMqV6zR+0wefHtNzKq+ar0jdIwRE5sCvenuvYQIDAQABAoIBAE34x1GkffKavwG6cF9xaWEv9aKAImZfQwIuzhuZPIQeuhHQZKGp43KiWvdDZL3JNjxotG3DSg7RXqfx6CePG+Sm1I9DrgMarJUQBGRjIj6jjbq0tXlWlYuUnNxAOfbWyZfFBJqcMUy1p3fR7yCRRrPew0K7GvS6FIqToqT7z9DTnnpENOmtwyAXOTDV2tKQlrHV05AMvXUfIN92JlMjQo4IPJ7Nt/Q28A4N01v99eE38yVx+3SlbVKU0mtjkNENA8pmsCHyNwNYe9pmV87gZCi0gyV0DzybHAdga+Wzxl9oR2mvjQ/7aeQip6kaEpht3fO6xw7FUItqH9YTsZ/REXECgYEA8Bbts1axs7p0ahi8KhnMitYNuoYh5R064zl44huJD4QVXBpaFN0ew6j5IVHTH3wVI2XxSU+7Abnih5oijwWVyLitDeXYImLX9bRgzBo36RDly2hAjWd40GGuqI7vp67hk9gRmsLBpxm9vdc9sNSzzeUPYXdZ0VxOdPVJlq10GLUCgYEAy5MWUCyLgPxxafR2dN893tAFSTatt1swnUrRo3MRCqQOqoo4lY40Hncd+ErGqC1ECqqVl6kfA7XJR0SWk9Jpvmi4zl2Eyce2Ve5cC/xu2R6OUo6s6ZOr8UllJfuUm756t/3MP/7wCX+h5Im47XjEMY1at/mAT+YYYk0BEirrg30CgYBH3YUfNmcQtlIcY/UG4RwRvsrzNy00KJC07UIa5EUdiNrf699k+V85D/4qlBSPCxREuUIFQMFoXdzJ3EBV8UUJhQX/7/nHKTEdkfWbgomT0QQH49fWu881CNhACjEAT1+kYnu6iGtw+kqKZcBIixKbIeJ9uT0AZxHw/m8dlCP5NQKBgH4PpEabfvut13FcT6pJv59lQ5C1ylb7ohwVA8Jqc8gaFXbnxuhducqPysBBVck41qSgQ6g5sxnyDSUTxQbeylp9sMshwCPPhwqpYtiNRfsvX8Sh01OE/XmaEIe3xau4BXhAMqYCcZQANDk4S4rKOPvSY7X/S8SuYLUmqB4e895lAoGBAOrfsoYwo05TJcB2kro+Bd0eaz0dMNbl7+iTLJdqkNehPqbspeOLQcFK6Ay+/SUQHu6tC/PsEGXqcVBZbyLYac/K6joYVxEve0RLU6tOqVKziTo5t7fRRC0pctnqpdB3VGneLmLvBo6IDE834RhB2pidrCkp3ah52U4ZVeiw9Mmb",
  //可设置AES密钥，调用AES加解密相关接口时需要（可选）
  encryptKey: 'NRTfpp+uGNoWgHHqYLnTcw=='
});
let app = http.createServer(async (req,res)=>{
  //支付宝拉取支付页面
  if(req.url==='/pay'&&req.method==="POST"){
    const formData = new AlipayFormData();
    formData.setMethod("get")
    formData.addField('notifyUrl', 'http://www.com/notify');
    formData.addField('bizContent', {
      outTradeNo: 'out_trade_no1',
      productCode: 'FAST_INSTANT_TRADE_PAY1',
      totalAmount: '999',
      subject: '商品1',
      body: '商品详情1',
    });
    let result = await alipaySdk.exec(
      'alipay.trade.page.pay',
      {},
      { formData: formData },
    )
    console.log(result);
    // result 为 form 表单
    res.end(result);
    
  }
  //发送邮件的
  if(req.url==='/email'){
    //首先获取收件人邮箱
    //创建发送对象
    let transporter = nodemailer.createTransport({
      host: "smtp.qq.com",
      port: 465,
      service:"qq",
      secureConnection:true,
      secure: false, // true for 465, false for other ports
      auth: {
        user: "1512620752", //用户名
        pass: "bhwsjwxyhzcjjhhf", // 授权码
      },
    });
    var mailOptions = {
      from:"1512620752@qq.com",//发件人邮件
      to:"941935892@qq.com",//收件人邮箱
      subject:"这是CK发送的第一封邮件",
      html:`
      <h1>程凯666</h1>
      `,
      text:"这是发送的邮件",
    }
    transporter.sendMail(mailOptions,(err,info)=>{
      if(err) {
      console.log(err);
      }else {
        console.log("邮件发送成功"+info.response);
      }
    });
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.setHeader("Access-Control-Allow-Origin","*");
  }
  //处理jsonp请求的
  if(req.url==="/jsonp"&&req.method==="GET"){
    res.end(`callback({
      name:"程凯",
      age:21,
      sex:true
    })`);
  }
  console.log(req.url);
  //处理jquery发送的请求
  if(req.url==='/jq'&&req.method==="GET"){
    console.log(req.query,"我已经答应了");
    res.setHeader('Access-Control-Allow-Origin',"*");
    res.end("我是返回的数据");
  }
  //实例化oss
  var cos = new COS({
    SecretId: 'AKIDsa3GDtJSKvFI4ZmjBSTEl23oKbbLq54n',
    SecretKey: 'UjiRvA8qDOJxLYVAYfW5NPBD0WlkSt4y'
  });
  //这个是查询桶的
  if(req.url==='/cos'){
    //查询
    let result =  await new Promise((resolve,rejects)=>{
      cos.getService(function (err, data) {
        if(err){
          rejects(err);
          return;
        }
        resolve(data);
      });
    })
    res.setHeader('content-type',"application/json; charset=utf-8");
    res.end(JSON.stringify(result));
  }
  //上传文件
  if(req.url==="/upload"&&req.method==="POST"){
    try {
      let rs=  fs.createReadStream('./data.json',{start:0});
      rs.on("open",()=>{

      })
      rs.on('data',(data)=>{
        console.log(data);
      })
      // console.log(ress);
    //   let result =  await new Promise((resolve,reject)=>{
    //     cos.putObject({
    //       Bucket: 'ck-1307380239', /* 必须 */
    //       Region: 'COS_REGION',    /* 必须 */
    //       Key: 'exampleobject',              /* 必须 */
    //       StorageClass: 'STANDARD',
    //       // Body: fs.createReadStream('./exampleobject'), // 上传文件对象
    //       onProgress: function(progressData) {
    //           console.log(JSON.stringify(progressData));
    //       }
    //    }, function(err, data) {
    //      if(err){
    //       reject(err);
    //       return;
    //      }
    //      resolve(data);
    //    });
    //   })
    //   console.log(result);
    } catch (error) {
      // console.log(error);
      res.end(error);
    }
  }
})

app.listen(3001,()=>{
  console.log("localhost 3001 is start");
})



export interface UserObject {
  id: string;
  name: string;
  avatar?: string;
  email?: any;
  role: string;
  status: string;
  createAt: string;
  updateAt: string;
};

export interface UserParams{
  name:string,
  email:string,
  role:string,
  status:string
}
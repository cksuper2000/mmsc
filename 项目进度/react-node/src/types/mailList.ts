export interface getmailis{
  id: string;
  from: string;
  to: string;
  subject: string;
  text: string;
  html: string;
  createAt: string;
}
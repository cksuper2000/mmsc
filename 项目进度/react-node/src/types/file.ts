export interface FileObject {
  id: string;
  originalname: string;
  filename: string;
  type: string;
  size: number;
  url: string;
  createAt: string;
};


export interface StateFile{
  File:State
};
export interface FileData{
  page:number,
  pageSize:number,
  originalname:string,
  type:string
}

interface State{
  fileList:Array<FileObject>,
  fileNum:number
}
export type IFile = (IFileDatum[] | number)[];
export interface IFileDatum {
  id: string;
  originalname: string;
  filename: string;
  type: string;
  size: number;
  url: string;
  createAt: string;
}
export interface ICommetObject {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: string;
  replyUserName?: string;
  replyUserEmail?: string;
  createAt: string;
  updateAt: string;
}
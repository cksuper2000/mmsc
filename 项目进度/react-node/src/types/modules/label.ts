export type Ilabel = Array<RootObject>
interface RootObject {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
export interface Idata {
  label:string,
  value:string
}
export type IList =  (Datum[] | number)[];
export interface Datum {
  id: string;
  title: string;
  cover?: string;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  totalAmount?: string;
  isPay: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: Array<{id:string,label:string,value:string}>;
  category?: any;
}
export interface ISerch {
  page: number
  pageSize: number
  title: string
  status: string
  category: string
}
export interface ITag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
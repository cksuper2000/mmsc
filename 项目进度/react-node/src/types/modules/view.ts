type IView = (ViewDatum[] | number)[];
export interface ViewDatum {
  id: string;
  ip: string;
  userAgent: string;
  url: string;
  count: number;
  address: string;
  browser: string;
  engine: string;
  os: string;
  device: string;
  createAt: string;
  updateAt: string;
}
//搜索参数
export interface IViewSerch{
  page: number
  pageSize: number
  ip: string
  userAgent: string
  url: string
  address: string
  browser: string
  engine: string
  os: string
  device: string
}

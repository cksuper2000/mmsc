//编辑器
export interface IEditorData {
  category: string
  content: string 
  cover: string
  html:string 
  isCommentable: boolean
  isRecommended: boolean
  password: string
  status: string
  summary: string
  tags:string
  title: string
  toc:string 
  totalAmount: string
}
export interface ISerchEditor {
  page: number
  pageSize: number
  originalname: string
  type: string
}
export interface IDetail {
  "id": string
  "title": string
  "cover": null|string,
  "summary": null|string,
  "content": string
  "html": string
  "toc": string  
  "status": string
  "views": number,
  "likes": number
  "isRecommended": boolean,
  "needPassword": boolean,
  "totalAmount": null|string
  "isPay": boolean,
  "isCommentable": boolean,
  "publishAt":string
  "createAt": string
  "updateAt": string
  "category": null|string
  "tags": Array<string>
}
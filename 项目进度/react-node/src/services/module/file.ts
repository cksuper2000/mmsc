import {request} from "umi"

//获取上传的图片信息
export function Img(page=1,pageSize=12,originalname?:string,type?:string){
    return request(`/api/file`,{
        params:{
            page,
            pageSize,
            originalname,
            type
        }
    })
};

//删除图片
export function Dele(id:string){
    return request(`/api/file/${id}`,{
        method:"delete"
    })
}
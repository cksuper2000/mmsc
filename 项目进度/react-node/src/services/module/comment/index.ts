import { request } from 'umi';

interface IOptions {
  id: string;
  page: number;
  pageSize: number;
  email: string;
  pass: string;
  name: string;
  status: boolean;
}
export type Options = Partial<IOptions>;

// 获取数据
export function CommentService(options: Options) {
  return request('/api/comment', {
    params: options,
  });
}

// 通过或拒绝接口
export function ChangeCommentStatus(options: Options) {
  return request(`/api/comment/${options.id}`, {
    method: 'PATCH',
    data: {
      pass: options.status,
    },
  });
}

// 删除接口
export function DeleteCommentData(options: Options) {
  return request(`/api/comment/${options.id}`, {
    method: 'DELETE',
  });
}

// 回复接口
export function ReplyCommentData(options: any) {
  return request('/api/comment', {
    method: 'POST',
    data: options,
  });
}

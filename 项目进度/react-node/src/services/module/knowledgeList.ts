import {request} from 'umi'

interface data{
    page:number;
    pageSize:number;
    title:string;
    status:string;
}

interface newkno{
    cover: string;
    isCommentable: boolean;
    summary: string;
    title: string;
}

interface sta{
    status:string;
}

interface essayList{
  title: string;
  content: string;
  html: string;
  toc: string;
  order: number;
  parentId: string;
}

export function getknowledgeList(obj:data){    
    return request('/api/knowledge',{
        params:obj
    })
}


export function delectknowledgeList(id:string){
    return request(`/api/knowledge/${id}`,{
        method:'DELETE'
    })
}

export function newknowledgeList(data:newkno){
    return request(`/api/knowledge/book`,{
        method:'POST',
        data
    })
}

export function setknowledgeList(id:string,data:newkno){
    return request(`/api/knowledge/${id}`,{
        method:'PATCH',
        data
    })
}

export function popupknowledgeList(id:string,data:sta){
    return request(`/api/knowledge/${id}`,{
        method:'PATCH',
        data
    })
}

export function getknowledgeListid(id:string){
    return request(`/api/knowledge/${id}`)
}

export function getessayid(data:essayList){
    return request('/api/knowledge/chapter',{
        method:'POST',
        data
    })
}
import { UserObject, UserParams } from "@/types/user"
import {request} from "umi"

//获取表格信息
export function table(page=1,pageSize=12,params:Partial<UserParams>){
    return request(`/api/user?page=${page}&pageSize=${pageSize}`,{
        params
    })
};


//用户操作
export function userSet(params:UserObject){
    return request(`/api/user/update`,{
        method:"post",
        data:{
            ...params
        }
    })
}
    

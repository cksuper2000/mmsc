import { IViewSerch } from "@/types";
import { request } from "umi";
//获取访问统计
export const getview = (page=1,pageSize=12)=>request('/api/view',{
  method:"GET",
  params:{
    page,
    pageSize
  }
})
//删除列列表
export const deleteData = (id:string)=>request('/api/view/'+id,{
  method:"DELETE",
})
//搜索功能
export const serchView = (params:Partial<IViewSerch>)=>request('/api/view',{
  method:"GET",
  params
})
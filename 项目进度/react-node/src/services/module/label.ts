import {request} from "umi";
import { Idata, ISerch } from '@/types'
export const getLabel = (url:string)=>request(url,{
  method:"GET",
})
export const addLabel = (data:Idata,token:string,url:string)=>request(url,{
  method:"POST",
  data,
  headers:{'authorization':token}
})
export const article = (page:number=1,pageSize:number=12)=>request('/api/article',{
  method:"GET",
  params:{
    page,
    pageSize
  }
})
//删除列表
export const removeList = (id:string)=>request('/api/article/'+id,{
  method:"DELETE"
});
//搜索
export const serchList = (serch:ISerch)=>request('/api/article',{
  method:'GET',
  params:serch
});
//修改状态
export const setArticleState = (id:string|number,data:any,type:string)=>request('/api/article/'+id,{
  method:type,
  data
})

//删除标签
export const removeLable = (id:string,type:string)=>request("/api/"+type+"/"+id,{
  method:"DELETE"
})
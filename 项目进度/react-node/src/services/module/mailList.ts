import {request} from 'umi'

interface mailList{
    page:number,
    pageSize:number,
    from:string,
    to:string,
    subject:string
}

export function getmailList(data:mailList){
    return request('/api/smtp',{
        params:data
    })
}

export function deledtmailList(id:string){
    return request(`/api/smtp/${id}`,{
        method:'DELETE'
    })
}
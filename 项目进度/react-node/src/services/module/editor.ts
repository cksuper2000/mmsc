import { IEditorData, ISerchEditor } from "@/types";
import { request } from "umi";
//发布文章接口
export const addArticle = (data:IEditorData)=>request('/api/article',{
  method:"POST",
  data
})
export const serchFile = (params:ISerchEditor)=>request('/api/file',{
  params
})
//获取详情数据
export const getDetail = (id:string)=>request('/api/article/'+id)
//删除详情的数据
export const deleteDetail = (id:string)=>request("/api/article/"+id,{
  method:"DELETE"
})
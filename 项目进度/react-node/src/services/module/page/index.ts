import { request } from 'umi';

interface IOptions {
  id: string;
  page: number;
  pageSize: number;
  path: string;
  status: string;
  name: string;
}
export type Options = Partial<IOptions>;
// 获取数据
export function PageService(options: Options) {
  if (options.id) {
    return request(`/api/page/${options.id}`);
  } else {
    return request('/api/page', {
      params: options,
    });
  }
}

// 上线或下线接口
export function ChangePageStatus(options: Options) {
  return request(`/api/page/${options.id}`, {
    method: 'PATCH',
    data: {
      status: options.status,
    },
  });
}

// 删除接口
export function DeletePageData(options: Options) {
  return request(`/api/page/${options.id}`, {
    method: 'DELETE',
  });
}

// 编辑接口
export function EditPageDage(options: Options) {
  return request(`/api/page/${options.id}`, {
    method: 'PATCH',
    data: options,
  });
}

// 新建接口
export function CreatePageDage(options: any) {
  return request('/api/page', {
    method: 'POST',
    data: options,
  });
}

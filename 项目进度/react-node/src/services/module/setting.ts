import { ISavaSettinng } from "@/types";
import { request } from "umi";
//请求设置
export const getsetting = ()=>request('/api/setting/get',{
  method:"POST"
});
//保存设置
export const saveSetting=(data:Partial<ISavaSettinng>)=>request('/api/setting',{
  method:"POST",
  data
})
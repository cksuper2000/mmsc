import { request } from 'umi'
import { ILoginForm, Iregister } from '@/types';

export const login = (data:ILoginForm)=>request('/api/auth/login',{
  method:"POST",
  data
})
export const register = (data:Iregister)=>request('/api/user/register',{
  method:"POST",
  data
})
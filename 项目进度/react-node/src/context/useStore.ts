import { useContext } from 'react';
import state from './stateContext';

export default function(){
    return useContext(state);
}

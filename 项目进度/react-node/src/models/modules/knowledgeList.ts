import { getknowledgeList, getknowledgeListid, getessayid } from '@/services';
import { knowledgeList } from '@/types'
import {makeAutoObservable} from 'mobx'
import {message} from 'antd'

class KnowledgeModel{
  KnowledgeModel:Array<knowledgeList>=[]
  total=0
  constructor(){
    makeAutoObservable(this)
  }

  async getknowledgeList(page:number,pageSize:number,title:string,status:string){
    let result=await getknowledgeList({page,pageSize,title,status})
    console.log(result)
    if(result.statusCode==200){
      this.KnowledgeModel=result.data[0]
      this.total=result.data[1]
    }
    return result.data[0],result.data[1];
  }

  async getknowledgeListid(id:string){
    let result=await getknowledgeListid(id)
    console.log(result)
    return result.data
  }

  async getessayid(title:string,content:string,html:string,toc:string,order:number,parentId:string){
    let result=await getessayid({title,content,html,toc,order,parentId})
    console.log(result)
    if(result.statusCode==201){
        message.success('添加成功')
    }else{
      message.error('添加失败')
    }
  }
}
  
export default KnowledgeModel;
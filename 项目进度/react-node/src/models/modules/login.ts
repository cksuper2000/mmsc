import { login, register } from '@/services';
import { ILoginForm, Iregister, IUserInfo } from '@/types';
import { setCookie } from '@/utils/cookie';
import {makeAutoObservable} from 'mobx';

class User{
    isLogin = false;
    userInfo:Partial<IUserInfo> = {};
    userRegister = {};
    constructor(){
        makeAutoObservable(this);
    }

    async login(data: ILoginForm){
        let result = await login(data);
        if (result.data){
            this.isLogin = true;
            this.userInfo = result.data;
            //设置登陆态
            setCookie(result.data.token);
        }
        return result.data;
    }
    async register(data:Iregister){
        let result = await register(data);
        if(result.data){
            this.userRegister=result.data
        }
        return result.data;
    }
}

export default User;
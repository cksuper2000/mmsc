import {makeAutoObservable} from 'mobx'
import {getmailList,deledtmailList} from '@/services'
import {message} from 'antd'

class MailList{
    constructor(){
        makeAutoObservable(this)
    }

    async getmailList(page:number,pageSize:number,from:string,to:string,subject:string){
        let result=await getmailList({page,pageSize,from,to,subject})
        if(result.statusCode==200){
            return result.data
        }else{
            console.log('获取失败')
        }
    }

    async delmailList(id:string){
        let result=await deledtmailList(id)
        if(result.statusCode==200){
            message.success('删除成功')
        }else{
            message.error('删除失败')
        }
    }
}

export default MailList;
import { makeAutoObservable } from 'mobx';
// 引入service
import {
  CommentService,
  ChangeCommentStatus,
  DeleteCommentData,
  ReplyCommentData,
} from '@/services/module/comment';
// 引入参数验证
import { Options } from '@/services/module/comment';
// 引入antd
import { message } from 'antd';
class CommentModel {
  constructor() {
    makeAutoObservable(this);
  }
  // 获取数据
  async commentService(options: Options) {
    let result = await CommentService(options);
    if (result.statusCode == 200) {
      return result.data;
    } else {
      message.error('获取失败');
    }
  }
  // 通过或拒绝接口
  async changeCommentStatus(options: Options) {
    let result = await ChangeCommentStatus(options);
    if (result.statusCode == 200) {
      if (result.data.pass) {
        message.success('评论已通过');
      } else {
        message.success('评论已拒绝');
      }
    }
  }
  // 删除接口
  async deleteCommentData(options: Options) {
    let result = await DeleteCommentData(options);
    if (result.statusCode == 200) {
      message.success('操作成功');
    } else {
      message.error('操作失败');
    }
  }
  // 回复接口
  async replyCommentData(options: any) {
    let result = await ReplyCommentData(options);
    if (result.statusCode == 201) {
      message.success('回复成功');
    } else {
      message.error('回复失败');
    }
  }
}
export default CommentModel;

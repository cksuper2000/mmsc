import { makeAutoObservable, runInAction } from 'mobx';
import {  ISetting } from '@/types/index';
import { getsetting, saveSetting } from '@/services'
class Setting {
  setting:Partial<ISetting>={}
  constructor(){
    makeAutoObservable(this);
  }
  async getSetting(){
    let result = await getsetting();
    if(result.statusCode===200){
      runInAction(()=>{
        console.log(result.data);
        this.setting=result.data;
      })
    }
    return result.data;
  }
  //保存设置
  async saveSetting(data:Partial<ISetting>){
    let result = await saveSetting(data);
    if(result.statusCode===201){
      this.getSetting();
    }
  }
}
export default Setting;
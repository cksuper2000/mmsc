import { Img, Dele } from "@/services"
import { FileData, FileObject } from "@/types/file";
import { makeAutoObservable } from "mobx"

class File {
    fileList: Array<FileObject> = []
    fileNum = 1;
    constructor() {
        makeAutoObservable(this)
    };

   
    async Img(page: number, pageSize: number, originalname: string, type: string) {
        let result = await Img(page, pageSize, originalname, type);
        if (result.statusCode == 200) {
            this.fileList = result.data[0];
            this.fileNum = result.data[1];
        }
        return { fileList: this.fileList, fileNum: this.fileNum }
    };

    async Dele(id: string) {
        let result=await Dele(id);
        console.log(result,"删除删除")
    };


}

export default File
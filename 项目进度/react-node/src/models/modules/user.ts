import { message } from 'antd';
import { table, userSet } from '@/services';
import { UserObject, UserParams } from '@/types/user';
import {makeAutoObservable} from 'mobx';

class User{
    UserList:Array<UserParams>=[];
    UserNum=1;
    constructor(){
        makeAutoObservable(this)
    }
    async Table(page:number,pageSize:number,params:Partial<UserParams>){
        let result=await table(page,pageSize,params);
        if(result.statusCode==200){
            this.UserList=result.data[0];
            this.UserNum=result.data[1];
        }
        return {UserList:this.UserList,UserNum:this.UserNum};
    }

    async userSet(params:Array<UserObject>){
       let b=await Promise.all( params.map(async item=>{
           let b=await userSet(item);
           return b
       })).then(()=>{
           return true
       }).catch(()=>{
           return false
       });
       
      return b
    }
}

export default User;
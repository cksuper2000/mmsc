import { getLabel,addLabel, article, removeList, serchList, setArticleState, removeLable } from '@/services/index'
import { Ilabel, Idata, Datum, ISerch, ITag } from '@/types';
import { makeAutoObservable ,runInAction} from 'mobx'
import moment from 'moment';
import {status} from '@/utils/status'

class Label {
  categoryList:Ilabel=[];
  tagsList:Array<ITag>=[];
  articleList:Array<Datum>=[];
  leng=0;
  constructor(){
    makeAutoObservable(this);
  }
  //获取文章数据
  async getarticle(page:number,pageSize?:number){
    let result = await article(page,pageSize);
    if(result.statusCode===200){
      runInAction(()=>{
        for(let i=0;i<(result.data[0] as Array<Datum>).length;i++){
          let data = (result.data[0] as Array<Datum>)[i];
          (result.data[0] as Array<Datum>)[i].publishAt = moment(data.publishAt).format('YYYY-MM-DD HH:mm:ss');
          (result.data[0] as Array<Datum>)[i].status = status((result.data[0] as Array<Datum>)[i].status);
        }
        this.articleList = result.data[0];
        this.leng = result.data[1];
      })
    }
    return result.data;
  }
  //删除文章数据
  async removeArticle(id:string){
    let result = await removeList(id);
    if(result.statusCode===200) {
      //更新页面数据
      runInAction(()=>{
        let newdata:Array<Datum> = JSON.parse(JSON.stringify(this.articleList));
        let index = newdata.findIndex(item=>item.id===id);
        if(index!==-1) newdata.splice(index,1);
        this.articleList = newdata;
      })
    }
    return result.data;
  }
  //搜索文章数据
  async serchArticle(serch:ISerch){
    let result = await serchList(serch)
    if(result.statusCode===200){
      console.log(result);
      runInAction(()=>{
        this.articleList=result.data[0];
      })
    }
  }
  //修改文章的状态
  async setArtticleState(id:Array<string|number>,state:{[index:string]:string|boolean},type:string){
    for(let i=0;i<id.length;i++){
      let result = await setArticleState(id[i],state,type);
      //查找id
      let newstate = result.data; 
      runInAction(()=>{
        if(type==="DELETE"){
          let newdata:Array<Datum> = JSON.parse(JSON.stringify(this.articleList));
          //查询下标
          let index = newdata.findIndex(item=>item.id===id[i]);
          if(index!==-1) newdata.splice(index,1);
          this.articleList = newdata;
        }else{
          let newdata =  this.articleList.map(item=>{
            if(item.id===newstate.id) item.status=status(newstate.status);
            return item;
          })
          this.articleList = newdata;
        }
      })
    }
  }
  async getcategory(){
    let result = await getLabel('/api/category');
    if(result.statusCode===200){
      runInAction(()=>{
        this.categoryList = result.data;
      })
    }
    return result.data;
  }
  async gettags(){
    let result = await getLabel('/api/tag');
    if(result.statusCode==200){
      runInAction(()=>{
        this.tagsList = result.data;
      })
    }
    return result.data;
  }
  async addcategory(values:Idata,token:string){
    let result = await addLabel(values,token,'/api/category');
    if(result.data){
      runInAction(()=>{
        this.categoryList=[...this.categoryList,result.data];
      })
    }
  }
  async addtags(values:Idata,token:string){
    let result = await addLabel(values,token,'/api/tag');
    if(result.data){
      runInAction(()=>{
        this.tagsList=[...this.tagsList,result.data];
      })
    }
  }
  //删除标签
  async deleteLabel(id:string,type:string){
    let result = await removeLable(id,type);
    if(result.statusCode===200){
      //删除
      if(type==='tag'){
        runInAction(()=>{
          this.tagsList = this.tagsList.filter(item=>item.id!==id);
        })
      }else if(type===''){
        runInAction(()=>{
          this.categoryList = this.tagsList.filter(item=>item.id!==id);
        })
      }
    }
  }
}
export default Label;
import { deleteData, getview, serchView } from '@/services/module/view';
import { IViewSerch, ViewDatum } from '@/types';
import { makeAutoObservable, runInAction } from 'mobx'


class View {
  constructor(){
    makeAutoObservable(this);
  }
  viewList:Array<ViewDatum>=[];
  leng:number=0;
  //获取数据
  async getViewList(page:number,pageSize?:number){
    let result = await getview(page,pageSize);
    if(result.statusCode===200){
      runInAction(()=>{
        this.viewList = result.data[0];
        this.leng=result.data[1];
      })
    }
  }
  //删除数据
  async deleteView(id:string|Array<string>){
    console.log(id);
    if(typeof id==='string'){
      //只有一条删除
      let result = await deleteData(id);
      if(result.statusCode===200){
        let newData:Array<ViewDatum> = this.viewList.filter(item=>{
          if(item.id===id) return;
          return item;
        });
        runInAction(()=>{
          //删除
          this.viewList=newData;
        })
      }
    }else if(id instanceof Array){
      let resultState;
      for(let i=0;i<id.length;i++){
        let result = await deleteData(id[i]);
        resultState = result.statusCode;
      }
      if(resultState===200){
        //刷新页面
        this.getViewList(1);
      }
    }
  }
  //搜索数据
  async getSerchView(params:Partial<IViewSerch>){
    let result = await serchView(params);
    console.log(result);
    //更新数
    if(result.statusCode===200){
      runInAction(()=>{
        this.viewList=result.data[0];
        this.leng = result.data[1];
      })
    }
  }
}
export default View;
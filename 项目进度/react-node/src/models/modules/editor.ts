import { makeAutoObservable ,runInAction} from 'mobx';
import { Img } from '@/services/index'
import {  IFileDatum } from '@/types/file';
import { addArticle, deleteDetail, getDetail, serchFile } from '@/services/module/editor';
import { IEditorData, ISerchEditor } from '@/types';

class Editor {
  fileList:Array<IFileDatum>=[];
  detail={}
  leng=0;
  constructor(){
    makeAutoObservable(this);
  }
  //请求详情数据
  async getDetail(id:string){
    let result = await getDetail(id);
    console.log(result);
    if(result.statusCode===200){
      runInAction(()=>{
        this.detail=result.data;
      })
    }
    return result.data;
  }
  //获取file文件数据
  async getfile(page:number,pageSize:number){
    let result = await Img(page,pageSize);
    if(result.statusCode===200){
      runInAction(()=>{
        this.fileList = result.data[0];
        this.leng = result.data[1];
      })
    }
  }
  //发布文章
  async publishArticle(values:IEditorData){
    let result = await addArticle(values);
    return result.data;
  }
  //搜索
  async serchFile(serch:ISerchEditor){
    let result = await serchFile(serch);
    if(result.statusCode===200){
      runInAction(()=>{
        this.fileList = result.data[0];
        this.leng = result.data[1];
      })
    }
    return result.data;
  }
}
export default Editor;
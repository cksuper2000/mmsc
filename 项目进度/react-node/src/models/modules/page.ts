// 引入services
import {
  ChangePageStatus,
  CreatePageDage,
  DeletePageData,
  EditPageDage,
  PageService,
} from '@/services/module/page';
// 引入mobx
import { makeAutoObservable } from 'mobx';
// 引入参数验证
import { Options } from '@/services/module/page';
// 引入antd
import { message } from 'antd';
// 定义类model
class PageModel {
  constructor() {
    makeAutoObservable(this);
  }
  // 发送接口
  async pageServer(options: Options) {
    let result = await PageService(options);
    if (result.statusCode == 200) {
      return result.data;
    } else {
      message.error('获取失败');
    }
  }
  // 上线或下线接口
  async changePageStatus(options: Options) {
    let result = await ChangePageStatus(options);
    if (result.statusCode == 200) {
      message.success('操作成功');
    } else {
      message.error('操作失败');
    }
  }
  // 删除接口
  async deletePageData(options: Options) {
    let result = await DeletePageData(options);
    if (result.statusCode == 200) {
      message.success('操作成功');
    } else {
      message.error('操作失败');
    }
  }
  // 编辑接口
  async editPageData(options: Options) {
    let result = await EditPageDage(options);
    if (result.statusCode == 200) {
      message.success('页面已更新');
    } else {
      message.error('更新失败');
    }
  }
  // 新建接口
  async createPageData(options: any) {
    let result = await CreatePageDage(options);   
    if (result.statusCode == 201) {
      message.success('页面已发布');
    } else {
      message.error('发布失败');
    }
  }
}
export default PageModel;

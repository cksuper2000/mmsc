// 引入状态管理模块
import UserSet from './modules/user';
import Know from './modules/knowledgeList';
import Label from './modules/label';
import PageModel from "./modules/page";
import File from "./modules/fiile";
import Login from "./modules/login";
import CommentModel from './modules/comment';
import View from './modules/view';
import Mail from './modules/mailList'
import Editor from './modules/editor';
import Setting from './modules/setting';

export default {
  user:new Login(),
  file: new File(),
  userset: new UserSet(),
  Know: new Know(),
  category: new Label(),
  PageModel: new PageModel(),
  CommentModel: new CommentModel(),
  view: new View(),
  Mail: new Mail(),
  editor:new Editor(),
  setting:new Setting()
};

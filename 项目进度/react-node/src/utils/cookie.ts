import Cookies from 'js-cookie'
const key = 'authorization';

//设置登陆态
export function setCookie(value:string){
  let time = +new Date();
  let expires = new Date(time+24*60*60*1000);
  Cookies.set(key,"Bearer "+value,{expires});
}
//获取登陆态
export function getCookie(){
  return Cookies.get(key);
}
//移除登陆态
export function removeCookie(){
  Cookies.remove(key);
}
import { RequestConfig } from 'umi';
import { message } from 'antd';
import StateContext from '@/context/stateContext'
import React from 'react';
import store from '@/models'
import { history } from 'umi';
import { getCookie, removeCookie } from './utils/cookie'

message.config({
  duration: 2,
  maxCount: 1,
});
// 全局路由切换配置
const whiteList = ['/login', '/register']
//在初始化加载路由或者切换路由时做点事情
export function onRouteChange({ matchedRoutes,routes,location  }:any) {
  document.title="小楼又清风" //切换title
  let authorization = getCookie();
  //判断是否有登陆态
  if(authorization!==undefined){
    //判断是否去往登陆,重定向首页
    if(location.pathname==='/login') history.replace('/');
  }else {
    //没有登陆态,判断是否当前在登陆路由,和注册
    if(!whiteList.includes(location.pathname)){
      console.log(12312);
      //定位到登陆页,并且携带过来时候的路由参数
      history.replace('/login?from='+encodeURIComponent(location.pathname));
    }
  }
};

// 覆盖根组件
export function rootContainer(container: React.ReactNode) {
  return React.createElement(StateContext.Provider, {value: store}, container);
}


const baseUrl = 'https://creationapi.shbwyz.com';
export const request: RequestConfig = {
  timeout: 10000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    (url, options) => {
      let  authorization = getCookie();
      options = {...options,headers:{"authorization":authorization!}}
      return {
        url: `${baseUrl}${url}`,
        options,
      };
    }
  ],
  responseInterceptors: [
    response => {
      const codeMaps:{[key:number]:string} = {
        400: '错误的请求',
        403: '访客无权进行该操作',
        404: '找不到资源',
        500: '服务器内部错误',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      if(response.status===401){
        //清除cookie
        removeCookie();
        //进入登陆
        location.href='/login';
      }
      if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1){
        message.destroy();
        message.error({
          content:codeMaps[response.status]
        });
      }
      return response;
    }
  ],
};
import {
  MailOutlined,
  DashboardOutlined,
  FormOutlined,
  CopyOutlined,
  TagOutlined,
  SnippetsOutlined,
  BookOutlined,
  StarOutlined,
  MessageOutlined,
  FolderOpenOutlined,
  SearchOutlined,
  ProjectOutlined,
  UserOutlined,
  SettingOutlined,
} from '@ant-design/icons';



import React, { useEffect, useState } from 'react';





export type Iroute = Array<Ioption>
interface Ioption {
  title: string,
  icon?: React.ReactNode,
  to: string,
  children?: Iroute
}

export const router: Iroute = [{
  title: '工作台',
  icon: <DashboardOutlined />,
  to: '/'
}, {
  title: "文章管理",
  to: '/article',
  icon: <FormOutlined />,
  children: [{
    title: "所有文章",
    to: "/article",
    icon: <FormOutlined />,
  }, {
    title: '分类管理',
    to: "/article/category",
    icon: <CopyOutlined />
  }, {
    title: "标签管理",
    to: "/article/tags",
    icon: <TagOutlined />
  }]
}, {
  title: "页面管理",
  to: "/page",
  icon: <SnippetsOutlined />
}, {
  title: "知识小册",
  to: "/knowledge",
  icon: <BookOutlined />
}, {
  title: "海报管理",
  to: "/poster",
  icon: <StarOutlined />
}, {
  title: "评论管理",
  to: "/comment",
  icon: <MessageOutlined />
}, {
  title: '邮件管理',
  to: '/mail',
  icon: <MailOutlined />
}, {
  title: "文件管理",
  to: "/file",
  icon: <FolderOpenOutlined />
}, {
  title: "搜索记录",
  to: "/search",
  icon: <SearchOutlined />
}, {
  title: "访问统计",
  to: "/view",
  icon: <ProjectOutlined />
}, {
  title: "用户管理",
  to: "/user",
  icon: <UserOutlined />
}, {
  title: "系统设置",
  to: "/setting",
  icon: <SettingOutlined />
}, {
  title: "即时聊天",
  to: '/message',
  icon: <SettingOutlined />
}]
export const newConstruction: Iroute = [{
  title: '新建文章-协同编辑器',
  to: '/article/amEditor'
}, {
  title: '新建文章',
  to: '/article/editor'
}, {
  title: '新建页面',
  to: '/page/editor'
}]
export const login: Iroute = [
  {
    title: "个人中心",
    to: '/ownspace'
  },
  {
    title: "用户管理",
    to: "/user",
  }, {
    title: '系统设置',
    to: '/setting'
  }, {
    title: "退出登陆",
    to: ''
  }]
export const whiteList = ['/login', '/register', "/article/editor", "/page/editor", /\editor\/((.*))[10-15]/ig, /\article\/((.*))[10-15]/ig, '/article/amEditor',/\knowledge\/((.*))[10-15]/ig];

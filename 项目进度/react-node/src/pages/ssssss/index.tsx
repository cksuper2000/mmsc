import React, { KeyboardEventHandler } from 'react'
import { useEffect } from 'react'
import styles from './index.less';
import classNames from 'classnames';
import io,{Socket} from 'socket.io-client';
import { useState ,useRef} from 'react';

interface Mess{
    title?:string,
    value:string
}


export default function Message() {
  // 声明变量
  const instance = useRef<Socket>();
  const [visible,setVisible]=useState(false);
  const [username,setUserName]=useState('');  //用户名
  const [connect,setConnect]=useState('');  //评论
  const [message,setMessage]=useState<Array<Mess>>([])
  
  //首先建立连接
  useEffect(()=>{
    instance.current =  io('ws://127.0.0.1:3000');
    //监听login事件
    instance.current.on(`login`,(data)=>{
        setMessage(messag=>[...messag,{
            value:`Welcome to Socket.IO Chat –`
        },{
            value:`there are${data.numUsers}participants`
        }]);
    });
    //监听发布评论事件
    instance.current.on(`new message`,data=>{
        setMessage(message=>[...message,{
            title:data.username,
            value:data.message
        }]);
    });
    //监听退出事件
    instance.current.on(`user left`,(data)=>{
        console.log(data)
        setMessage(i=>[...i,{
            value:`${data.username}离开了`
        },{
            value:`there are${data.numUsers}participants`
        }])
    });
    //监听其他用户加入
    instance.current.on(`user joined`,(data)=>{
        setMessage(i=>[...i,{
            value:`${data.username}加入聊天室`
        },{
            value:`there are${data.numUsers}participants`
        }])
    })
    
  },[]);
  //用户加入
  function down(every:React.KeyboardEvent){
        if(username&&every.keyCode==13){
            setVisible(true)
            instance.current!.emit(`add user`,username);
        }
  };
  //用户发送信息
  function subimt(every:React.KeyboardEvent){
      if(connect && every.keyCode==13){
          setConnect('');  //清空评论栏
          instance.current!.emit(`new message`,connect); //调用事件，并把评论传递过去
          console.log(username,"namename")
          setMessage(i=>[...i,{
              title:username,
              value:connect
          }])
      }
  }

  return (
    <ul className={styles.pages}>
      {
          visible ? <div className={styles.chatArea}>
              <div className={styles.divdiv}>
                {
                    message.map((item,index)=>{
                        if(!item.title){
                            return <p key={index}>{item.value}</p>
                        }else{
                            return <p key={index}>
                                <span>{item.title}</span>
                                <span>{item.value}</span>
                            </p>
                        }
                    })
                }
              </div>
          
          <input value={connect} onKeyDown={subimt} onChange={(e)=>setConnect(e.target.value)} className={styles.inputMessage} placeholder="Type here..." />
          </div>
          
            : 
            <li className={classNames(styles.login,styles.page)}>
            <div className={styles.form}>
              <h3 className={styles.title}>What's your nickname?</h3>
              <input onKeyDown={down} value={username} onChange={(e)=>setUserName(e.target.value)} className={styles.usernameInput} type="text" maxLength={14} />
            </div>
          </li>
      } 
      
    </ul>
  )
}
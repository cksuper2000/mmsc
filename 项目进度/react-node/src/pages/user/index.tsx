import useStore from "@/context/useStore";
import { UserParams, UserObject } from "@/types/user";
import { Button, Table, Space, message, Form, Input, Row, Select, } from "antd";
import { Col } from "antd/lib/grid";
import moment from "moment";
import React, { useEffect, useState } from "react";
const { Option } = Select;

let user: React.FC = () => {
    let store = useStore();
    const [userButton, setUserButton] = useState([
        {
            name: "启用",
            key: "status",
            value: "active"
        },
        {
            name: "禁用",
            key: "status",
            value: "locked"
        },
        {
            name: "解除授权",
            key: "role",
            value: "visitor"
        },
        {
            name: "授权",
            key: "role",
            value: "admin"
        }
    ]);  //用户操作按钮
    const [userAction, setUserAction] = useState([
        {
            name: "name",
            lable: "账户",
            defa: "请输入用户账户"
        },
        {
            name: "email",
            lable: "邮箱",
            defa: "请输入账户邮箱"
        },
        {
            name: "role",
            lable: "角色",
            children: [
                {
                    name: "管理员",
                    lable: "admin"
                },
                {
                    name: "用户",
                    lable: "visitor"
                }
            ]
        },
        {
            name: "status",
            lable: "状态",
            children: [
                {
                    name: "锁定",
                    lable: "locked"
                },
                {
                    name: "可用",
                    lable: "active"
                }
            ]
        }
    ])
    const [form] = Form.useForm(); // 表单数据
    const [userItem, setUserItem] = useState<Array<UserObject>>([]);
    const [params, setParams] = useState<Partial<UserParams>>({});
    const [date, setDate] = useState<Array<UserObject>>([]); //表格数据
    let [page, setPage] = useState(1);  //页数
    let [pageSize, setPageSize] = useState(12);  //一页多少条
    let [total, setToal] = useState(0);  //总数据
    const [selectedRowKey, setSelectedRowKeys] = useState<Array<string>>([]);  //被选中的数据
    //分页搜索
    async function user() {
        let aa = await store.userset.Table(page, pageSize, params);
        setDate(JSON.parse(JSON.stringify(aa.UserList)));
        setToal(i => i = aa.UserNum);
    };
    //用户单一操作
    async function userset() {
        let acc = await store.userset.userSet(userItem);
        if (acc) {
            user()
            message.success("操作成功")
        } else {
            message.error("操作失败")
        }
    }
    useEffect(() => {
        user()
    }, [page, pageSize,params]);

    useEffect(() => {
        if (userItem.length != 0) {
            userset()
        }

    }, [userItem])

    const rowSelection = {
        selectedRowKey,
        onChange: onSelectChange
    };

    function onSelectChange(selectedRowKeys: Array<number | string>) {
        setSelectedRowKeys(i => i = selectedRowKeys as Array<string>);
    };
    function pageChange(a: any) {
        let { current, pageSize } = a;
        setPage(current);
        setPageSize(pageSize);
    };
    function scd(k: string, v: string) {
        let newArr = selectedRowKey.map(item => {
            return date.filter(v => {
                return v.id == item
            })
        });
        let ffffff = newArr.flat(newArr.length).map(item => {
            item = {
                ...item,
                [k]: v
            }
            return item
        });
        setUserItem(v => v = ffffff as Array<UserObject>)

    };
    const onFinish = (values: UserObject) => {
        setParams(values);
        setPage(i=>i=1);
    };

    const columns = [
        {
            title: '账户',
            dataIndex: 'name'
        },
        {
            title: '邮箱',
            dataIndex: 'email',

        },
        {
            title: '角色',
            dataIndex: 'role',
            render: (text: string) => text == "visitor" ? <span>访客</span> : <span>管理员</span>

        },
        {
            title: '状态',
            dataIndex: 'status',
            render: (text: string) => text == "active" ? <span>可用</span> : <span>已锁定</span>

        },
        {
            title: '注册日期',
            dataIndex: 'createAt',
            render: (text: string) => <span>{moment(text).format('YYYY-MM-DD HH:mm:ss')}</span>

        },
        {
            title: '操作',
            key: 'action',
            render: (text: string, recome: UserObject) => (
                <Space size="middle">
                    <a onClick={() => setUserItem(i => i = [{ ...recome, status: recome.status == "locked" ? "active" : "locked" }])}>{recome.status == "locked" ? "启用" : "禁用"}</a>
                    <a onClick={() => setUserItem(i => i = [{ ...recome, role: recome.role == "visitor" ? "admin" : "visitor" }])}>{recome.role == "visitor" ? "授权" : "解除授权"}</a>
                </Space>
            ),
        }
    ];
    return (
        <div>
            <div style={{ backgroundColor: "#fff", marginBottom: "22px" }}>
                <Form
                    form={form}
                    name="advanced_search"
                    className="ant-advanced-search-form"
                    onFinish={onFinish}
                >
                    <Row gutter={24}>
                        {
                            userAction.map((item, index) => {
                                if (!item.children) {
                                    return <Col span={8} key={index}>
                                        <Form.Item
                                            name={item.name}
                                            label={item.lable}
                                        >
                                            <Input placeholder={item.defa} />
                                        </Form.Item>
                                    </Col>
                                } else {
                                    return <Col span={8} key={index}>
                                    <Form.Item
                                        name={item.name}
                                        label={item.lable}
                                    >
                                        <Select placeholder="select your gender">
                                            {
                                                item.children.map((v,i)=>{
                                                  return <Option key={i} value={v.lable}>{v.name}</Option>
                                                })
                                            }
                                        </Select>
                                    </Form.Item>
                                </Col>
                                }
                            })
                        }
                    </Row>
                    <Row>
                        <Col span={24} key={5} style={{ textAlign: 'right' }}>
                            <Button type="primary" htmlType="submit">
                                搜索
                            </Button>
                            <Button
                                style={{ margin: '0 8px' }}
                                onClick={() => {
                                    form.resetFields();
                                }}
                            >
                                重置
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </div>
            {
                selectedRowKey.length != 0 ? <div style={{ marginBottom: 16 }}>
                    {
                        userButton.map((item, index) => {
                            return <Button onClick={() => scd(item.key, item.value)} key={index}>{item.name}</Button>
                        })
                    }
                </div> : null

            }
            <Table
                onChange={pageChange}
                rowKey={(e) => {
                    return e.id
                }} rowSelection={rowSelection}
                columns={columns} dataSource={date}
                pagination={{ total, showSizeChanger: true, current: page, pageSize: pageSize, pageSizeOptions: ["8", "12", "24", "36"] }}
            />
        </div>
    )
}

export default user
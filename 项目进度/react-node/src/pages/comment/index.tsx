import React, { useState, useEffect } from 'react';
// 在需要用到的 组件文件中引入中文语言包
import zhCN from 'antd/es/locale/zh_CN';
// 引入antd组件
import {
    Form, Row, Col,
    Input, Button,
    Cascader, Table,
    Space, Divider, Typography,
    ConfigProvider, Empty, Badge,
    Popconfirm, Tooltip, Popover,
    Modal
} from 'antd';
// 引入样式
import PageLess from "@/pages/page/index.less"
import CommentLess from "./index.less"
import { CascaderValueType } from 'antd/lib/cascader';
// 引入类型验证
import { ICommetObject } from "@/types/modules/comment"
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite'
// 引入参数验证
import { Options } from '@/services/module/comment';
// 引入表格操作组件
import TableAction from "@/components/TableAction";
// 引入时间插件
import moment from "moment";

interface Props {

}
// 定义列表接口
interface DataType extends ICommetObject { }
// 验证搜索框的值
interface IValues {
    name: string,
    email: string,
    pass: string
}
// 验证通过和拒绝
export interface ICommentStatus {
    status: boolean,
    selectedRows: Array<ICommetObject>
}
const Page = () => {
    // 创建store
    const store = useStore();
    // 定义初始的page
    let [page, setPage] = useState(1);
    // 定义初始的pageSize
    let [pageSize, setPageSize] = useState(12);
    // 定义表格数据
    const [data, setData] = useState<ICommetObject[]>([]);
    // 定义初始总数据数量
    const [total, setTotal] = useState(0);
    // 定义初始搜索到的值
    const [value, setValue] = useState({})
    // 定义预览窗口的位置
    const [placement, setPlacement] = useState<any>("right")
    // 定义回复弹窗初始状态
    const [isModalVisible, setIsModalVisible] = useState(false);
    // 创建文本域
    const { TextArea } = Input;
    // 定义文本域默认值
    const [textValue, setTextValue] = useState('');
    // 回复需要的参数
    const [reply, setReply] = useState({});
    // 展示回复弹窗
    const showModal = (record: ICommetObject) => {
        setIsModalVisible(true);
        setReply({
            email: record.email,
            hostId: record.hostId,
            name: record.name,
            parentCommentId: record.parentCommentId,
            replyUserEmail: record.replyUserEmail,
            replyUserName: record.replyUserName,
            url: record.url
        })
    };
    // 点击确定关闭回复弹窗
    async function handleOk() {
        setIsModalVisible(false);
        await store.CommentModel.replyCommentData({ ...reply, createByAdmin: true, content: textValue });
        getData({ page, pageSize })
    };
    // 点击取消关闭回复弹窗
    function handleCancel() {
        setIsModalVisible(false);
    };
    useEffect(() => {
        // 调用公共方法
        getData({ page, pageSize })
    }, [])
    // 封装公共函数
    async function getData(options: Options) {
        // 发接口拿数据
        let result = await store.CommentModel.commentService(options)
        // 赋值
        setData(result[0])
        setTotal(result[1])
    }
    window.onresize = (e: any) => {
        if (e.target.innerWidth < 800) {
            setPlacement("left")
        }
    }
    // 判断状态定义循环的圈数
    const [expand, setExpand] = useState(false);
    // 定义搜索框
    const [inpArr, setInpArr] = useState([
        {
            name: "name",
            label: "称呼",
            placeholder: "请输入称呼"
        },
        {
            name: "email",
            label: "Email",
            placeholder: "请输入联系方式"
        },
        {
            name: "pass",
            label: "状态",
            placeholder: ""
        },
    ])
    // 定义状态下拉框
    const options = [
        {
            value: "1",
            label: "已通过"
        },
        {
            value: "0",
            label: "未通过"
        }
    ]
    // 下拉框选中的值
    function onChange(value: CascaderValueType) {
    }
    const [form] = Form.useForm();
    const getFields = () => {
        const count = expand ? 10 : 3;
        const children = [];
        for (let i = 0; i < count; i++) {
            children.push(
                <Col span={6} key={i}>
                    <Form.Item
                        name={inpArr[i].name}
                        label={inpArr[i].label}
                    // rules={[
                    //     {
                    //         // required: true,
                    //         message: 'Input something!',
                    //     },
                    // ]}
                    >
                        {
                            inpArr[i].label !== "状态" ? <Input placeholder={inpArr[i].placeholder} /> : <Cascader options={options} onChange={onChange} placeholder={inpArr[i].placeholder} />
                        }
                    </Form.Item>
                </Col>,
            );
        }
        return children;
    };
    // 点击搜索拿到的值
    const onFinish = (values: IValues) => {
        setValue(values)
        // 传值
        getData({ page, pageSize, ...values })
    };
    // 定义表格表头列表
    const columns = [
        {
            title: '状态',
            dataIndex: 'pass',
            width: 100,
            fixed: 'left',
            render: (text: boolean, record: DataType) => (
                <Badge status={text == true ? "success" : "warning"} text={text == true ? "已通过" : "未通过"} />
            )
        },
        {
            title: '称呼',
            dataIndex: 'name',
        },
        {
            title: '联系方式',
            dataIndex: 'email',
        },
        {
            title: '原始内容',
            dataIndex: 'content',
            render: (text: any) => <Tooltip title={<div style={{ color: "black" }}><p style={{ borderBottom: "1px solid #eee" }}>评论详情-原始内容</p><p>{text}</p></div>} color={"white"}>
                <Button type="link">查看内容</Button>
            </Tooltip>
        },
        {
            title: 'HTML 内容',
            dataIndex: 'html',
            render: (text: any, record: any) => <Tooltip title={<div style={{ color: "black" }}><p style={{ borderBottom: "1px solid #eee" }}>评论详情-HTML 内容</p><p dangerouslySetInnerHTML={{ __html: text }}></p></div>} color={"white"}>
                <Button type="link">查看内容</Button>
            </Tooltip>
        },
        {
            title: '管理文章',
            dataIndex: 'url',
            render: (text: any, record: any) => <Popover placement={placement} overlayClassName={CommentLess.pop} title="页面预览" color={"white"} content={<iframe src={`https://creationadmin.shbwyz.com/creationad.shbwyz.co${text}`}></iframe>}>
                <Button type="link">文章</Button>
            </Popover>
        },
        {
            title: '创建时间',
            dataIndex: 'createAt',
            render: (text: string) => moment(text).format("YYYY-MM-DD HH:mm:ss")
        },
        {
            title: '父级评论',
            dataIndex: 'replyUserName',
            render: (text: string) => text ? <p>{text}</p> : <p>无</p>
        },
        {
            title: '操作',
            dataIndex: 'actions',
            key: 'x',
            fixed: 'right',
            render: (text: any, record: ICommetObject) => (
                <Space split={<Divider type="vertical" />} wrap size={2}>
                    <Typography.Link onClick={() => status({ status: true, selectedRows: [record] })}>通过</Typography.Link>
                    <Typography.Link onClick={() => status({ status: false, selectedRows: [record] })}>拒绝</Typography.Link>
                    <Typography.Link onClick={() => showModal(record)}>回复</Typography.Link>
                    <Popconfirm
                        title="确认删除？"
                        onConfirm={() => confirm([record])}
                        onCancel={cancel}
                        okText="确认"
                        cancelText="取消"
                    >
                        <Typography.Link>删除</Typography.Link>
                    </Popconfirm>
                </Space>
            ),
        },
    ];

    // 定义被选中表格的数据
    const [selectedRows, setSelectedRows] = useState<DataType[]>([]);
    // 选中表格事件
    const rowSelection = {
        // 被选中表格的数据
        onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
            setSelectedRows(selectedRows)
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record: DataType) => ({
            disabled: record.name === 'Disabled User', // Column configuration not to be checked
            name: record.name,
        }),
    };
    // 设置空状态
    const customizeRenderEmpty = () => (
        //这里面就是我们自己定义的空状态
        <div style={{ textAlign: 'center' }}>
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={"暂无数据"} />
        </div>
    );
    // 点击刷新按钮刷新数据
    function refresh() {
        getData({ page, pageSize })
    }
    // 确认删除
    function confirm(data: Array<ICommetObject>) {
        data.forEach(item => {
            deleteCommentData({ id: item.id })
        })
    }
    async function deleteCommentData(options: Options) {
        await store.CommentModel.deleteCommentData(options)
        getData({ page, pageSize })
    }
    // 取消删除
    function cancel() {

    }
    // 通过或拒绝
    function status(options: ICommentStatus) {
        options.selectedRows.forEach(item => {
            changeStatus({ status: options.status, id: item.id })
        })
    }
    // 通过或拒绝
    async function changeStatus(options: Options) {
        await store.CommentModel.changeCommentStatus(options)
        getData({ page, pageSize })
    }

    // 渲染页面
    return <div className="page">
        <div className={PageLess.header}>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                onFinish={onFinish}
            >
                <Row gutter={24}>{getFields()}</Row>
                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button
                            style={{ margin: '0 8px' }}
                            onClick={() => {
                                // 重置输入框的值
                                form.resetFields();
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>
        </div>
        <div className="main">
            <div className="search-result-list">
                <div className={PageLess.table}>
                    <div className={PageLess.tableAction}>
                        <TableAction selectedRows={selectedRows} pathname={window.location.pathname} refresh={refresh} confirm={confirm} status={status} />
                    </div>
                    <ConfigProvider renderEmpty={customizeRenderEmpty} locale={zhCN}>
                        <Table
                            scroll={{ x: 1800 }}
                            rowKey="id"
                            rowSelection={{
                                ...rowSelection,
                            }}
                            columns={columns as any}
                            dataSource={data}
                            pagination={{
                                total, pageSizeOptions: ["8", "12", "24", "36"], pageSize, showSizeChanger: true, showTotal: (total) => { return `共${total}条`; }, onChange: (page, pageSize) => {
                                    setPageSize(pageSize!)
                                    getData({ page, pageSize, ...value })
                                }
                            }}
                        />
                    </ConfigProvider>
                </div>
            </div>
        </div>
        <Modal title="回复评论" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} okText={"回复"}>
            <TextArea
                value={textValue}
                onChange={(e) => setTextValue(e.target.value)}
                placeholder="支持Markdown"
                autoSize={{ minRows: 6, maxRows: 10 }}
            />
        </Modal>
    </div>
}
export default observer(Page)

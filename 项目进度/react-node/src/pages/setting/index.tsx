import React,{useEffect, useState} from 'react';
import styles from './index.less';
import {Tabs,Form, Input, Button}from 'antd';
import useStore from '@/context/useStore';
import { useHistory } from 'react-router-dom';
import { useLocation } from 'umi';
import Globalization  from '@/components/Globalization'
import SettingForm from '@/components/SettingForm';
import SEOForm from '@/components/SEOForm';
import DataForm from '@/components/DataForm'
import SMTPForm from '@/components/SMTPForm';
import { observer } from 'mobx-react-lite';
import OssSetting from '@/components/OssSetting';

const { TabPane } = Tabs;

interface ILocation {
  query:{type:string}
}

function Setting() {
  const history = useHistory();
  const {query} = (useLocation() as unknown as ILocation);
  const [ currTitle,setCurrTitle ] = useState("0");
  const store = useStore(); 
  const { setting } = store.setting;
  console.log(setting);
  const tabList = ([{
    title:"系统设置",
    component:<SettingForm formData={setting}/>
  },{
    title:"国际化设置",
    component:setting.i18n?.length?<Globalization value={setting.i18n}/>:null
  },{
    title:"SEO设置",
    component:<SEOForm formData={setting}/>
  },{
    title:"数据统计",
    component:<DataForm formData={setting}/>
  },{
    title:"oss设置",
    component:<OssSetting value={setting.oss!}/>
  },{
    title:"SMTP服务",
    component:<SMTPForm formData={setting}/>
  }])
  useEffect(()=>{
    store.setting.getSetting();
  },[])
  useEffect(()=>{
    let index = tabList.findIndex(item=>item.title===query.type);
    if(index===-1){
      setCurrTitle(tabList[0].title);
    }else{
      setCurrTitle(tabList[index].title);
    }
  },[query.type])
  return (
    <div className={styles.setting}>
      <Tabs 
        tabPosition='left'
        activeKey={currTitle}
        onTabClick={(key)=>{
          history.push('/setting?type='+key)
        }}
      >{
          tabList.map((item)=>{
            return (
              <TabPane tab={item.title} key={item.title}>
                {item.component}
              </TabPane>
            )
          })
        }</Tabs>
    </div>
  )
}
export default observer(Setting);
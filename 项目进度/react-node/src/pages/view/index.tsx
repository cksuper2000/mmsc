import React, { useState } from 'react';
import { Form, Button,Table ,Popconfirm ,Tooltip,Badge } from 'antd';
import {  PlusOutlined,ReloadOutlined } from '@ant-design/icons';
import styles from './index.less';
import {observer} from 'mobx-react-lite';
import useStore from '@/context/useStore'
import { useEffect } from 'react';
import {  IViewSerch, ViewDatum } from '@/types';
import ViewFrom from '@/components/ViewFrom';

const View:React.FC = () => {
  const store = useStore();
  //多选框
  let [ check,setCheck ] = useState<Array<string|number>>([]);
  const [pagination,setPagination] = useState<{page:number,pageSize:number}>({
    page:1,
    pageSize:12
  });
  //仓库数据
  const { viewList ,leng} = store.view;
  const columns: any = [
    {
      title: 'URL',
      dataIndex: 'url',
      key: 'name',
      fixed: 'left',
      width:200
    },
    { title: 'IP', dataIndex: 'ip',width:160 },
    { title: '浏览器', dataIndex: 'browser',width:121 },
    { title: '内核', dataIndex: 'engine' ,width:121},
    { title: '操作系统',dataIndex:"os",width:121},
    { title: '设备',dataIndex:"",width:121},
    { title: '地址', dataIndex: 'address',width:121 },
    { title: '访问量', width:121,render:(data:ViewDatum)=>(
      <Badge
        className="site-badge-count-109"
        count={data.count}
        style={{ backgroundColor: '#52c41a' }}
      />
    ) },
    { title: '访问时间', dataIndex: 'createAt',width:'200px' },
    {
      title: '操作',
      key: 'operation',
      fixed: 'right',
      render: (data:ViewDatum) => (
        <span className={styles.action}>
          <Popconfirm 
            title="确认删除这个文章?" okText="Yes" cancelText="No" 
            onConfirm={()=>store.view.deleteView(data.id)}
          >
            <a href="">
              <button type="button" className="ant-btn ant-btn-link ant-btn-sm">
                <span>删除</span>
              </button>
            </a>
          </Popconfirm>
        </span>
      ),
    },
  ];
  
  //请求数据
  useEffect(()=>{
   store.view.getViewList(pagination.page,pagination.pageSize);
  },[pagination.page,pagination.pageSize])
  //搜索
  const onFinish = (values:Partial<IViewSerch>) => {
    //去除undefined与空
    store.view.getSerchView({...values,page:pagination.page,pageSize:pagination.pageSize});
  };
  const onSelectChange = (selectedRowKeys:Array<string|number>) => {
    setCheck(selectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys:check,
    onChange: onSelectChange,
  };
  //请求数据
  return (
    <div className={styles.article}>
      <ViewFrom onFinish={onFinish}/>
      <div className={styles.searchResultList}>
        <div className={styles.newestablish}>
          {
            check.length?(
              <div className={styles.btn}>
                <Popconfirm title="确认删除这个文章?" okText="Yes" cancelText="No" 
                  onConfirm={()=>store.view.deleteView((check as Array<string>))}
                >
                  <Button danger>删除</Button>
                </Popconfirm>
              </div>
            ):<div></div>
          }
          <div>
            <Button type="primary" icon={<PlusOutlined />}>新建</Button>
            <Tooltip placement="top" title={"刷新"}>
              <ReloadOutlined
                style={{marginLeft:"12px",cursor:"pointer"}}
                onClick={()=>store.view.getViewList(1)}
              />
            </Tooltip>
          </div>
        </div>
        <Table
          rowKey={(data)=>data.id}
          columns={columns} 
          rowSelection={(rowSelection)}
          dataSource={ viewList as Array<ViewDatum>} 
          scroll={{ x: 1300 }} 
          pagination={{
            current:pagination.page,
            pageSize:pagination.pageSize,total:leng,
            pageSizeOptions:["8","24","36"],
            onChange:(page:number,pageSize:number|undefined)=>{
              setPagination({
                page,
                pageSize:pageSize!
              });
            }}}
        />
      </div>
    </div>
  );
};
export default observer(View);
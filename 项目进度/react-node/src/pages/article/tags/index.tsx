import React from 'react';
import styles from './index.less';
import useStore from '@/context/useStore'
import { useEffect } from 'react';
import {observer} from 'mobx-react-lite'
import { Idata } from '@/types';
import Article from '@/components/Article';
import { message } from 'antd';

const Tags= ()=> {
  const store = useStore();
  useEffect(()=>{
    store.category.gettags();
  },[])
  let { tagsList } = store.category;
  const onFinish = (values: Idata) => {
    let token = localStorage.token;
    store.category.addtags(values,token);
  };
  const onDelete = (id:string)=>{
    store.category.deleteLabel(id,'tag');
    message.success("删除成功");
  }
  return (
    <div className={styles.category}>
      <Article onDelete={onDelete} List={tagsList} onFinish={onFinish}/>
    </div>
  )
}
export default observer(Tags);
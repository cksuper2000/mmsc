import React, { useState } from 'react';
import styles from './index.less';
import { 
  Form,
  message,
} from 'antd';
import { useEffect } from 'react';
import useStore from '@/context/useStore';
import FileView from '@/components/FileView';
import EditorPage from '@/components/EditorPage'
import { makeHtml, makeToc } from '@/utils/markdown';
import { IEditorData } from '@/types';
import { useHistory,useParams } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import EditorFile from '@/components/EditorFile';

let str = `
# 欢迎使用 Wipi Markdown 编辑器

> * 整理知识，学习笔记
> * 发布日记，杂文，所见所想
> * 撰写发布技术文稿（代码支持）

## 什么是 Markdown

Markdown 是一种方便记忆、书写的纯文本标记语言，用户可以使用这些标记符号以最小的输入代价生成极富表现力的文档：譬如您正在阅读的这份文档。它使用简单的符号标记不同的标题，分割不同的段落，**粗体** 或者 *斜体* 某些文字。

### 1. 待办事宜 Todo 列表

- [ ] 支持以 PDF 格式导出文稿
- [x] 新增 Todo 列表功能

### 2. 高亮一段代码[^code]
\`\`\`python
@requires_authorization
class SomeClass:
    pass

if __name__ == '__main__':
    # A comment
    print 'hello world'
\`\`\`

### 3. 绘制表格

| 项目        | 价格   |  数量  |
| --------   | -----:  | :----:  |
| 计算机     | 1600 |   5     |
| 手机        |   12   |   12   |
| 管线        |    10    |  234  |

### 4. 嵌入网址
<iframe src="//player.bilibili.com/player.html?aid=77737877&bvid=BV1xJ411z7eS&cid=132993821&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>

\`\`\`HTML
<iframe src="//player.bilibili.com/player.html?aid=77737877&bvid=BV1xJ411z7eS&cid=132993821&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>
\`\`\`
`;

const MyEditor = ()=> {
  let {id} = useParams<{id:string}>();
  const store = useStore();
  const [form] = Form.useForm();
  //控制图片上传页面显示隐藏
  const [ imgVisible,setImgVisible ] = useState(false);
  const [title,setTitle] = useState('');
  const [value,setValue] = useState(str);
  const [visible, setVisible] = useState(false);
  const [image,setimage] = useState('');
  const history = useHistory();
  //文件上传页码
  const [page,setPage] = useState(1); 
  //文件上传的页数
  const [pageSize,setPageSize] = useState(12);
  //文件上传数据
  const {fileList,leng} = store.editor;
  //加载中页面
  useEffect(()=>{
    //请求数据
    store.editor.getfile(page,pageSize);
  },[])
  useEffect(()=>{
    //请求详情
    if(id){
      store.editor.getDetail(id).then(res=>{
        form.setFieldsValue({
          ...form.getFieldValue,
          tags:res.tags.map((item:{id:string})=>item.id),
          cover:res.cover,
          isRecommended:res.isRecommended,
          isCommentable:res.isCommentable,
          category:res.category?.id,
          summary:res.summary,
        });
        setValue(res.content);
        setimage(res.cover);
        setTitle(res.title)
      })
    }
  },[id])
  const submit =async ()=>{
    let values: IEditorData = form.getFieldsValue();
    if (!title){
        message.warn('请输入文章标题');
        return;
    }
    // 添加文章的md
    values.content = value;
    values.tags = (values.tags as unknown as Array<string>).join(',');
    // 添加文章内容的html
    values.html = makeHtml(value);
    // // 添加文章的toc
    values.toc = JSON.stringify(makeToc(values.html as string));
    // // 添加文章的标题
    values.title = title;
    // // 添加文章状态
    values.status = 'publish';

    let result = await store.editor.publishArticle(values);
    if (result){
        history.replace(`/article/editor/${result.id}`);
    }
  }
  //编辑器的回调
  const titleChange = (e:React.ChangeEvent<HTMLInputElement>)=>{
    setTitle(e.target.value)
  }
  const editorChange = (str:string)=>{
    setValue(str);
  }
  //点击设置开启页面
  const setting = ()=>{
    //开启页面
    if(!title.length){
      return message.warning("请输入标题");
    }
    setVisible(true);
  }
  //图片上传页面的搜索
  const onSerch = (data:{originalname:string,type:string})=>{
    store.editor.serchFile({...data,page,pageSize});
  }
  //点击回去表单页
  const hanldImage=(imgurl:string)=>{
    form.setFieldsValue({
      ...form.getFieldValue,
      cover:imgurl
    });
    setimage(imgurl);
    setImgVisible(false);
    setVisible(true);
  }
  //分页
  const PageChange = (page:number,pageSize?:number)=>{
    store.editor.getfile(page,pageSize!);
    setPage(page);
    setPageSize(pageSize!);
  }
  return (
    <EditorPage 
      id={id}
      value={value} 
      titleChange={titleChange} 
      editorChange={editorChange} 
      submit={submit}
      fn={setting} 
      title={title}
    >
      <div className={styles.editor}>
        <EditorFile
          form={form}
          visible={visible}
          hanldFile={()=>setImgVisible(true)}
          hanldOk={()=>setVisible(false)}
          onClose={()=>setVisible(false)}
          image={image}
        />
        <FileView
          fileList={fileList}
          hanldImage={hanldImage}
          page={page}
          pageSize={pageSize}
          onChange={PageChange}
          onClose={()=>setImgVisible(false)}
          imgVisible={imgVisible}
          onFinish={onSerch}
          leng={leng}
        />
      </div>
    </EditorPage>
  )
}
export default observer(MyEditor);

import React, { useEffect, useState } from "react";
import Editorpage from "@/components/EditorPage";
import styles from "./index.less";
let ind: React.FC = () => {
    const [title, setTitle] = useState("");
    const [connect,setConnect]=useState("");
    let titleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTitle(e.currentTarget.value)
    };
    let submit = () => {

    };
    let fn = () => {

    };
    useEffect(()=>{
        window.addEventListener("message",function(message){
            setConnect(message.data);
        })
    },[])
    return (
        <div style={{width:"100%",height:"100%"}}>
            <div>
                <Editorpage fn={fn} submit={submit} titleChange={titleChange} title={title} editorHide={false} />
            </div>
            <div className={styles.ifrnam}>
                <iframe style={{height:"100%",width:"100%"}} src="https://jasonandjay.com/editor/static/"></iframe>
            </div>
        </div>
    )
}

export default ind
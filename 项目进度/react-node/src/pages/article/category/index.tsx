import React from 'react';
import styles from './index.less';
import useStore from '@/context/useStore'
import { useEffect } from 'react';
import {observer} from 'mobx-react-lite'
import { Idata } from '@/types';
import Article from '@/components/Article';
import { message } from 'antd';


const Category= ()=> {
  const store = useStore();
  useEffect(()=>{
    //获取本地token
    store.category.getcategory();
  },[])
  let { categoryList } = store.category;
  const onFinish = (values: Idata) => {
    console.log('Success:', values);
    let token = localStorage.token;
    store.category.addcategory(values,token);
  };
  const onDelete = (id:string)=>{
    store.category.deleteLabel(id,'category');
    message.success("删除成功");
  }
  return (
    <div className={styles.category}>
      <Article onDelete={onDelete} List={categoryList} onFinish={onFinish}/>
    </div>
  )
}
export default observer(Category);
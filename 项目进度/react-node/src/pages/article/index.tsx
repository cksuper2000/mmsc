import React, { useState } from 'react';
import { Form, Row, Col, Input, Button, Select,Table ,Divider,Popconfirm,Modal ,Tooltip,Badge,Tag } from 'antd';
import {  PlusOutlined,ReloadOutlined } from '@ant-design/icons';
import styles from './index.less';
import {observer} from 'mobx-react-lite';
import useStore from '@/context/useStore'
import { useEffect } from 'react';
import { Datum, ISerch } from '@/types';
import { useHistory } from 'umi';

const { Option } = Select;
const AdvancedSearchForm:React.FC = () => {
  const store = useStore();
  const [form] = Form.useForm();
  const history = useHistory();
  const [pagination,setPagination] = useState<{
    page:number,
    pageSize:number
  }>({
    page:1,
    pageSize:12
  });
  const { articleList ,categoryList ,leng} = store.category;
  const [visible, setVisible] = useState(false);
  let [ check,setCheck ] = useState<Array<string>>([]);
  const columns: any = [
    {
      title: '标题',
      dataIndex: 'title',
      key: 'name',
      fixed: 'left',
      width:200
    },
    { title: '状态' ,render:(data:Datum)=>(
      <Badge status={data.status==="已发布"?"success":"warning"} text={data.status}/>
    ),width:"100px"},
    { title: '分类', render:(data:Datum)=>(
      <div>
        {
          data.category?<Tag color={"#"+Math.random().toString(16).slice(-6)}>
            {data.category.label}
          </Tag>:null
        }
      </div>
    )},
    { title: '标签', render:(data:Datum)=>(
      <div>
        {
          data.tags.map(item=>(
            <Tag color={"#"+Math.random().toString(16).slice(-6)} key={item.id}>
              {item.label}
            </Tag>
          ))
        }
      </div>
    ),width:"200px"},
    { title: '阅读量',render:(data:Datum)=>(
      <Badge
        count={data.views?data.views:"0"}
        style={{ backgroundColor: '#52c41a' }}
        showZero
      />
    ) },
    { title: '喜欢数',render:(data:Datum)=>(
      <Badge style={{background:"rgb(235, 47, 150)"}} count={data.likes} showZero />
    )},
    { title: '发布时间', dataIndex: 'publishAt',width:'200px' },
    {
      title: '操作',
      key: 'operation',
      fixed: 'right',
      width: 318,
      render: (data:Datum) => (
        <span className={styles.action}>
          <a href="">
            <button type="button" className="ant-btn ant-btn-link ant-btn-sm">
              <span onClick={()=>history.push('/article/editor/'+data.id)}>编辑</span>
            </button>
          </a>
          <Divider type="vertical"/>
          <button type="button" className="ant-btn ant-btn-link ant-btn-sm">
            <span>首焦推荐</span>
          </button>
          <Divider type="vertical"/>
          <button type="button" 
            onClick={() => setVisible(true)}
            className="ant-btn ant-btn-link ant-btn-sm"
          >
            <span>查看访问</span>
          </button>
          <Divider type="vertical"/>
          
          <button type="button" className="ant-btn ant-btn-link ant-btn-sm">
            <Popconfirm title="确认删除这个文章?" okText="Yes" cancelText="No" 
              onConfirm={()=>removeArticle(data.id)}
            >
              <span>删除</span>
            </Popconfirm>
          </button>
        </span>
      ),
    },
  ];
  //请求数据
  useEffect(()=>{
    store.category.getcategory();
    store.category.getarticle(pagination.page,pagination.pageSize);
  },[])
  //搜索
  const onFinish = (values:ISerch) => {
    store.category.serchArticle({
      page:pagination.page,
      pageSize:pagination.pageSize,
      title:values.title,
      status: values.status=="draft"?"draft":"publish",
      category: values.category
    });
  };
  const removeArticle = (id:string)=>{
    store.category.removeArticle(id);
  }
  const onSelectChange = (selectedRowKeys:Array<string>) => {
    setCheck(selectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys:check,
    onChange: onSelectChange,
  };
  return (
    <div className={styles.article}>
      <Form
        form={form}
        name="advanced_search"
        className={styles.antAdvancedSearchForm}
        onFinish={onFinish}
      >
        <Row gutter={24}>
          <Col>
            <Form.Item
              name="title"
              label="标题"
              rules={[
                {
                  required: true,
                  message: 'Input something!',
                },
              ]}
            >
              <Input placeholder="请输入文章标题" />
            </Form.Item>
          </Col>
          <Col>
          <Form.Item
            name="status"
            label="状态"
            rules={[
              {
                required: true,
                message: 'Input something!',
              },
            ]}
          >
            <Select style={{width: "180px"}}>
              <Option value="已发布">已发布</Option>
              <Option value="草稿">草稿</Option>
            </Select>
          </Form.Item>
          </Col>
          <Col>
            <Form.Item
              name="category"
              label="分类"
              rules={[
                {
                  required: true,
                  message: 'Input something!',
                },
              ]}
            >
              <Select style={{width: "180px"}}>
                {
                  categoryList.map(item=>(
                    <Option key={item.id} value={item.id}>{item.label}</Option>
                  ))
                }
                
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
            <Button
              style={{ margin: '0 8px' }}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
          </Col>
        </Row>
      </Form>
      <div className={styles.searchResultList}>
        <div className={styles.newestablish}>
          {
            check.length?(
              <div className={styles.btn}>
                <Button onClick={()=>store.category.setArtticleState(check,{status:'publish'},"PATCH")}>发布</Button>
                <Button onClick={()=>store.category.setArtticleState(check,{status:'draft'},"PATCH")}>草稿</Button>
                <Button onClick={()=>store.category.setArtticleState(check,{isRecommended:true},"PATCH")}>首焦推荐</Button>
                <Button onClick={()=>store.category.setArtticleState(check,{isRecommended:false},"PATCH")}>撤销首焦</Button>
                <Popconfirm title="确认删除这个文章?" okText="Yes" cancelText="No" 
                  onConfirm={()=>store.category.setArtticleState(check,{},"DELETE")}
                >
                  <Button danger>删除</Button>
                </Popconfirm>
              </div>
            ):<div></div>
          }
          <div>
            <Button 
              type="primary" 
              icon={<PlusOutlined />}
              onClick={()=>history.push('/article/editor')}
            >新建</Button>
            <Tooltip placement="top" title={"刷新"}>
              <ReloadOutlined
                style={{marginLeft:"12px",cursor:"pointer"}}
                onClick={()=>{
                  store.category.getarticle(1);
                }}
              />
            </Tooltip>
          </div>
        </div>
        <Table
          rowKey={(data)=>data.id}
          columns={columns} 
          rowSelection={(rowSelection as any)}
          dataSource={ articleList as Array<Datum>} 
          pagination={{
            current:pagination.page,
            total:leng,
            pageSize:pagination.pageSize,
            showSizeChanger:true,
            pageSizeOptions:["8","12",'24','36'],
            showTotal: (total) =>`共${total}条`,
            onChange:(page:number,pageSize:number|undefined)=>{
              store.category.getarticle(page,pageSize).then(res=>{
                setPagination(prev=>({
                  page,
                  pageSize:pageSize!
                }))
              })
            }
          }}
          scroll={{ x: 1300 }} 
        />
      </div>
      <Modal
        title="访问统计"
        centered
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}
        width={"60vw"}
        footer={null}
      >
        <p>some contents...</p>
        <p>some contents...</p>
        <p>some contents...</p>
      </Modal>
    </div>
  );
};
export default observer(AdvancedSearchForm);
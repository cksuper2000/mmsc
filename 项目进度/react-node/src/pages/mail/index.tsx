import React, { useState, useEffect } from 'react'
import { Form, Row, Col, Button, Input, Table, Popconfirm, message } from 'antd';
import styles from './index.less'
import useStore from '@/context/useStore'
import {getmailis} from '@/types'

interface Props {

}

const mailList = (props: Props) => {

    function confirm(id:string) {
        setida(id)
        store.Mail.delmailList(id).then(()=>{
            store.Mail.getmailList(page, pageSize, from, to, subject).then(res => {
                setMailfile(res[0])
                settotal(res[1])
            })
        })
    }

    function cancel(e:React.MouseEvent<HTMLElement, MouseEvent> | undefined) {
        console.log(e);
        message.error('Click on No');
    }
    const columns = [
        {
            title: '发件人',
            dataIndex: 'from',
            key: 'from'
        },
        {
            title: '收件人',
            dataIndex: 'to',
            key: 'to'
        },
        {
            title: '主题',
            dataIndex: 'subject',
            key: 'subject'
        },
        {
            title: '发送时间',
            dataIndex: 'createAt',
            key: 'createAt'
        },
        {
            title: '操作',
            dataIndex: 'del',
            key: 'del',
            render: (text:any,record:getmailis) => <Popconfirm
                title="确认删除这个邮件?"
                onConfirm={()=>confirm(record.id)}
                onCancel={cancel}
                okText="确认"
                cancelText="取消"
            >
                <a href="#">删除</a>
            </Popconfirm>
        },
    ];
    let [selectedRowKeys, selectedRowKeysstate] = useState([])
    let [loading, loadingstate] = useState(false)
    const store = useStore()
    let [page, pagestate] = useState(1)
    let [pageSize, pageSizestate] = useState(12)
    let [Mailfile, setMailfile] = useState<Array<getmailis>>([])
    let [total, settotal] = useState(0)
    let [from, setfrom] = useState('')
    let [to, setto] = useState('')
    let [subject, setsubject] = useState('')
    let [ida,setida]=useState('')
    useEffect(() => {
        store.Mail.getmailList(page, pageSize, from, to, subject).then(res => {
            console.log(res)
            setMailfile(res[0])
            settotal(res[1])
        })
    }, [])
    // function start() {
    //     loadingstate(true)
    //     setTimeout(() => {
    //         selectedRowKeys = []
    //         loadingstate(false)
    //     }, 1000);
    // };
    function inputchange() {
        setfrom(from)
        setto(to)
        setsubject(subject)
        store.Mail.getmailList(page, pageSize, from, to, subject).then(res => {
            console.log(res)
            setMailfile(res[0])
            settotal(res[1])
        })
    }

    const rowSelection = {
        onChange: (selectedRowKeys: React.SetStateAction<never[]>) => {
            selectedRowKeysstate(selectedRowKeys);

        },
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (
        <div>
            <Form
                name="advanced_search"
                className={styles.advanced_search}
            >
                <div className={styles.note}>
                    <Form.Item rules={[{ required: true }]}>
                        发件人 : <Input style={{ width: 222 }} placeholder='请输入发件人' value={from} onChange={(e) => setfrom(e.target.value)} />
                    </Form.Item>
                    <Form.Item rules={[{ required: true }]}>
                        收件人 : <Input style={{ width: 222 }} placeholder='请输入收件人' value={to} onChange={(e) => setto(e.target.value)} />
                    </Form.Item>
                    <Form.Item rules={[{ required: true }]}>
                        主题 : <Input style={{ width: 222 }} placeholder='请输入主题' value={subject} onChange={(e) => setsubject(e.target.value)} />
                    </Form.Item>
                </div>

                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit" onClick={() => inputchange()}>
                            搜索
                        </Button>
                        <Button
                            style={{ margin: '0 8px' }}
                            onClick={() => {
                                setfrom('')
                                setto('')
                                setsubject('')
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>


            <Form
                className={styles.footerall}
            >
                <div style={{ marginBottom: 16 }}>
                <Popconfirm
                title="确认删除这个邮件?"
                onConfirm={()=>confirm(ida)}
                onCancel={cancel}
                okText="确认"
                cancelText="取消"
            >
               <Button danger  style={{ display: hasSelected ? 'block' : 'none' }} loading={loading}>
                        删除
          </Button> 
            </Popconfirm>
                </div>
                <Table rowKey="id" rowSelection={rowSelection as never} dataSource={Mailfile} columns={columns} scroll={{ x: 1600 }} pagination={{
                    total,
                    pageSizeOptions: ['8', '10', '12', '16'],
                    showSizeChanger: true,
                    showTotal: (total) => `共${total}条`,
                    onChange: (page, pageSize) => {
                        pageSizestate(pageSize!)
                        store.Mail.getmailList(page, pageSize!, from, to, subject)
                    }
                }} />


            </Form>
        </div>
    )
}

export default mailList;

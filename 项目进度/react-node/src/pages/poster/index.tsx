import React from 'react'
import { Form, Row, Col, Button, Input } from 'antd';
import styles from './index.less'

interface Props {
    
}

const Poster = (props: Props) => {
    return (
        <div>
            <Form
                name="advanced_search"
                className={styles.advanced_search}
            >
                <div className={styles.note}>
                    <Form.Item rules={[{ required: true }]}>
                        名称 : <Input style={{ width: 222 }} placeholder='请输入文件名称' />
                    </Form.Item>
                </div>

                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button
                            style={{ margin: '0 8px' }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
                </Form>


                <Form
                className={styles.footerall}
            >



            </Form>
        </div>
    )
}

export default Poster;

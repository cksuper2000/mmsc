import React from 'react'
import { useEffect } from 'react'
import styles from './index.less';
import classNames from 'classnames';
import io,{Socket} from 'socket.io-client';
import { useState ,useRef} from 'react';
import md5 from 'md5';

interface IMessage {
  log:string,
  numUsers:number,
  username:string,
  messageBody:string,
  color:string
  [index:string]:any
}
const COLORS = [
  '#e21400', '#91580f', '#f8a700', '#f78b00',
  '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
  '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
];
let lastTypingTime:number;
let TYPING_TIMER_LENGTH = 400;
export default function Message() {
  //登陆页面显示隐藏
  const [logo,setLogo] = useState(true);
  //登陆输入框内容
  const [logoValue,setLogoValue] = useState('');
  //消息输入框的
  const [ message,setMessage ] = useState("");
  //打字
  const [isTyping, setIsTyping] = useState(false);
  //消息列表
  const [ messageList,setMessageList ] = useState<Array<Partial<IMessage>>>([{
    log:'Welcome to Socket.IO Chat – '
  }]);
  //保存用户名
  const [ useName,setUserName ] = useState('');
  const instance = useRef<Socket>();
  useEffect(()=>{
    instance.current=io('ws://127.0.0.1:3000');
    //接受服务端返回用户个数
    instance.current.on('login', (data) => {
      //渲染列表
      setMessageList((prev)=>{
        return [...prev,{log:"there's "+data.numUsers+" participant"}]
      })
    });
    //监听用户进入
    instance.current.on('user joined',(user)=>{
      setMessageList((prev)=>{
        return [...prev,
          {log:user.username+' joined'},
          {log:'there are '+user.numUsers+' participants'}
      ]
      })
      //监听用户离开
      instance.current!.on('user left',(user)=>{
        setMessageList((prev)=>{
          return [...prev,{log:user.username+"离开了"},
          {log:"there are"+user.numUsers+" participant"}];
        })
      })
    })
    //监听其他人发送的消息
    instance.current.on('new message',(information)=>{
      //添加消息列表
      setMessageList((prev)=>{
        let color = getUserColor(information.username);
        return [...prev,{
          username:information.username,
          messageBody:information.message,
          color:COLORS[color]
        }]
      })
    })
    //监听别人打字
    instance.current.on('typing',(user)=>{
      //查找是哪个用户
      setMessageList((prev)=>{
        //查找是哪个用户
        return [...prev,{
          log:user.username+"正在打字....",
        }];
      })
    })
    //别人停止打字
    instance.current!.on('stop typing',(user)=>{
      setMessageList((prev)=>{
        return [...prev,{
          log:user.username+"停止了打字....",
        }];
      })
    })
  },[])
  //用户输入
  const logoInput = (event:React.KeyboardEvent<HTMLInputElement>)=>{
    if (event.which === 13) {
      //发送事件
      instance.current?.emit('add user',logoValue);
      //设置用户名
      setUserName(logoValue);
      //关闭登陆也
      setLogo(false);
    }
  }
  //消息框回车
  const messageInput = (event:React.KeyboardEvent<HTMLInputElement>)=>{
    //发送消息
    if (event.which === 13) {
      //消息列表添加
      setMessageList((prev)=>{
        let color = getUserColor(useName);
        return [...prev,{username:useName,messageBody:message,color:COLORS[color]}];
      })
      //发送消息
      instance.current!.emit('new message',message);
    }
  }
  //消息框正在打字
  const messageChange = (e:React.ChangeEvent<HTMLInputElement>)=>{
    setMessage(e.target.value);
    //发送正在打字
    instance.current?.emit('typing');
    //保存当前时间
    lastTypingTime = (new Date()).getTime();
  //   if (!isTyping){
  //     instance.current!.emit('typing');
  //     timeout = setTimeout(() =>{
  //         instance.current!.emit('stop typing');
  //         setIsTyping(false);
  //     }, TYPING_TIMER_LENGTH) as unknown as number;
  // }else{
  //     clearTimeout(timeout);
  //     timeout = setTimeout(() =>{
  //         instance.current!.emit('stop typing');
  //         setIsTyping(false);
  //     }, TYPING_TIMER_LENGTH) as unknown as number;
  // }

  }
  const getUserColor = (username:string)=>{
    //计算颜色公式
    return parseInt(md5(username).slice(-1),16)%12;
  }
  return (
    <ul className={styles.pages}>
      {logo?<li 
        className={classNames(styles.login,styles.page)}
      >
        <div className={styles.form}>
          <h3 className={styles.title}>What's your nickname?</h3>
          <input 
            className={styles.usernameInput} 
            type="text" maxLength={14} 
            onKeyDown={logoInput}
            value={logoValue}
            onChange={(e:React.ChangeEvent<HTMLInputElement>)=>setLogoValue(e.target.value)}
          />
        </div>
      </li>:<li
        className={classNames(styles.chat, styles.page)}
      >
        <div className={styles.chatArea}>
          <ul 
            className={styles.messages}
          >
            {/* 渲染消息列表 */}
            {
              messageList.map((item,index)=>(
                <li key={index} 
                  className={styles[item.log?"log":"message"]}

                >
                  {item.log}
                  {item.username?<span 
                    className={styles.username}
                    style={{
                      color:item.color
                    }}
                  >{item.username}</span>:null}
                  {item.messageBody?<span className={styles.messageBody}>{item.messageBody}</span>:null}
                </li>
              ))
            }
          </ul>
        </div>
        <input 
          className={styles.inputMessage} 
          placeholder="Type here..." 
          value={message}
          onKeyDown={messageInput}
          onChange={messageChange}
        />
      </li>}
    </ul>
  )
}
import React,{FC, useState} from 'react';
import { Form, Input, Button,Modal  } from 'antd';
import styles from './index.less';
import { useHistory } from 'react-router-dom'
import useStore from '@/context/useStore';
import { NavLink } from 'umi';
import LoginLayout from '@/components/LoginLayout';
import { Iregister } from '@/types';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { RegisterLogo } from '@/assets/img/logoSvg';
import { refStructEnhancer } from 'mobx/dist/internal';

const { confirm } = Modal;

export default function Register() {
  const history = useHistory();
  const store = useStore();
  const [ value,setValue ] = useState({
    name:'',
    password:'',
    confirm:'',
    nameState:false,
    passwordState:false,
    confirmState:false
  });
  //发送注册
  const onFinish = async(values:Iregister) => {
    //发送请求
    let result = await store.user.register(values);
    showConfirm();
  };
  function showConfirm() {
    confirm({
      title: '注册成功',
      icon: <ExclamationCircleOutlined />,
      content: '是否跳转登陆',
      onOk() {
        history.push('/login');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  return (
    <div className={styles.register}>
      <LoginLayout logo={RegisterLogo}>
        <div style={{width:"100%"}}>
          <h2>访客注册</h2>
          <Form
            name="normal_login"
            className={styles.loginForm}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onValuesChange={(data)=>setValue(prev=>{
              return {...prev,...data,[Object.keys(data)[0]+"State"]:true};
            })}
          >
            <Form.Item
              label="账户"
              name="name"
              hasFeedback={value.nameState}
              validateStatus={value.name?'success':(value.nameState?"error":'')}
              rules={[{ required: true, message: '请输入用户名!' }]}
            >
              <Input placeholder="请输入用户名"/>
            </Form.Item>

            <Form.Item
              label="密码"
              name="password"
              hasFeedback={value.passwordState}
              validateStatus={value.password?'success':(value.passwordState?"error":'')}
              rules={[{ required: true, message: '请输入密码!' }]}
            >
              <Input placeholder="请输入密码"/>
            </Form.Item>

            <Form.Item
              label="确认"
              name="confirm"
              hasFeedback={value.confirmState}
              validateStatus={value.confirm?'success':(value.confirmState?"error":'')}
              rules={[{ required: true, message: '请再次输入密码!' }]}
            >
              <Input placeholder="请再次输入密码"/>
            </Form.Item>

            <Form.Item>
              <Button type="primary" style={{width:"100%"}} size={'large'} htmlType="submit">
                注册
              </Button>
            </Form.Item>

            <Form.Item>
              Or <NavLink to="/login">去登陆</NavLink>
            </Form.Item>
          </Form>
        </div>
      </LoginLayout>
    </div>
  )
}
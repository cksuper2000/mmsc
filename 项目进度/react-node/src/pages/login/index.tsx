import { Form, Input, Button,Row, Col } from 'antd';
import styles from './index.less';
import React,{ FC } from 'react';
import { useHistory } from 'react-router-dom'
import useStore from '@/context/useStore';
import { NavLink, useLocation } from 'umi';
import LoginLayout from '@/components/LoginLayout';
import { LogoSvg } from '@/assets/img/logoSvg'

interface ILocationQuery{
  query: {
      from: string;
  }
}
const NormalLoginForm:FC = () => {
  const history = useHistory();
  const location = useLocation();
  const store = useStore();
  const onFinish = async(values:{name:string,password:string}) => {
    let result = await store.user.login(values);
    //保存token,保存本地储存
    localStorage.token = result.token;
    localStorage.user = JSON.stringify(result);
    let redirect = '/';
    if ((location as unknown as ILocationQuery).query.from){
        redirect = decodeURIComponent((location as unknown as ILocationQuery).query.from);
    }
    history.replace(redirect);
  };
  return (
    <div className={styles.login}>
      <LoginLayout logo={LogoSvg}>
        <div style={{width:"100%"}}>
          <h2>访客注册</h2>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
          >
            <Form.Item
              label="账户"
              name="name"
              rules={[{ required: true, message: '请输入用户名!' }]}
            >
              <Input placeholder="请输入用户名"/>
            </Form.Item>

            <Form.Item
              label="密码"
              name="password"
              rules={[{ required: true, message: '请输入密码!' }]}
            >
              <Input.Password placeholder="请输入密码"/>
            </Form.Item>

            <Form.Item>
              <Button type="primary" style={{width:"100%"}} htmlType="submit">
                登陆
              </Button>
            </Form.Item>

            <Form.Item>
              Or <NavLink to="/register">注册用户</NavLink>
            </Form.Item>
          </Form>
        </div>
      </LoginLayout>
    </div>
  )
};

export default NormalLoginForm;
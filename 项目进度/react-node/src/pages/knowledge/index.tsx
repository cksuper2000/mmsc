import React, { useState, useEffect } from 'react'
import { useHistory, NavLink } from 'umi'
import styles from './index.less'
import { Form, Row, Col, Button, Input, Select, Drawer, Switch, Upload, message, Card, Pagination, Popconfirm, Tooltip } from 'antd';
import { InboxOutlined, CloudUploadOutlined, EditOutlined, SettingOutlined, DeleteOutlined, CloudDownloadOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react-lite'
import { delectknowledgeList, newknowledgeList, setknowledgeList, popupknowledgeList } from '@/services';
import File from '@/components/File/index'
const { Meta } = Card;
import useStore from '@/context/useStore';
import { knowledgeList } from '@/types';
const { Option } = Select;
const { Dragger } = Upload;

interface Props {

}

interface filelist {
    createAt: string;
    filename: string;
    id: string;
    originalname: string;
    size: number;
    type: string;
    url: string;
}

const Knowledge = () => {
    let [fileList, fileListstate] = useState<filelist>({} as filelist)
    let aaa = (i: filelist, b: boolean) => {
        setVisiblea(b)
        console.log(i, b)
        fileListstate(i)
    }
    console.log(fileList)
    const store = useStore()
    const history = useHistory()
    let [page, pagestate] = useState(1)
    let [pageSize, pageSizestate] = useState(12)
    let [title, titlestate] = useState('')
    let [status, statusstate] = useState('')
    useEffect(() => {
        store.Know.getknowledgeList(page, pageSize, title, status)
    }, [page]);
    function pageChange(e: number) {
        console.log(e)
        pagestate(e)
        store.Know.getknowledgeList(page, pageSize, title, status)
    }


    function confirm(e: React.MouseEvent<HTMLElement, MouseEvent> | undefined, id: string) {
        console.log(e);
        message.success('删除成功');

        delectknowledgeList(id).then(() => {
            store.Know.getknowledgeList(page, pageSize, title, status)
        })
    }

    function cancel(e: React.MouseEvent<HTMLElement, MouseEvent> | undefined) {
        console.log(e);
        message.error('取消删除');
    }





    const [visible, setVisible] = useState(false);
    const [visiblea, setVisiblea] = useState(false)
    let [titlea, titleastate] = useState('')
    let [isCommentable, isCommentablestate] = useState(false)
    let [summary, summarystate] = useState('')
    let [cover, coverstate] = useState('')
    const showDrawer = () => {
        setVisible(true);

    };
    let [ida, idastate] = useState('')
    const showDrawera = (item: knowledgeList) => {
        setVisible(true);
        titleastate(item.title)
        summarystate(item.summary)
        isCommentablestate(item.isCommentable)
        coverstate(fileList.url)
        idastate(item.id)
    };
    const onClose = () => {
        setVisible(false);
        if (!titlea && !summary) {
            message.error('缺少名称或描述')
        } else {
            ida ? setknowledgeList(ida, { title: titlea, summary: summary, cover: fileList.url, isCommentable: isCommentable }).then(res => {
                if (ida) {
                    if (res.statusCode == 200) {
                        titleastate('')
                        isCommentablestate(false)
                        summarystate('')
                        coverstate('')
                        idastate('')
                        message.success('修改成功')
                        store.Know.getknowledgeList(page, pageSize, title, status)
                    }
                }
            })
                : newknowledgeList({ title: titlea, summary: summary, cover: fileList.url, isCommentable: isCommentable }).then(res => {
                    if (res.statusCode == 201) {
                        titleastate('')
                        isCommentablestate(false)
                        summarystate('')
                        coverstate('')
                        message.success('添加成功')
                        store.Know.getknowledgeList(page, pageSize, title, status)
                    }
                })
        }

    };

    function inputbtn() {
        titlestate(title = title)
        statusstate(status = status)
        store.Know.getknowledgeList(page, pageSize, title, status)
    }

    function showChildrenDrawer() {
        setVisiblea(true)
    };

    function onChildrenDrawerClose() {
        setVisiblea(false)
    };

    function popupbtn(item: knowledgeList) {
        popupknowledgeList(item.id, {
            status: item.status === 'draft' ? 'publish' : 'draft'
        }).then(res => {
            if (res.statusCode == 200) {
                store.Know.getknowledgeList(page, pageSize, title, status)
            }
        })
    }



    return (
        <div className={styles.all}>
            <Form
                name="advanced_search"
                className={styles.advanced_search}
            >
                <div className={styles.note}>
                    <Form.Item rules={[{ required: true }]}>
                        名称 : <Input style={{ width: 222 }} placeholder='请输入知识库名称' value={title} onChange={(e) => titlestate(e.target.value)} />
                    </Form.Item>
                    <Form.Item rules={[{ required: true }]} style={{ marginLeft: 20 }} >
                        状态 : <Select
                            allowClear
                            style={{ width: 180 }}
                            value={status}
                            onChange={value => statusstate(value)}
                        >
                            <Option value="publish">已发布</Option>
                            <Option value="draft">草稿</Option>
                        </Select>
                    </Form.Item>
                </div>


                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit" onClick={() => inputbtn()}>
                            搜索
                        </Button>
                        <Button
                            style={{ margin: '0 8px' }}
                            onClick={() => {
                                titlestate('')
                                statusstate('')
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>
            <Form
                className={styles.footerall}
            >
                <Row>
                    <Col span={24} style={{ textAlign: 'right', fontWeight: 800 }}>
                        <Button type="primary" htmlType="submit" onClick={showDrawer}>
                            + 新建
                        </Button>
                    </Col>
                </Row>
                <div style={{ display: 'flex', marginTop: 12 }}>
                    {
                        store.Know.KnowledgeModel.map(item => {
                            return <Card key={item.id}
                                style={{ width: 300, marginRight: 15, height: '100%' }}
                                cover={
                                    <img style={{ width: '100%', height: 120 }}
                                        alt="example"
                                        src={item.cover}
                                    />
                                }
                                actions={[
                                    <NavLink to={`/knowledge/editor/${item.id}`}><EditOutlined key="edit" /></NavLink>,
                                    item.status==='draft'?<Tooltip placement="top" title='发布线上'>
                                        <CloudUploadOutlined onClick={() => popupbtn(item)} />,
                                     </Tooltip>:<Tooltip placement="top" title={'设为草稿'}>
                                        <CloudDownloadOutlined onClick={() => popupbtn(item)} />,
                                     </Tooltip>,
                                    

                                    <SettingOutlined key="setting" onClick={() => showDrawera(item)} />,
                                    <Popconfirm
                                        title="确认删除?"
                                        onConfirm={(e) => confirm(e, item.id)}
                                        onCancel={cancel}
                                        okText="确认"
                                        cancelText="取消"
                                    >
                                        <DeleteOutlined />
                                    </Popconfirm>

                                ]}
                            >
                                <Meta
                                    title={item.title}
                                    description={item.summary}
                                />
                            </Card>
                        })
                    }
                </div>
                {
                    store.Know.KnowledgeModel.length !== 0 ? <Pagination showSizeChanger={true} showTotal={total => `共${store.Know.total}条`} pageSizeOptions={['8', '12']} current={page} defaultCurrent={page} defaultPageSize={pageSize} total={store.Know.total} onChange={(e) => pageChange(e)} style={{ textAlign: 'right', marginTop: 12 }} /> : ''
                }

            </Form>
            <Drawer
                title={ida ? '更新知识库' : '新建知识库'}
                placement="right"
                closable={true}
                onClose={onClose}
                visible={visible}
                width={600}
                footer={[
                    <Button key="back" onClick={() => setVisible(false)} style={{ marginRight: 10 }}>
                        取消
            </Button>,
                    <Button key="submit" type="primary" onClick={onClose}>
                        {ida ? '更新' : '创建'}
                    </Button>
                ]}
                footerStyle={{ textAlign: 'right' }}
            >
                <Form.Item
                    label="名称"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input value={titlea} onChange={e => titleastate(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="描述"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input value={summary} onChange={e => summarystate(e.target.value)} />
                </Form.Item>
                <Form.Item label="评论" valuePropName="checked">
                    <Switch checked={isCommentable} onChange={e => isCommentablestate(e)} />
                </Form.Item>
                <Form.Item label="封面" style={{ marginTop: '16px' }}>
                    {
                        fileList.url ? <Dragger>
                            <img style={{ width: '100%', height: 120 }} src={fileList.url} alt="" />
                        </Dragger> : <Dragger>
                                <p className="ant-upload-drag-icon">
                                    <InboxOutlined />
                                </p>
                                <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                                <p className="ant-upload-hint">
                                    文件上传到OSS,如未配置请先配置
    </p>
                            </Dragger>
                    }
                    <Input type='text' value={fileList.url} style={{ marginTop: '12px' }} />
                    <Button style={{ marginTop: '12px' }} onClick={showChildrenDrawer}>选择文件</Button>
                </Form.Item>


                <Drawer
                    title="文件选择"
                    width={786}
                    closable={true}
                    onClose={onChildrenDrawerClose}
                    visible={visiblea}
                >
                    <File source="56" fun={aaa} />
                </Drawer>
            </Drawer>
        </div>
    )
}

export default observer(Knowledge);
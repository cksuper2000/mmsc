import React, { useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import styles from './index.less'
import { SettingOutlined, CloseOutlined, MenuOutlined, DeleteOutlined } from '@ant-design/icons';
import useStore from '@/context/useStore'
import { knowledgeListid } from '@/types'
import { Popconfirm, message, Popover, Button, Input } from 'antd';
import ForEditor from 'for-editor'

interface Props {

}

const Knowledgeid: React.FC<RouteComponentProps<{ id: string }>> = (props) => {
    let [title, settitlea] = useState('')
    let [content, setcontenta] = useState('')
    let [html, sethtmla] = useState('')
    let [toc, settoca] = useState('[]')
    let [order, setordera] = useState(0)
    let [parentId, setparentIda] = useState('')
    let [uid, setuid] = useState(-1)
    // console.log(props.match.params.id)
    const store = useStore()
    const toolbar = {
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        /* v0.0.9 */
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        /* v0.2.3 */
        subfield: true, // 单双栏模式
    }
    let [KnoledgeListid, KnoledgeListidstate] = useState<knowledgeListid>({} as knowledgeListid)
    useEffect(() => {
        store.Know.getknowledgeListid(props.match.params.id).then((res) => {
            KnoledgeListidstate(res)
        })
    }, [])
    let [contentchil,setcontentchil]=useState<Array<Partial<knowledgeListid>>>(KnoledgeListid.children || [])
    const contentchildren=contentchil[uid]||null;
    function confirm(e: React.MouseEvent<HTMLElement, MouseEvent> | undefined) {
        console.log(e);
        message.success('Click on Yes');
        props.history.push('/knowledge')
    }

    function cancel(e: React.MouseEvent<HTMLElement, MouseEvent> | undefined) {
        console.log(e);
        message.error('Click on No');
    }

    const contenta = (
        <div style={{ display: 'flex' }}>
            <Input type='text' value={title} onChange={(e) => settitlea(e.target.value)} />
            <div>
                <Button style={{ marginLeft: 10 }} type='primary' onClick={() => {
                    if (!title) {
                        message.error('缺少参数')
                    } else {
                        store.Know.getessayid(title = title, content = content, html = html, toc = toc, order = order, parentId = KnoledgeListid.id).then(() => {
                            store.Know.getknowledgeListid(props.match.params.id).then((res) => {
                                KnoledgeListidstate(res)
                            })
                        })
                    }


                }}>添加</Button>
            </div>
        </div>
    );
    return (
        <div className={styles.all}>
            <div className={styles.left}>
                <header className={styles.header}>
                    <div className={styles.headertop}>
                        <Popconfirm
                            title="确认关闭？如果有内容变更，请先保存。"
                            placement="topRight"
                            onConfirm={confirm}
                            onCancel={cancel}
                            okText="确认"
                            cancelText="取消"
                        >
                            <span><CloseOutlined /></span>
                        </Popconfirm>

                        <div>
                            <span>
                                <img style={{ width: 32, height: 32 }} src={KnoledgeListid.cover} alt="" />
                            </span>
                            <span style={{ marginLeft: 8 }}>{KnoledgeListid.title}</span>
                        </div>
                        <span><SettingOutlined /></span>
                    </div>
                    <div style={{ marginTop: 16 }}>
                        <button className={styles.button}>保存</button>
                    </div>
                    <div style={{ marginTop: 16, display: 'flex', justifyContent: 'space-between' }}>
                        <span style={{ fontWeight: 500 }}>{KnoledgeListid.children?.length}篇文章</span>
                        <Popover placement="right" content={contenta} >
                            <button style={{ width: 66, height: 24, background: '#fff', border: '1px solid #d9d9d9' }}>+ 新建</button>
                        </Popover>

                    </div>
                </header>
                <main className={styles.main}>
                    <div className={styles.fram}>
                        {
                            KnoledgeListid.children?.map((item, index) => {
                                return <div style={{ background: index === uid ? '#09f' : '', color: index === uid ? '#fff' : '' }} onClick={() => setuid(index)} className={styles.frame} key={index}>
                                    <span><MenuOutlined /></span>
                                    <span style={{ flex: 1, marginLeft: 8 }}>{item.title}</span>
                                    <span><DeleteOutlined /></span>
                                </div>
                            })
                        }
                    </div>
                </main>
            </div>
            <div className={styles.right}>
                {
                    uid != -1 ? <ForEditor
                        height='100%'
                        value={KnoledgeListid.children[uid].content}
                        toolbar={toolbar}
                        onChange={(value) => setcontenta(value)}
                        subfield={true}
                        preview={true}
                    /> : <div className={styles.righthint}>请新建章节（或者选择章节进行编辑）</div>


                }

            </div>
        </div>
    )
}

export default Knowledgeid;
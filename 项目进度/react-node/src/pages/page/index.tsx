import React, { useState, useEffect } from 'react';
import { history } from "umi"
// 在需要用到的 组件文件中引入中文语言包
import zhCN from 'antd/es/locale/zh_CN';
// 引入antd组件
import {
    Form, Row, Col,
    Input, Button,
    Cascader, Table,
    Space, Divider, Typography,
    ConfigProvider, Empty, Badge,
    Popconfirm, message
} from 'antd';
// 引入样式
import PageLess from "./index.less"
import { CascaderValueType } from 'antd/lib/cascader';
// 引入类型验证
import { IPageObject } from "@/types/modules/page"
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite'
// 引入参数验证
import { Options } from '@/services/module/page';
// 引入表格操作组件
import TableAction from "@/components/TableAction";
// 引入时间插件
import moment from "moment";

interface Props {

}
// 定义列表接口
interface DataType extends IPageObject { }
// 验证搜索框的值
interface IValues {
    name: string,
    path: string,
    status: string
}
// 验证发布和下线
export interface IPageStatus {
    type: string,
    selectedRows: Array<IPageObject>
}
const Page = () => {
    // 创建store
    const store = useStore();
    // 定义初始的page
    let [page, setPage] = useState(1);
    // 定义初始的pageSize
    let [pageSize, setPageSize] = useState(8);
    // 定义表格数据
    const [data, setData] = useState<IPageObject[]>([]);
    // 定义初始总数据数量
    const [total, setTotal] = useState(0);
    // 定义初始搜索到的值
    const [value, setValue] = useState({})
    // 获取数据
    useEffect(() => {
        getData({ page, pageSize })
    }, [])
    // 获取数据
    async function getData(options: Options) {
        let result = await store.PageModel.pageServer(options);
        setData(result[0]);
        setTotal(result[1])
    }
    // 发送上线或下线接口
    async function changeStatus(options: Options) {
        await store.PageModel.changePageStatus(options);
        getData({ page, pageSize })
    }
    // 上线或下线
    function status(options: IPageStatus) {
        options.selectedRows.forEach(item => {
            changeStatus({ id: item.id, status: options.type })
        })
    }
    // 判断状态定义循环的圈数
    const [expand, setExpand] = useState(false);
    // 定义搜索框
    const [inpArr, setInpArr] = useState([
        {
            name: "name",
            label: "名称",
            placeholder: "请输入页面名称"
        },
        {
            name: "path",
            label: "路径",
            placeholder: "请输入页面路径"
        },
        {
            name: "status",
            label: "状态",
            placeholder: ""
        },
    ])
    // 定义状态下拉框
    const options = [
        {
            value: "publish",
            label: "已发布"
        },
        {
            value: "draft",
            label: "草稿"
        }
    ]
    // 下拉框选中的值
    function onChange(value: CascaderValueType) {
        console.log(value);
    }
    // 点击搜索拿到的值
    const onFinish = (values: IValues) => {
        console.log('Received values of form: ', values);
        setValue(values)
        // 传值
        getData({ page, pageSize, ...values })
    };
    const [form] = Form.useForm();
    const getFields = () => {
        const count = expand ? 10 : 3;
        const children = [];
        for (let i = 0; i < count; i++) {
            children.push(
                <Col span={6} key={i}>
                    <Form.Item
                        name={inpArr[i].name}
                        label={inpArr[i].label}
                    // rules={[
                    //     {
                    //         // required: true,
                    //         message: 'Input something!',
                    //     },
                    // ]}
                    >
                        {
                            inpArr[i].label !== "状态" ? <Input placeholder={inpArr[i].placeholder} /> : <Cascader options={options} onChange={onChange} placeholder={inpArr[i].placeholder} />
                        }
                    </Form.Item>
                </Col>,
            );
        }
        return children;
    };

    // 定义表格表头列表
    const columns = [
        {
            title: '名称',
            dataIndex: 'name',
        },
        {
            title: '路径',
            dataIndex: 'path',
        },
        {
            title: '顺序',
            dataIndex: 'order',
        },
        {
            title: '阅读量',
            dataIndex: 'views',
            render: (text: number, record: DataType) => (
                <Badge
                    className="site-badge-count-109"
                    count={text}
                    style={{ backgroundColor: '#52c41a' }}
                    showZero={true}
                />
            )
        },
        {
            title: '状态',
            dataIndex: 'status',
            render: (text: string, record: DataType) => (
                <Badge status={text == "publish" ? "success" : "warning"} text={text == "publish" ? "已发布" : "草稿"} />
            )
        },
        {
            title: '发布时间',
            dataIndex: 'publishAt',
            render: (text: any) => moment(text).format("YYYY-MM-DD HH:mm:ss")
        },
        {
            title: '操作',
            dataIndex: 'actions',
            key: 'x',
            render: (text: any, record: IPageObject) => (
                <Space split={<Divider type="vertical" />} wrap={true}>
                    <Typography.Link onClick={() => {
                        history.push(`/page/editor/${record.id}`,)

                    }}>编辑</Typography.Link>
                    {
                        record.status == "draft" ? <Typography.Link onClick={() => status({ type: "publish", selectedRows: [record] })}>发布</Typography.Link> : <Typography.Link onClick={() => status({ type: "draft", selectedRows: [record] })}>下线</Typography.Link>
                    }

                    <Typography.Link>查看访问</Typography.Link>
                    <Popconfirm
                        title="确认删除？"
                        onConfirm={() => confirm([record])}
                        onCancel={cancel}
                        okText="确认"
                        cancelText="取消"
                    >
                        <Typography.Link>删除</Typography.Link>
                    </Popconfirm>

                </Space>
            ),
        },
    ];
    // 定义被选中表格的数据
    const [selectedRows, setSelectedRows] = useState<DataType[]>([]);
    // 选中表格事件
    const rowSelection = {
        // 被选中表格的数据
        onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
            setSelectedRows(selectedRows)
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record: DataType) => ({
            disabled: record.name === 'Disabled User', // Column configuration not to be checked
            name: record.name,
        }),
    };
    // 设置空状态
    const customizeRenderEmpty = () => (
        //这里面就是我们自己定义的空状态
        <div style={{ textAlign: 'center' }}>
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={"暂无数据"} />
        </div>
    );
    // 点击刷新按钮刷新数据
    function refresh() {
        getData({ page, pageSize })
    }
    // 删除数据
    async function delData(options: Options) {
        await store.PageModel.deletePageData({ id: options.id })
        getData({ page, pageSize })
    }
    // 确认删除
    function confirm(data: Array<IPageObject>) {
        data.forEach(item => {
            delData({ id: item.id })
        })
    }
    // 取消删除
    function cancel() {
        // console.log();
        // message.error('Click on No');
    }
    // 渲染页面
    return <div className="page">
        <div className={PageLess.header}>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                onFinish={onFinish}
            >
                <Row gutter={24}>{getFields()}</Row>
                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button
                            style={{ margin: '0 8px' }}
                            onClick={() => {
                                // 重置输入框的值
                                form.resetFields();
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>
        </div>
        <div className="main">
            <div className="search-result-list">
                <div className={PageLess.table}>
                    <div className={PageLess.tableAction}>
                        <TableAction selectedRows={selectedRows} pathname={window.location.pathname} refresh={refresh} confirm={confirm} status={status} />
                    </div>
                    <ConfigProvider renderEmpty={customizeRenderEmpty} locale={zhCN}>
                        <Table
                            rowKey="id"
                            rowSelection={{
                                ...rowSelection,
                            }}
                            columns={columns}
                            dataSource={data}
                            pagination={{
                                total, pageSizeOptions: ["8", "12", "24", "36"], pageSize, showSizeChanger: true, showTotal: (total) => { return `共${total}条`; }, onChange: (page, pageSize) => {
                                    setPageSize(pageSize!)
                                    getData({ page, pageSize, ...value })
                                }
                            }}
                        />
                    </ConfigProvider>
                </div>
            </div>
        </div>
    </div>
}
export default observer(Page)

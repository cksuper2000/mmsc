import React, { useEffect, useState } from 'react'
import { observer } from "mobx-react-lite"
import Edit from "for-editor"
import {
    Button, Input, Menu,
    Dropdown, Typography, Popconfirm,
    message,
    Drawer, Form, Col, Row, Select, DatePicker, Space, InputNumber
} from "antd"
import { EllipsisOutlined, CloseOutlined, FileImageOutlined } from "@ant-design/icons"
import EditorLess from "./index.less"
import EditorPage from '@/components/EditorPage'
import FileView from "@/components/FileView"
import useStore from '@/context/useStore'
import { RouteComponentProps } from "react-router-dom"
import { makeHtml, makeToc } from '@/utils/markdown'
interface Props extends RouteComponentProps<{ id: string }> {
}

let str = `
# 欢迎使用 Wipi Markdown 编辑器

> * 整理知识，学习笔记
> * 发布日记，杂文，所见所想
> * 撰写发布技术文稿（代码支持）

## 什么是 Markdown

Markdown 是一种方便记忆、书写的纯文本标记语言，用户可以使用这些标记符号以最小的输入代价生成极富表现力的文档：譬如您正在阅读的这份文档。它使用简单的符号标记不同的标题，分割不同的段落，**粗体** 或者 *斜体* 某些文字。

### 1. 待办事宜 Todo 列表

- [ ] 支持以 PDF 格式导出文稿
- [x] 新增 Todo 列表功能

### 2. 高亮一段代码[^code]
\`\`\`python
@requires_authorization
class SomeClass:
    pass

if __name__ == '__main__':
    # A comment
    print 'hello world'
\`\`\`

### 3. 绘制表格

| 项目        | 价格   |  数量  |
| --------   | -----:  | :----:  |
| 计算机     | 1600 |   5     |
| 手机        |   12   |   12   |
| 管线        |    10    |  234  |

### 4. 嵌入网址
<iframe src="//player.bilibili.com/player.html?aid=77737877&bvid=BV1xJ411z7eS&cid=132993821&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>

\`\`\`HTML
<iframe src="//player.bilibili.com/player.html?aid=77737877&bvid=BV1xJ411z7eS&cid=132993821&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>
\`\`\`
`
const Editor = (props: Props) => {

    // 定义默认id
    const [id, setId] = useState('')
    const store = useStore()
    const [form] = Form.useForm()
    // 文章标题
    const [title, setTitle] = useState('');
    useEffect(() => {
        if (props.match.params.id) {
            setId(props.match.params.id)
            detail({ id: props.match.params.id })
        }

    }, [])
    // 编辑器的值
    const [value, setValue] = useState(str);
    const [detailData, setDetail] = useState({})
    // 请求详情数据
    async function detail(options: any) {
        let result = await store.PageModel.pageServer(options);

        setDetail(result)
        // 更改标题
        setTitle(result.name);
        // 更改内容
        setValue(result.content);
        form.setFieldsValue({
            ...form.getFieldsValue(true),
            cover: result.cover,
            path: result.path,
            order: result.order
        })
    }
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    useEffect(() => {
        getFile(page, pageSize)
    }, [])
    // 获取文章数据
    async function getFile(page: number, pageSize: number) {
        await store.editor.getfile(page, pageSize)
    }
    // 搜索文章数据
    async function searchFile(optios: any) {
        await store.editor.serchFile({ ...optios })
    }
    // 确认按钮
    function confirm(e: any) {
        console.log(e);
        message.success('Click on Yes');
    }
    // 取消按钮
    function cancel(e: any) {
        console.log(e);
        message.error('Click on No');
    }
    // 定义抽屉的初始状态
    const [visible, setVisible] = useState(false);
    // 定义图片抽屉的显示隐藏
    const [imgVisible, setImgVisible] = useState(false)
    // 展示抽屉
    function showDrawer() {
        setVisible(true)
    };
    // 隐藏抽屉
    function onClose() {
        setVisible(false)
    };
    function onChange(value: number) {
        console.log('changed', value);
    }
    const onFinish = (values: any) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <EditorPage
            id={id}
            title={title}
            titleChange={(e) => { setTitle(e.target.value) }}
            value={value}
            editorChange={(val) => setValue(val)}
            fn={() => { setVisible(true) }}
            submit={async () => {
                if (!form.getFieldsValue(true).path) {
                    return message.warning("请输入页面路径")
                }
                if (id) {
                    await store.PageModel.editPageData({ ...detailData, ...form.getFieldsValue(true), name: title })
                } else {
                    await store.PageModel.createPageData({ content: str, ...form.getFieldsValue(true), html: makeHtml(str), name: title, status: "publish", toc: JSON.stringify(makeToc(makeHtml(str))) })
                }
            }}
        >
            <Drawer
                title="页面属性"
                width={480}
                onClose={onClose}
                visible={visible}
                bodyStyle={{ paddingBottom: 80 }}
                footer={<Button type="primary" htmlType="submit" onClick={() => {
                    setVisible(false)
                }}>
                    确认
                </Button>}
                footerStyle={{ display: "flex", justifyContent: "flex-end" }}
            >
                <Form
                    name="basic"
                    labelCol={{ span: 28 }}
                    wrapperCol={{ span: 36 }}
                    initialValues={{  order: 0 }}
                    autoComplete="off"
                    colon={false}
                    form={form}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label="封面"
                        name="cover"
                    >
                        <Input placeholder={"请输入页面封面"} addonAfter={<FileImageOutlined onClick={() => setImgVisible(true)} />} />
                    </Form.Item>
                    <Form.Item
                        label="路径"
                        name="path"
                    >
                        <Input placeholder={"请配置页面路径"} onChange={(e) => form.setFieldsValue({ ...form.getFieldsValue(), path: e.target.value })} />
                    </Form.Item>
                    <Form.Item
                        label="顺序"
                        name="order"
                    >
                        <InputNumber />
                    </Form.Item>
                </Form>
            </Drawer>

            <FileView imgVisible={imgVisible} fileList={store.editor.fileList} page={page} pageSize={pageSize} onChange={(page, pageSize) => {
                setPage(page)
                setPageSize(pageSize!)
                getFile(page, pageSize!)
            }}
                hanldImage={() => { }}
                onClose={() => { setImgVisible(false) }}
                onFinish={(option) => {
                    searchFile({ page, pageSize, originalname: option.originalname, type: option.type })
                }}
                leng={store.editor.leng}
            />
        </EditorPage >
    )
}
export default observer(Editor)
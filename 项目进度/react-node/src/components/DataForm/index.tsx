
import React, { useEffect } from 'react'
import {Tabs,Form, Input, Button}from 'antd';
import { ISetting } from '@/types';
import useStore from '@/context/useStore';


interface Iprops {
  formData:Partial<ISetting>
}
export default function DataForm({formData}:Iprops) {
  const [form] = Form.useForm();
  const store = useStore();
  useEffect(()=>{
    form.setFieldsValue({
      baiduAnalyticsId:formData.baiduAnalyticsId,
      googleAnalyticsId:formData.googleAnalyticsId
    })
  },[formData]);
  const onFinish = (data:ISetting)=>{
    store.setting.saveSetting(data);
  }
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={onFinish}
    >
      <Form.Item 
        label="百度统计"
        name="baiduAnalyticsId"
      >
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item 
        label="谷歌分析"
        name="googleAnalyticsId"
      >
        <Input placeholder="请输入谷歌分析id" />
      </Form.Item>
      <Form.Item>
        <Button type="primary">保存</Button>
      </Form.Item>
    </Form>
  )
}
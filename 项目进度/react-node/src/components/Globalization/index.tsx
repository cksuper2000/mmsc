import React, {useEffect, useRef, useState} from 'react';
import { Tabs ,Button,Modal ,Form,Input} from 'antd';
import MonacoEditor from '@/components/MonacoEditor';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { confirm } = Modal;
const { TabPane } = Tabs;
interface IProps {
  value:string
}

function Globalization(props:IProps) {
  const [panes,setPanes] = useState(JSON.parse(props?.value??"{}"));
  //对话框的显示隐藏
  const [value,setValue] = useState<string>('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  //控制当前选中
  const [activeKey,setActiveKey] = useState(panes);
  const onChange = (activeKey:string)=>{
    setActiveKey(activeKey);
  }
  //添加标签
  const add = (key:any,action:string) => {
    if(action==='remove'){
      showConfirm(key);
    }else {
      setIsModalVisible(true);
    }
  };
  const handleOk = ()=>{
    //添加标签
    setPanes({
      ...panes,
      [value]:{}
    });
    //关闭对话框
    setIsModalVisible(false);
  }
  function showConfirm(key:string) {
    confirm({
      title: '确认删除',
      icon: <ExclamationCircleOutlined />,
      onOk() {
        let newdata:{[index:string]:any} = {};
        Object.entries(panes).forEach((item)=>{
          if(item[0]!==key){
            newdata[item[0]]=item[1];
          }
        })
        console.log(newdata);
        setPanes(newdata);
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  return (
    <div
    >
      <Tabs
        type="editable-card"
        style={{height:"100%"}}
        onChange={onChange}
        onEdit={add}
      >
        {Object.keys(panes).map(pane => (
          <TabPane tab={pane} key={pane} style={{width:"100%",height:"680px"}}>
            <MonacoEditor value={panes[pane]}/>
          </TabPane>
        ))}
      </Tabs>
      <Button type="primary">保存</Button>
      <Modal closeIcon={<></>} visible={isModalVisible} onOk={handleOk} >
        <Input value={value} onChange={(e:React.ChangeEvent<HTMLInputElement>)=>setValue(e.target.value)}/>
        <h3>请输入语言名称（英文）</h3>
      </Modal>
    </div>
  );
}
 
export default Globalization;
import React, { useEffect, useRef, useState } from 'react';
import { Form, Row, Col, Input, Button, Card, Pagination, Drawer, message, Popconfirm } from 'antd';
const { Meta } = Card;
import Img from "@/components/Imgg"
import moment from 'moment';
import useStore from '@/context/useStore';
import { FileObject } from '@/types/file';
import copy from 'copy-to-clipboard';
import styles from "./index.less";

interface Iproos{
    source?:string,
    fun?:Function
}

const File: React.FC<Iproos> = ({source,fun}) => {
    const store = useStore();
    const [fileItem, setFileItem] = useState<Partial<FileObject>>({});
    const [visible, setVisible] = useState(false);  //控制抽屉的显示与隐藏
    const [originalname, setOriginalname] = useState('');
    const [type, setType] = useState('');
    const [fileList, setFileList] = useState<Array<FileObject>>([])
    const [fileNum, setFileNum] = useState(1);
    const [form] = Form.useForm();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    async function File(page?: number,pageSize?:number,originalname?: string, type?: string ) {
        let res = await store.file.Img(page!, pageSize!, originalname!, type!);
        setFileList(i => i = res.fileList);
        setFileNum(v => v = res.fileNum);
    };
    useEffect(() => {
        File(page,pageSize)
    }, [page,pageSize])
    function pageChange(num: number,numSize:number) {
        File(num,pageSize,originalname,type);
        setPage(num);
        setPageSize(numSize);
        }
        const onClose = () => {
            setVisible(false);
        };
        //搜索事件
        async function inn(a: any) {
            let { type, originalname } = a;
            File(1,pageSize,originalname,type);
            setPage(i=>i=1);
            setOriginalname(i=>i=originalname);
            setType(i=>i=type);
        }
        function confirm() {
            store.file.Dele(fileItem.id!);
            message.success('Click on Yes');
        };

        return (
            <main>
                <Drawer
                    title="文件信息"
                    placement="right"
                    closable={true}
                    onClose={onClose}
                    visible={visible}
                    destroyOnClose={true}
                    width={700}
                >
                    {
                        visible && <Img> <div className={styles.drawer}>
                            
                            <img src={fileItem.url} alt="" />
                            
                            <div>
                                <span>文件名称:</span>
                                <span>{fileItem.originalname}</span>
                            </div>
                            <div>
                                <span>存储路径:</span>
                                <span>{fileItem.filename}</span>
                            </div>
                            <div>
                                <div>
                                    <span>文件类型:</span>
                                    <span>{fileItem.type}</span>
                                </div>
                                <div>
                                    <span>文件大小:</span>

                                    <span>{fileItem.size && fileItem.size / 1024 < 1024 ?
                                        String(fileItem.size / 1024).split(".")[0] + "." + String(fileItem.size / 1024).split(".")[1].slice(0, 2) + "KB"
                                        : String(fileItem.size! / 1024 / 1024).split(".")[0] + "." + String(fileItem.size! / 1024 / 1024).split(".")[1].slice(0, 2) + "MB"}
                                    </span>
                                </div>
                                <div>
                                    <span>访问链接:</span>
                                    <div>
                                        <input type="text" disabled={true} value={fileItem.url} />
                                        <span style={{ color: "#09f" }} onClick={() => {
                                            copy(fileItem.url!);
                                            message.success("已复制内容到剪切板!")
                                        }}>复制</span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <Button type="primary" onClick={() => setVisible(false)}>关闭</Button>
                                <Popconfirm
                                    title="确定要删除吗?"
                                    onConfirm={confirm}
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <Button danger>删除</Button>
                                </Popconfirm>
                            </div>
                        </div></Img>
                    }


                </Drawer>
                <div className={styles.inp}>
                    <Form
                        form={form}
                        name="advanced_search"
                        className="ant-advanced-search-form"
                        onFinish={inn}
                    >
                        <Row gutter={24} >
                            <Form.Item
                                style={{ marginLeft: "12px", marginRight: "12px" }}
                                name={`originalname`}
                                label={`文件名称:`}
                            >
                                <Input placeholder="请输入文件名称" />
                            </Form.Item>
                            <Form.Item
                                style={{ marginLeft: "12px", marginRight: "12px" }}
                                name={`type`}
                                label={`文件类型:`}
                            >
                                <Input placeholder="请输入文件类型" />
                            </Form.Item>
                        </Row>
                        <Row>
                            <Col span={24} style={{ textAlign: 'right' }}>
                                <Button type="primary" htmlType="submit">
                                    搜索
                                </Button>
                                <Button
                                    style={{ margin: '0 8px' }}
                                    onClick={() => {
                                        form.resetFields();
                                    }}
                                >
                                    重置
                                </Button>

                            </Col>
                        </Row>
                    </Form>
                </div>
                <Img>
                {
                    source ? <div>
                        <input type="file" name="file"/>
                    </div> : null
                }
                <div className={styles.img}>
                    {
                        fileList.map((item, index) => {
                            if(source){
                                return  <Card
                                onClick={(e)=>{
                                    if(String(e.target).indexOf("HTMLDiv")!=-1){
                                        fun!(JSON.parse(JSON.stringify(item)),false)
                                    }
                                }}
                                key={item.id}
                                hoverable
                                style={{ width: "24%", minWidth: "174px", height: "275px", margin: "10px 0" }}
                                cover={<img alt="example" src={item.url} />}
                            >
                                <Meta title={item.originalname} description={`上传于${moment(item.createAt).format('YYYY-MM-DD HH:mm:ss')}`} />
                            </Card> 
                            }else{
                                return <Card
                                onClick={() => {
                                    setFileItem(i => i = item)
                                    setVisible(true)

                                }}
                                key={item.id}
                                hoverable
                                style={{ width: "24%", minWidth: "174px", height: "275px", margin: "10px 0" }}
                                cover={<img alt="example" src={item.url} />}
                            >
                                <Meta title={item.originalname} description={`上传于${moment(item.createAt).format('YYYY-MM-DD HH:mm:ss')}`} />
                            </Card>
                            }
                           
                        })
                    }
                    

                </div>
                <div className={styles.pageindex}>
                        <span>{`共${fileNum}条`}</span>
                        <Pagination pageSizeOptions={["8", `12`, `26`, `34`]} defaultPageSize={pageSize} current={page} total={fileNum} showSizeChanger={true} onChange={(i,v) => pageChange(i,v!)} />
                    </div>
                </Img>

            </main>
        )

    }

export default File
import React from 'react'
import {Row, Col } from 'antd';
import styles from './index.less';

interface Iprops {
  logo:React.ReactNode
}
const LoginLayout:React.FC<Iprops> = ({children,logo})=> {
  return (
    <Row className={styles.loginLayout}>
      <Col xs={{span:0}} sm={0} md={12} className={styles.images}>
        {logo}
      </Col>
      <Col xs={{span:24}} sm={24} md={12} className={styles.form}>
        {children}
      </Col>
    </Row>
  )
}
export default LoginLayout;
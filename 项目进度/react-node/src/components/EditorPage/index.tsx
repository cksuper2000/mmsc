import React from 'react'
import {CloseOutlined} from '@ant-design/icons';
import {PageHeader,Dropdown,Button,Input,Menu,message} from 'antd';
import Editor from 'for-editor'
import useStore from '@/context/useStore';
import { useHistory } from 'react-router-dom'
interface Iprops {
  /**标题的内容 */
  title:string
  /**标题改变的回调 */
  titleChange:(e:React.ChangeEvent<HTMLInputElement>)=>void
  /**点击发布 */
  submit:()=>void
  /**编辑器的内容 */
  value?:string
  /**编辑器改变触发的回调 */
  editorChange?:(str:string)=>void
  /**点击设置的回调 */
  fn:()=>void
  editorHide?:boolean
  id:string
}
const EditorPage:React.FC<Iprops> = ({id,title,titleChange,submit,children,value,editorChange,fn,editorHide=true})=> {
  const store = useStore();
  const history = useHistory();
  const setting = [{
    title:'查看',
    disabled:id?false:true
  },{
    title:'设置',
    disabled:false,
    fn
  },{
    title:"保存草稿",
    disabled:false
  },{
    title:"删除",
    disabled:id?false:true,
    fn: async()=>{
      //删除
      let result = await store.category.removeArticle(id);
      message.success("删除成功");
      if(result){
        history.push('/article');
      }
    }
  }];
  const menu = (
    <Menu>
      {setting.map((item,index)=>(
        <Menu.Item key={index} onClick={item.fn} disabled={item.disabled}>
          {item.title}
        </Menu.Item>
      ))}
    </Menu>
  );
  const toolbar = {
    img: true, // 图片
    link: true, // 链接
    code: true, // 代码块
    preview: true, // 预览
    expand: true, // 全屏
    /* v0.0.9 */
    save: true, // 保存
    /* v0.2.3 */
    subfield: true, // 单双栏模式
  }
  return (
    <div>
      <header style={{height:"64px"}}>
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            backIcon={<Button size="small" icon={<CloseOutlined/>}></Button>}
            onBack={() => window.history.back()}
            subTitle={<Input placeholder="请输入文章标题" 
              value={title}
              onChange={titleChange}
              bordered={false} style={{
              borderBottom:"1px solid #ccc",
              width:300,
              background:"#fff",
            }}/>}
            extra={[
              <Button type="primary" onClick={submit} key="1">
                发布
              </Button>,
              <Dropdown overlay={menu} placement="bottomRight" key="2">
                <Button type="text">
                  ...
                </Button>
              </Dropdown>
            ]}
          >
          </PageHeader>
        </div>
      </header>
      {editorHide?<Editor
        value={value}
        toolbar={toolbar}
        subfield={true}
        preview={true}
        onChange={editorChange}
      />:null}
      {children}
    </div>
  )
}
export default EditorPage;

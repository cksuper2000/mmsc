import React from 'react'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  GithubOutlined,
} from '@ant-design/icons';
import { Layout ,Dropdown,Avatar,Menu} from 'antd'
import { Iroute } from '@/routerList';
import { useHistory } from 'umi';
import { removeCookie } from '@/utils/cookie';
const { Header } = Layout;
interface Iprops{
  collapsed:boolean
  toggle():void
  login:Iroute
  name:string
  avatar:string
}

const MyHeader:React.FC<Iprops> = ({collapsed,toggle,login,name,avatar})=> {
  const history = useHistory();
  const loginMenu = (
    <Menu>
      {
        login.map((item,index)=>(
          <Menu.Item key={index}>
            <span onClick={()=>{if(item.title==="退出登陆")localStorage.clear();removeCookie();history.push('/login?from='+location.pathname)}}>
              {item.title}
            </span>
          </Menu.Item>
        ))
      }
    </Menu>
  )
  return (
    <Header className="site-layout-background">
      {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
        className: 'trigger',
        onClick: toggle,
      })}
      <div className="login">
        <GithubOutlined style={{fontSize:"23px"}}/>
        <Dropdown overlay={loginMenu} placement="bottomCenter">
          <div className="userName">
            <Avatar size="small" icon={<img src={avatar}/>}/>
            <span style={{marginLeft:"8px",fontSize:"16px",cursor:"pointer"}}>Hi,{name}</span>
          </div>
        </Dropdown>
      </div>
    </Header>
  )
}
export default MyHeader
import React from 'react'
// 引入样式
import PageLess from "@/pages/page/index.less"
import { Button, Tooltip, Popconfirm, message } from 'antd';
import { PlusOutlined, RedoOutlined } from '@ant-design/icons';
// 引入发布下线接口
import { IPageStatus } from "@/pages/page"
// 引入通过拒绝接口
import { ICommentStatus } from "@/pages/comment"
import { history } from "umi"
// 定义上线或下线接口
interface IProps {
    selectedRows: any,
    pathname: string,
    refresh: () => void,
    confirm: (data: any) => void,
    status: (arg0: any) => void,
}
const TableAction: React.FC<IProps> = (props) => {
    // 取消删除
    function cancel() {
        // message.info('取消删除');
    }
    return (
        <>
            {
                props.pathname == "/page" ? <> <div className={PageLess.left}>
                    {
                        props.selectedRows.length > 0 && <><Button onClick={() => props.status({ type: "publish", selectedRows: props.selectedRows })}>发布</Button>
                            <Button onClick={() => props.status({ type: "draft", selectedRows: props.selectedRows })}>下线</Button>
                            <Popconfirm
                                title="确认删除？"
                                onConfirm={() => props.confirm(props.selectedRows)}
                                onCancel={cancel}
                                okText="确认"
                                cancelText="取消"
                            >
                                <Button danger>删除</Button>
                            </Popconfirm>
                        </>
                    }
                </div>
                    <div className={PageLess.right}>
                        <Button type="primary" icon={<PlusOutlined />} onClick={() => history.push("/page/editor")}>新建</Button>
                        <Tooltip title="刷新">
                            <RedoOutlined style={{ cursor: "pointer", fontSize: "16px" }} rotate={-90} onClick={() => {
                                props.refresh()
                            }} />
                        </Tooltip>
                    </div></> : <> <div className={PageLess.left}>
                        {
                            props.selectedRows.length > 0 && <><Button onClick={() => props.status({ status: true, selectedRows: props.selectedRows })}>通过</Button>
                                <Button onClick={() => props.status({ status: false, selectedRows: props.selectedRows })}>拒绝</Button>
                                <Popconfirm
                                    title="确认删除？"
                                    onConfirm={() => props.confirm(props.selectedRows)}
                                    onCancel={cancel}
                                    okText="确认"
                                    cancelText="取消"
                                >
                                    <Button danger>删除</Button>
                                </Popconfirm>
                            </>
                        }
                    </div>
                    <div className={PageLess.right}>
                        <Button type="primary"></Button>
                        <Tooltip title="刷新">
                            <RedoOutlined style={{ cursor: "pointer", fontSize: "16px" }} rotate={-90} onClick={() => {
                                props.refresh()
                            }} />
                        </Tooltip>
                    </div></>
            }

        </>
    )
}
export default TableAction
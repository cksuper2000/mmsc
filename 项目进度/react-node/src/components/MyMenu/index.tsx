import React, { useState } from 'react'
import {
  PlusOutlined,
} from '@ant-design/icons';
import {Button,Layout,Menu,Dropdown} from 'antd';
import { Iroute } from '@/routerList';
import { useHistory } from 'umi';
const { Header, Content, Sider } = Layout;
const { SubMenu } = Menu;

interface Iprops {
  collapsed:boolean
  title:Iroute
  pathname:string
  newConstruction:Iroute
}
const MyMenu:React.FC<Iprops> = ({collapsed,title,pathname,newConstruction})=> {
  const [openKeys,setOpenKeys] = useState<Array<string>>([]);
  const routerColor:Array<string> = [];
  const history = useHistory();
  for(let i=0;i<title.length;i++){
    if(!title[i].children&&pathname==title[i].to){
      routerColor.push(title[i].to);
    }else if(title[i].children?.length&&title[i].children?.some(val=>val.to===pathname)){
      title[i].children?.forEach((item,index)=>{
        if(item.to===title[i].to) routerColor.push(title[i].to);
        if(item.to===pathname) routerColor.push(title[i].to+"/"+index);
      })
    }
  }
  const menu = (
    <Menu>
      {
        newConstruction.map(item=>(
          <Menu.Item key={item.to}>
            <a href={item.to}>
              {item.title}
            </a>
          </Menu.Item>
        ))
      }
    </Menu>
  );
  return (
    <Sider 
      width="200px"
      trigger={null} 
      collapsible 
      collapsed={collapsed} 
      style={{height:"100%",overflowY:"scroll",maxWidth:"200px",minWidth:"200px"}}
    >
      {
        !collapsed?
        <div className="logo" style={{margin:"1rem 0 .5rem 0",fontSize:"1.2rem",display:"flex",justifyContent:"center",alignItems:"center"}}>
          <img 
            style={{width:"32px",height:"32px",borderTopRightRadius:"var(--border-radius)",objectFit:"cover",borderTopLeftRadius:"var(--border-radius)"}}
            src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" alt=""
          />
          <span style={{color:"#fff",marginLeft:"4px"}}>管理后台</span>
        </div>:null
      }
      {!collapsed?
        <div style={{padding:"0 var(--aside-padding-horizontal)",margin:"12px 0",boxSizing:"border-box"}}>
          <Dropdown overlay={menu} placement="bottomCenter">
            <Button type="primary" style={{width:"100%"}} icon={<PlusOutlined />} size="large">
              新建
            </Button>
          </Dropdown>
        </div>
      :null}
      {/* 左边的路由渲染 */}
      <Menu
        mode="inline"
        theme="dark"
        defaultSelectedKeys={routerColor}
        openKeys={openKeys}
      >
        {collapsed?
          <Menu.Item
            key={"av"}
            icon={<img 
              style={{transform:"translateX(-25%)",lineHeight:"0",padding:"0",width:"32px",height:"32px",borderTopRightRadius:"var(--border-radius)",objectFit:"cover",borderTopLeftRadius:"var(--border-radius)"}}
              src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" alt="" 
          />}
        >
        </Menu.Item>:null}
        {collapsed?
          <Menu.Item 
            key='newConstruction'
            icon={
              <Dropdown overlay={menu} placement="bottomLeft">
                <Button style={{transform:"translateX(-25%)",lineHeight:"0",padding:"0"}} type="primary" icon={<PlusOutlined/>} size="large">
                </Button>
              </Dropdown>} >
        </Menu.Item>:null}
        {
          title.map((item)=>{
            if(!item.children){
              return (
                <Menu.Item 
                  style={{background:"rgb(0, 21, 41)"}}
                  icon={item.icon}
                  key={item.to}
                  onClick={()=>history.push(item.to)}
                >
                {item.title}
                </Menu.Item>
              )
            }else {
              return (
                <SubMenu 
                  icon={item.icon}
                  key={item.to}
                  title={item.title}
                  onTitleClick={(key)=>{setOpenKeys((prev=>{
                    let index = prev.findIndex(item=>item===key.key);
                    if(index===-1){
                      return [...prev,key.key]
                    }else {
                      prev.splice(index,1);
                      return [...prev];
                    }
                  }))}}
                >
                  {
                    item.children.map((val,index)=>(
                      <Menu.Item 
                        key={item.to+"/"+index}
                        style={{background:"#000c17"}}
                        icon={val.icon}
                        onClick={()=>{history.push(val.to);setOpenKeys((prev)=>{
                          let index = prev.findIndex(ele=>item.to===ele);
                          prev.splice(index,1);
                          return [...prev];
                        })}}
                      >{val.title}</Menu.Item>
                    ))
                  }
                </SubMenu>
              )
            }
          })
        }
      </Menu>
    </Sider>
  )
}
export default MyMenu;
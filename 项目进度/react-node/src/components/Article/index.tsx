import React from 'react';
import styles from './index.less';
import { Row, Col,Card,Input,Form ,Button,Popconfirm,message } from 'antd';
import { Ilabel, Idata } from '@/types'
import { useState } from 'react';

interface Iprops {
  onFinish(values:Idata):void
  List:Ilabel
  onDelete:(id:string)=>void
}
const Article:React.FC<Iprops> = ({onFinish,List,onDelete})=> {
  let [form] = Form.useForm();
  const [ visible,setVisible ] = useState(false);
  return (
    <Row>
      <Col xs={24} sm={24} md={9} lg={9}>
      <div className={styles.siteCardBorderLessWrapper}>
        <Card title="添加分类" bordered={false}>
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 0 }}
          wrapperCol={{ span: 27 }}
          initialValues={{ remember: true }}
          onFinish={(values:Idata)=>{onFinish(values);form.setFieldsValue({
            label:"",
            value:""
          })}}
        >
          <Form.Item
            name="label"
            rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input placeholder="输入分类名称"/>
          </Form.Item>
          
          <Form.Item
            name="value"
            rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input placeholder="输入分类值(请输入英文,作为路由使用)"/>
          </Form.Item>

          <Form.Item>
            <div style={{display:"flex",justifyContent:"space-between"}}>
              <div style={{display:"flex"}}>
                <Button type="primary" htmlType="submit">
                  保存
                </Button>
                {
                  visible&&<Button type="dashed" onClick={()=>{setVisible(false);form.setFieldsValue({label:'',value:''})}}>
                  返回添加
                </Button>
                }
              </div>
              <div>
              {
                visible&&
                <Popconfirm
                  title="确认删除这个分类?"
                  onConfirm={()=>{onDelete(form.getFieldValue("id"));setVisible(false);form.setFieldsValue({label:'',value:'',id:''})}}
                  okText="确认"
                  cancelText="取消"
                >
                <Button type="primary" danger ghost>
                删除
              </Button>
              </Popconfirm>
              }
              </div>
            </div>
          </Form.Item>
        </Form>
        </Card>
      </div>
      </Col>
      <Col xs={24} sm={24} md={15} lg={15}>
      <div className={styles.siteCardBorderLessWrapper}>
        <Card title="所有分类" bordered={false}>
          <ul>
            {
              List.map(item=>(
                <li 
                  key={item.id}
                  onClick={()=>{form.setFieldsValue({label:item.label,value:item.value,id:item.id});setVisible(true)}}
                >{item.label}</li>
              ))
            }
          </ul>
        </Card>
      </div>
      </Col>
    </Row>
  )
}
export default Article;
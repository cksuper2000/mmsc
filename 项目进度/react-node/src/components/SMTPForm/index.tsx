
import React, { useEffect } from 'react'
import {Tabs,Form, Input, Button}from 'antd';
import { ISetting } from '@/types';
import useStore from '@/context/useStore';


interface Iprops {
  formData:Partial<ISetting>
}
export default function SMTPForm({formData}:Iprops) {
  const [form] = Form.useForm();
  const store = useStore();
  useEffect(()=>{
    form.setFieldsValue({
      smtpHost:formData.smtpHost,
      smtpPort:formData.smtpPort,
      smtpUser:formData.smtpUser,
      smtpPass:formData.smtpPass,
      smtpFromUser:formData.smtpFromUser
    })
  },[formData]);
  const onFinish = (data:ISetting)=>{
    store.setting.saveSetting(data);
  }
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={onFinish}
    >
      <Form.Item 
        label="SMTP地址"
        name="smtpHost"
      >
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item 
        label="SMTP 端口（强制使用 SSL 连接）"
        name="smtpPort"
      >
        <Input placeholder="请输入谷歌分析id" />
      </Form.Item>
      <Form.Item 
        label="SMTP 用户"
        name="smtpUser"
      >
        <Input placeholder="请输入谷歌分析id" />
      </Form.Item>
      <Form.Item 
        label="SMTP 密码"
        name="smtpPass"
      >
        <Input placeholder="请输入谷歌分析id" />
      </Form.Item>
      <Form.Item 
        label="发件人"
        name="smtpFromUser"
      >
        <Input placeholder="请输入谷歌分析id" />
      </Form.Item>
      <Form.Item>
        <Button type="primary">保存</Button>
      </Form.Item>
    </Form>
  )
}
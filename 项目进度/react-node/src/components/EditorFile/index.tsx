import React,{useState} from 'react'
import { 
  Button ,
  Input ,
  Drawer,
  Form,
  Switch ,
  Select,
  message,
} from 'antd';
import { useEffect } from 'react';
import useStore from '@/context/useStore';
interface Iprops {
  /**点击确定按钮关闭 */
  hanldFile:()=>void
  /**表单 */
  form:any
  /**控制蒙版的显示隐藏 */
  visible:boolean
  /**点击确定按钮 */
  hanldOk:()=>void
  /**点击关闭的回调 */
  onClose:()=>void
  /** 图片的路径*/
  image:string
}
const EditorFile:React.FC<Iprops> = ({hanldFile,form,visible,hanldOk,onClose,image})=> {
  const store = useStore();
  
  const { categoryList ,tagsList} = store.category;

  //加载中页面
  useEffect(()=>{
    store.category.getcategory();
    store.category.gettags();
  },[])
  return (
    <Drawer
      title="文章设置"
      placement="right"
      onClose={onClose}
      visible={visible}
      width="480px"
      forceRender
      footer={<Button type="primary"
       style={{ float: "right" }}
       onClick={hanldOk}
       >确认</Button>}
    >
      <Form
        form={form}
      >
        <Form.Item
          name="summary"
          label="文章摘要"
        >
          <Input.TextArea placeholder="文章摘要" style={{
            height: "142px",
            minHeight: "142px",
            maxHeight: "186px",
            overflowY: "hidden",
            resize: "none"
          }} />
        </Form.Item>
        <Form.Item
          label="访问密码"
          name="password"
        >
          <Input.Password placeholder="输入后查看需要密码" />
        </Form.Item>
        <Form.Item
          label="付费查看"
          name="totalAmount"
        >
          <Input.Password placeholder="输入后需要支付的费用" />
        </Form.Item>
        <Form.Item
          label="开启评论"
          name="isCommentable"
          valuePropName='checked'
        >
          <Switch />
        </Form.Item>
        <Form.Item
          label="首页推荐"
          name="isRecommended"
          valuePropName='checked'
        >
          <Switch />
        </Form.Item>
        <Form.Item
          label="选择分类"
          name="category"
        >
          <Select>
            {
              categoryList.map(item => (
                <Select.Option key={item.id} value={item.id}>{item.label}</Select.Option>
              ))
            }
          </Select>
        </Form.Item>
        <Form.Item
          label="选择标签"
          name="tags"
        >
          <Select mode="multiple">
            {
              tagsList.map(item => (
                <Select.Option value={item.id} key={item.id}>{item.label}</Select.Option>
              ))
            }
          </Select>
        </Form.Item>
        <Form.Item
          label="文章封面"
        >
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "180px",
              marginBottom: "16px",
              color: "#888",
              backgroundColor: " #f5f5f5"
            }}
          >
            <img src={image} alt="预览图"
              style={{
                display: "block",
                maxWidth: "100%",
                maxHeight: "180px",
                width: "100%",
                height: "120px",
                borderTopRightRadius: "var(--border-radius)",
                borderTopLeftRadius: "var(--border-radius)",
                objectFit: "cover"
              }}
              onClick={hanldFile}
            />
          </div>
        </Form.Item>
        <Form.Item
          name="cover"
        >
          <Input placeholder="或输入外部链接" />
        </Form.Item>
        <Form.Item>
          <Button onClick={() => {
            form.setFieldsValue({
              ...form.getFieldValue,
              cover: ''
            })
          }}>移除</Button>
        </Form.Item>
      </Form>
    </Drawer>
  )
}
export default EditorFile;
import React, { useEffect, useRef } from "react";
import Viewer from 'viewerjs';
import 'viewerjs/dist/viewer.css';
let Img:React.FC=(props)=>{
    let Div=useRef<HTMLDivElement>(null);
    
    useEffect(()=>{
           let c=new Viewer(Div.current!);
           c.update();
           return ()=>{
               c.destroy()
           }
    },[props.children])

    return(
        <div ref={Div}>
            {props.children}
        </div>
    )
}
export default Img
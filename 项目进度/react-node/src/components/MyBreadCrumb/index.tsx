import React from 'react';
import {Breadcrumb} from 'antd';
import { Iroute } from '@/routerList';
import { useHistory } from 'react-router-dom'

interface Iprops {
  title:Iroute
  pathname:string
  role:string
  name:string
}
//判断是否是二级路由
// function FnBreadCrumb(router:Iroute,pathname:string,hostRouter:string):any{
//   return router!.map((val,num)=>{
//     if(hostRouter===val.to){
//       return (
//         <Breadcrumb.Item key={num}>
//         {val.title}
//         </Breadcrumb.Item>
//       )
//     }else{
//       return (
//         <>
//         {val.to===pathname?<Breadcrumb.Item key={num} >
//           {val.title}
//         </Breadcrumb.Item>:null}
//         </>
//       )
//     }
//   })
// }
const MyMyBreadCrumb:React.FC<Iprops> = ({title,pathname,role,name})=> {
  const history = useHistory();
  return (
    <Breadcrumb style={{ padding: '16px 16px',borderTop:"1px solid #eee",background:"#fff",cursor:"pointer"}}>
      <Breadcrumb.Item onClick={()=>history.push('/')}>
        {title.length&&title.filter(item=>item.to==='/')[0].title}
        {
          pathname==='/'?<div style={{margin:"12px 0 0"}}>
            <h1 style={{fontSize:"38px",lineHeight:"1.23",color:'#333',marginBottom:".5em",fontWeight:600}}>你好,{name}</h1>
            <div>您的角色:{role}</div>
          </div>:null
        }
      </Breadcrumb.Item>
      {
        title.map((item,num)=>{
          //判断是否有子路由
          if(!item.children&&item.to===pathname){
            if('/'===pathname) return
            //没有直接拼接
            return (
              <Breadcrumb.Item key={num} onClick={()=>history.push(item.to)}>
              {item.title}
              </Breadcrumb.Item>
            )
          //判断是否是二级路由
          }else if(item.children?.length&&item.children.some(val=>val.to===pathname)){
            console.log(item.children);
            // return FnBreadCrumb(item.children,pathname,item.to);
            //拿当前的和里面的二级路由对比
            return item.children!.map(val=>{
              if(item.to===val.to){
                return (
                  <Breadcrumb.Item key={num} onClick={()=>history.push(val.to)}>
                  {val.title}
                  </Breadcrumb.Item>
                )
              }else{
                return (
                  <>
                  {val.to===pathname?<Breadcrumb.Item key={num} onClick={()=>history.push(val.to)}>
                    {val.title}
                  </Breadcrumb.Item>:null}
                  </>
                )
              }
            })
          }
        })
      }
    </Breadcrumb>
  )
}
export default MyMyBreadCrumb;
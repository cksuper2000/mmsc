import React, { useEffect } from 'react'
import {Tabs,Form, Input, Button}from 'antd';
import useStore from '@/context/useStore';
import {  ISetting } from '@/types';


interface Iprops {
  formData:Partial<ISetting>
}
export default function SettingForm({formData}:Iprops) {
  const [form] = Form.useForm();
  const store = useStore();
  useEffect(()=>{
    form.setFieldsValue({
      systemUrl:formData.systemUrl,
      adminSystemUrl:formData.adminSystemUrl,
      seoKeyword:formData.seoKeyword,
      systemLogo:formData.systemLogo,
      systemFavicon:formData.systemFavicon,
      systemFooterInfo:formData.systemFooterInfo
    })
  },[formData]);
  const onFinish = (data:Partial<ISetting>)=>{
    store.setting.saveSetting(data);
  }
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={onFinish}
    >
      <Form.Item 
        label="系统地址"
        name="systemUrl"
      >
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item 
        label="后台设置"
        name="adminSystemUrl"
      >
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item 
        label="系统标题"
        name="seoKeyword"
      >
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item 
        label="Logo"
        name="systemLogo"
      >
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item 
        label="Favicon"
        name="systemFavicon"
      >
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item 
        label="页脚信息"
        name="systemFooterInfo"
      >
        <Input.TextArea style={{height:"186px"}} placeholder="input placeholder" />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">保存</Button>
      </Form.Item>
    </Form>
  )
}


import React, { useEffect } from 'react'
import {Tabs,Form, Input, Button}from 'antd';
import { ISetting } from '@/types';
import useStore from '@/context/useStore';


interface Iprops {
  formData:Partial<ISetting>
}
export default function SEOForm({formData}:Iprops) {
  const [form] = Form.useForm();
  const store = useStore();
  useEffect(()=>{
    form.setFieldsValue({
      seoKeyword:formData.seoKeyword,
      seoDesc:formData.seoDesc
    })
  },[formData]);
  const onFinish = (data:ISetting)=>{
    store.setting.saveSetting(data);
  }
  return (
    <Form
      layout="vertical"
      form={form}
      onFinish={onFinish}
    >
      <Form.Item 
        label="关键词"
        name="seoKeyword"
      >
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item 
        label="描述信息"
        name="seoDesc"
      >
        <Input.TextArea style={{height:"186px"}} placeholder="input placeholder" />
      </Form.Item>
      <Form.Item>
        <Button type="primary">保存</Button>
      </Form.Item>
    </Form>
  )
}

import React from 'react';
import {Form, Row, Col, Input,Button,Radio} from 'antd';
import styles from './index.less';
interface Iprops {
  onFinish(values:any):void
}
const ViewFrom:React.FC<Iprops> = ({onFinish})=> {
  //用户表单
  const [form] = Form.useForm();
  const inputList = [{
    title:"IP",
    place:"请输入IP",
    name:'ip'
  },{
    title:'UA',
    place:"请输入UA",
    name:'userAgent'
  },{
    title:"URL",
    place:"请输入URL",
    name:'url'
  },{
    title:"地址",
    place:"请输入地址",
    name:'address'
  },{
    title:"浏览器",
    place:"请输入浏览器",
    name:'browser'
  },{
    title:"内核",
    place:"请输入内核",
    name:'engine'
  },{
    title:"OS",
    place:"请输入OS",
    name:'os'
  },{
    title:"设备",
    place:"请输入设备",
    name:'device'
  }]
  return (
    <Form
      form={form}
      className={styles.antAdvancedSearchForm}
      layout="horizontal"
      onFinish={onFinish}
    >
      <Row gutter={24}>
        {
          inputList.map((item,index)=>(
            <Col key={index} xs={24} sm={12} md={6}>
              <Form.Item label={item.title} name={item.name}>
                <Input placeholder={item.place}/>
              </Form.Item>
            </Col>
          ))
        }
      </Row>
      <Row>
        <Col span={24} style={{ textAlign: 'right' }}>
          <Button type="primary" htmlType="submit">
            搜索
          </Button>
          <Button
            style={{ margin: '0 8px' }}
            onClick={() => {
              form.resetFields();
            }}
          >
            重置
          </Button>
        </Col>
      </Row>
    </Form>
  )
}
export default ViewFrom;
import React, {useEffect, useRef} from 'react';
import * as monaco from 'monaco-editor/esm/vs/editor/editor.api.js';

interface IProps {
  style?:any
  value:any
  onChange?:any
  fontSize?:any
  monacoOptions?:any
  language?:any,
}
function MonacoEnvironment(props:IProps) {
  const {
    style = { // dom节点样式
      height:"680px",
      width: '100%',
      border: '1px solid #eee',
    },
    value = '', // 代码文本
    onChange = () => { // 改变的事件

    },
    fontSize = 14, // 代码字体大小
    monacoOptions = {}, // monaco 自定义属性
    language = 'json', // 语言 支持 js ts sql css json html等
  } = props;
  const editOrRef = useRef<HTMLDivElement>(null);
  const ThisEditor = useRef<HTMLDivElement>(null);
  useEffect(() => {
    (ThisEditor.current as any) = monaco.editor.create(editOrRef.current!, {
      value: JSON.stringify(value,null,1) || '',
      language,
      theme: "vs",
      fontSize: fontSize + 'px',
      minimap: { // 关闭代码缩略图
        enabled: true,
      },
      ...monacoOptions,
    });
    
    // ThisEditor.current.onDidChangeModelContent((e) => {
    //   let newValue = ThisEditor.current.getValue();
    //   console.log(newValue);
    //   onChange(newValue);
    // });
    // return () => {
    //   ThisEditor.current.dispose();
    //   ThisEditor.current = undefined; // 清除编辑器对象
    // }
  }, []);
  useEffect(() => {
    // if (ThisEditor.current) {
    //   ThisEditor.current.updateOptions({
    //     fontSize: fontSize + 'px',
    //   })
    // }
  }, [fontSize]);
 
  return (
    <div
      style={style}
      ref={editOrRef}
    >
    </div>
  );
}
 
export default MonacoEnvironment;
import React from 'react'
import Viewer from 'viewerjs';
import { createRef ,useEffect} from 'react';
import 'viewerjs/dist/viewer.css';


export default function Myck(props:any) {
  const dom = createRef<HTMLDivElement>();
  useEffect(()=>{
    const viewer = new Viewer(dom.current!);
      viewer.update();  //每当props的children里的图片更新时，更新图片查看器的实例
    return ()=>{
      viewer.destroy();  //卸载查看器
    }
  },[props.children])
  return (
    <div ref={dom}>{props.children}</div>
  )
}
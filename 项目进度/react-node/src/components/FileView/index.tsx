import React from 'react'
import { IFile, IFileDatum } from '@/types/file';
import {
  Card,
  Pagination,
  message,
  Row,
  Col,
  Upload,
  Drawer,
  Button,
  Form,
  Input
} from 'antd';
import styles from './index.less';
import Picture from '@/components/Picture';


const { Meta } = Card;
interface Iprop {
  fileList:Array<IFileDatum>
  page:number
  pageSize:number
  onChange:(page:number,pageSize?:number)=>void
  hanldImage:(imgurl:string)=>void
  /**上传图片的显示隐藏 */
  imgVisible:boolean
  /** 点击关闭页面的显示隐藏*/
  onClose:()=>void
  onFinish:(form:any)=>void
  leng:number
}
const FileView:React.FC<Iprop>=({fileList,page,pageSize,onChange,hanldImage,imgVisible,onClose,onFinish,leng})=> {
  const [form] = Form.useForm();
  return (
    <Drawer
      title="文件选择"
      placement="right"
      onClose={onClose}
      visible={imgVisible}
      width="786px"
    >
      <Form
        form={form}
        layout="horizontal"
        className={styles.antAdvancedSearchForm}
        style={{
          padding: "24px 12px",
          marginBottom: "16px"
        }}
        onFinish={onFinish}
      >
        <Row gutter={24}>
          <Col xs={24} sm={12} md={6}>
            <Form.Item
              name="originalname"
              label="文件名称"
            >
              <Input placeholder="请输入文章标题" />
            </Form.Item>
          </Col>
          <Col xs={24} sm={12} md={6}>
            <Form.Item
              name="type"
              label="文件类型"
            >
              <Input placeholder="请输入文章标题" />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24} style={{ textAlign: 'right' }}>
            <Button 
              type="primary" 
              htmlType="submit"
            >
              搜索
            </Button>
            <Button
              style={{ margin: '0 8px' }}
              onClick={() => {
                form.resetFields();
              }}
            >
              重置
            </Button>
          </Col>
        </Row>
      </Form>
      <div className={styles.upload}>
        <div style={{
          display: "flex",
          justifyContent: "flex-end",
          marginBottom: "16px"
        }}>
          <Upload >
            <Button>上传文件</Button>
          </Upload>
        </div>
        <Picture>
          <div className={styles.fileList}>
            {
              fileList && (fileList as unknown as Array<IFileDatum>).map((item) => {
                return <Card
                  key={item.id}
                  hoverable
                  style={{
                    width: "25%",
                    paddingLeft: "8px",
                    paddingRight: "8px",
                    outline: "none",
                    border: "none"
                  }}
                  cover={
                    <img alt="example" style={{
                      height: "120px"
                    }} src={item.url}
                    />
                  }
                >
                  <div onClick={() => hanldImage(item.url)}>
                    <Meta
                      title={item.originalname}
                    />
                  </div>
                </Card>
              })
            }
            <div className={styles.pageindex}>
              <span>{`共${leng}条`}</span>
              <Pagination
                total={leng}
                responsive={true} pageSizeOptions={["8", `12`, `26`, `34`]}
                defaultPageSize={pageSize}
                current={page}
                showSizeChanger={true}
                onChange={onChange}
              />
            </div>
          </div>
        </Picture>
      </div>
    </Drawer>
  )
}
export default FileView;
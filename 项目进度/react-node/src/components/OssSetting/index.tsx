import React from 'react'
import { Alert } from 'antd';
import MonacoEditor from '../MonacoEditor';

interface Iprops {
  value:string
}
export default function OssSetting({ value }:Iprops) {
  return (
    <div>
      <Alert
        message="说明"
        description={"请在编辑器中输入您的 oss 配置，并添加 type 字段区分{\"type\":\"aliyun\",\"accessKeyId\":\"\",\"accessKeySecret\":\"\",\"bucket\":\"\",\"https\":true,\"region\":\"\"}"}
        type="info"
        showIcon
      />
      {value?<MonacoEditor value={JSON.parse(value)}/>:null}
    </div>
  )
}

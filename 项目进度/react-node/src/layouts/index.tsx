import React,{ useState,FC, useEffect } from 'react';
import './index.less';
import { Layout } from 'antd';
import { useLocation,RouteComponentProps } from 'react-router-dom'

import { router ,newConstruction,login,whiteList} from '../routerList';
import Footer from '@ant-design/pro-layout/lib/Footer';
import MyMenu from '@/components/MyMenu';
import MyMyBreadCrumb from '@/components/MyBreadCrumb';
import MyHeader from '@/components/MyHeader';
const { Content } = Layout;

interface Iprops extends RouteComponentProps{
}
const Layouts:FC<Iprops> = (props)=> {
  
  const [collapsed,setCollapsed] = useState(false);
  const {pathname} = useLocation();
  const [ title,setTitle ] = useState(router);
  const { name,role,avatar }  = JSON.parse(localStorage?.user??"{}");
  const toggle = () => {
    setCollapsed(!collapsed)
  };
  for(let i=0;i<whiteList.length;i++){
    if(typeof whiteList[i]==='string'){
      if(whiteList.includes(pathname))return <>{props.children}</>
    }
    if(typeof whiteList[i] !== "string") {
      if((whiteList[i] as RegExp).test(pathname)) {
        return <>{props.children}</>
      }
    }
  }
  
  return (
    <Layout style={{width:"100%",height:"100%"}}>
      <MyMenu collapsed={collapsed} title={title} pathname={pathname} newConstruction={newConstruction}/>
      <Layout className="site-layout">
        <MyHeader collapsed={collapsed} toggle={toggle} login={login} name={name} avatar={avatar}/>
        <MyMyBreadCrumb title={title} pathname={pathname} role={role} name={name}/>
        <main style={{overflowY:'scroll'}}>
        <Content
          style={{
            padding:"24px",
            background:"--comment-editor-btn-disable-b",
            minHeight: 280,
            minWidth:"760px"
          }}
        >
          {props.children}
        </Content>
        <Footer style={{ textAlign: 'center'  }}>Ant Design ©2018 Created by Ant UED</Footer>
        </main>
      </Layout>
    </Layout>
  )
}
export default Layouts;
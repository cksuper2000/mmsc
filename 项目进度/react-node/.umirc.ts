import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  // routes: [
  //   { path: '/', component: '@/pages/index' },
  // ],
  fastRefresh: {},
  antd:{},  //开启antd
  dva: {
    immer: true,
    hmr: false,
  },
  locale: {
    default: 'zh-CN',
    antd: true,
    title: false,
    baseNavigator: true,
    baseSeparator: '-',
  }
  // proxy: {
  //   '/api': {
  //     target: 'http://127.0.0.1:3000',
  //     pathRewrite: { '^/api': '' },
  //     changeOrigin: true
  //   }
  // }
});

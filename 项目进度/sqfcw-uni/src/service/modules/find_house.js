import request from "@utils/request.js";
export const getList = (aaa) => request.post("/basic/pageConfig/tabs",{
        ...aaa
})

export const operatList=()=>request.post('/basic/app/operatList')

export const queryList=(aaa)=>request.post('/c_msh/mLife/goods/list/queryByTab',{
        ...aaa
})
/**
 * 轮播图数据
 */
export const getSwiper = function (){
        return request.post('/basic/app/banner')
}
/**
 * 获取详情数据
 */
export const getDetail = function (id){
        return request.post('/c_msh/mLife/goods/goodsDetail',id);
}
/**
 * 获取门店的请求
 */
export const getInstructionsList = function(payload){
        return request.post('/c_msh/mLife/shops/usableShopList',payload)
}
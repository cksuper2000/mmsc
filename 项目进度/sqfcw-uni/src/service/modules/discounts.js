import request from "@/utils/request";

export const getAppList = () => {
  return request.post(
    "/msh/app/index.php?i=5&t=0&v=1.0.1&from=wxapp&c=entry&a=wxapp&do=index&&m=zofui_sales&sign=bbdda43e5139cfc2fbe94c8ec020675c",
    {
      op: "info",
      lat: "39.95933",
      lng: "116.29845",
      zfid: "0",
      isnew: "1",
      isposter: "0",
      shopid: "0",
      cityId: "19",
      mwtoken:
        "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqthPfLkg3yGX-CwC9hjmX-Cx8f6Qqe8bcfngxpdoPqh45Odoj5qlSfgGOB73ydpINZjeIkASTGglS3bY-BmRWo13FI2rRitKaSTJDZgKsdpaBe4wNEsToD1A22oRLT4PDMbdDHQzrm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
      from: "wxapp",
    },
    {
      headers: { "content-type": "application/x-www-form-urlencoded" },
    }
  );
};

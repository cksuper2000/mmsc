import Vue from "vue";
import Vuex from "vuex";
import logger from "vuex/dist/logger";

import login from "./modules/login";
import find_house from "./modules/find_house";
import discounts from "./modules/discounts";
Vue.use(Vuex); //vue的插件机制

//Vuex.Store 构造器选项
const store = new Vuex.Store({
  state: {
    //存放状态
  },
  modules: {
    login,
    find_house,
    discounts,
  },
  plugins: [logger()],
});
export default store;

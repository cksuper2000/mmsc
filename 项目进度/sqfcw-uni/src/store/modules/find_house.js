import { getList, operatList, queryList ,getInstructionsList} from '@/service'
import {getDetail, getSwiper} from "../../service";

const state = {
    data: [],
    class_list: {
        cityId: 19,
        showPage: 0,
        platform: 0,
    },
    data_list: [],
    query_list: {
        tabId: 41,
        showPage: 0,
        platform: 0,
        longitude: 116.29845,
        latitude: 39.95933,
        showSort: 1,
        showSortMode: 1,
        pageNo: 1,
        pageSize: 10,
    },
    data_query:[],
    swiperList:[],//轮播图列表
    detail:{},//详情的数据
    instructions:{},//门店列表相关数据
}

const mutations = {
    aaa(state, payload) {
        state.data = payload.value
    },
    bbb(state, payload) {
        state.data_list = payload.value
    },
    ccc(state,payload){
        state.data_query=payload.value
    },
    updata(state,payload){
        state[payload.key] = payload.value;
    }
}

const actions = {
    async goodList({ commit }, payload) {
        let result = await getList(state.class_list)
        console.log(result)
        commit('aaa', {
            value: result.data
        })
    },
    async operatList({ commit }, payload) {
        let result = await operatList()
        console.log(result)
        commit('bbb', {
            value: result.data
        })
    },
    async queryList({ commit }, payload) {
        let result = await queryList(state.query_list)
        console.log(result)
        if(result.errNo==0){
            commit('ccc',{
                value:result.data.data
            })
        }
    },
    /**
     * 请求轮播图数据
     */
    async getSwiperList({commit},payload){
        let result = await getSwiper();
        if(result.errNo===0){
            commit("updata",{
                key:"swiperList",
                value:result.data
            });
        }
    },
    /**
     * 获取详情数据
     */
    async getDetailData({commit},id){
        let result = await getDetail(id);
        if(result.errNo===0){
            commit('updata',{
                key:"detail",
                value:result.data
            })
        }
    },
    /**
     * 获取门店列表
     */
    async getInstructions({commit,state }){
        let {id} = state.detail;
        let result = await getInstructionsList({
            goodId: id,
            lng: 116.29845,
            lat: 39.95933,
            showAll: 0,
            count: 3,
        });
        if(result.errNo===0){
            commit("updata",{
                key:'instructions',
                value:result.data
            })
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
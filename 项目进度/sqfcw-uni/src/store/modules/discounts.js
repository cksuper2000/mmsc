import { getAppList } from "@/service/modules/discounts";
const state = {
  appList: [],
};
const mutations = {
  updata(state, payload) {
    state[payload.type] = payload.data;
  },
};
const actions = {
  async getAppList({ commit }, payload) {
    let result = await getAppList();
    console.log("app...",result);
    if(result.errno==0){
        commit("updata",{
            type:"appList",
            data:result.data
        })
    }
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  actions,
};

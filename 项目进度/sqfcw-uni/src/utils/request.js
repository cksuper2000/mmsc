var Fly=require("flyio/dist/npm/wx")
var fly=new Fly;
fly.config={
  baseURL:'https://capi.mwee.cn',
  timeout:5000
}
const body={
  "cityId": 19,
	"mwAuthToken": "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtMVcmAmY-BLY-BaAbH6OgMzQRAY-BHqtqY-BNSAQcx56TOuU3wYTy9NkaDWEy7JauEOi42BM5qmDJ7CZHu2X-C9OQO3wsKP1HRyJOPX-CFHNzvPw5DLkvs29uHZ2nfrh43XNQLSPX-CfI4Erm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
	"mAuthToken": "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtMVcmAmY-BLY-BaAbH6OgMzQRAY-BHqtqY-BNSAQcx56TOuU3wYTy9NkaDWEy7JauEOi42BM5qmDJ7CZHu2X-C9OQO3wsKP1HRyJOPX-CFHNzvPw5DLkvs29uHZ2nfrh43XNQLSPX-CfI4Erm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
	"fromw": 1401,
  
}
console.log(fly)
//添加请求拦截器
fly.interceptors.request.use((request)=>{
  if(request.body){
    request.body={
      ...request.body,...body
    }
  }else{
    request.body=body
  }
  //给所有请求添加自定义header
  request.headers["X-Tag"]="flyio";
  //打印出请求体
  //终止请求
  //var err=new Error("xxx")
  //err.request=request
  //return Promise.reject(new Error(""))
  //可以显式返回request, 也可以不返回，没有返回值时拦截器中默认返回request
  return request;
})

//添加响应拦截器，响应拦截器会在then/catch处理之前执行
fly.interceptors.response.use(
    (response) => {
      //只将请求结果的data字段返回
      return response.data
    },
    (err) => {
      //发生网络错误后会走到这里
      //return Promise.resolve("ssss")
    }
)
export default fly;
const moment=require("moment")

export const time=(option)=>{
    return moment(option).format("YYYY-MM-DD")
}
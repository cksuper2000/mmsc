
"use strict";
const path = require("path");

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  // 路径别名
  configureWebpack: {
    resolve: {
      alias: {
        "@": resolve("src"),
        "@components": resolve("./src/components"),
        "@assets": resolve("./src/assets"),
        "@utils": resolve("./src/utils"),
        "@pages": resolve("./src/pages"),
        "@store":resolve("./src/store"),
        "@service":resolve("./src/service")
      },
    },
  },
}
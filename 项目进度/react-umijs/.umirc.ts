import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },

  fastRefresh: {},
  locale: {
    default: 'zh-CN',
  },
  antd:{},  //开启antd
  dynamicImport: {
    loading: "@/components/Loading",
  },
  publicPath:process.env.NODE_ENV === 'production' ? '/1812A/chengkai/react-umijs/' : '/',
  base:process.env.NODE_ENV === 'production' ? '/1812A/chengkai/react-umijs' : '/',
  analytics: {
    baidu: '824ee93a32a3071d94f1833f9b3e6395',
  },
});

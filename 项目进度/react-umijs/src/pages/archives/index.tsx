import React from 'react'
import styles from './index.less'
import Time from '@/components/Time'
import Heig from '@/components/Height'
import Recommend from '@/components/Module1'
import Classifylist from '@/components/Classifylist'

export default function Archives() {
  return (
    <div style={styles.archives}>
        <Heig renderLeft={()=>(
            <>
              <Time/>
            </>
          )}
          renderRight={()=>(
            <>
              <Recommend/>
              <Classifylist slide={1}/>
            </>
          )}
        />
    </div>
  )
}

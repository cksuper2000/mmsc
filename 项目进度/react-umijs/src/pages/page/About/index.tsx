import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'umi';
import { Name_About } from "@/models/about"
import Comment from "@/components/Comment"
import List from "@/components/List"
import { IRootState } from "@/types"
import about from "./index.less"
import "./index.less"
import "@/components/List/index.less"
import { request } from '@/app';
import { getComment } from "@/services/module/comment"
import { useState } from 'react';
interface Props {

}

const About = (props: Props) => {
    const dispatch = useDispatch()
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(6);
    useEffect(() => {
        dispatch({
            type: "about/about"
        })
    }, [])
    // 获取数据
    const { aboutData } = useSelector((state: Name_About) => state.about)
    useEffect(() => {
        dispatch({
            type: "articleList/getList"
        })
    }, [])
    const { articleList } = useSelector((state: IRootState) => state.articleList);
    console.log(articleList)
    const [dataMsgboard, setDataMsgboard] = useState([])
    useEffect(() => {
        getComment({ page: 1, pageSize: 6, id: aboutData.id! }).then(res => {
            console.log(res);
            if (res.statusCode == 200) {
                setDataMsgboard(res.data)
            }
        }).catch(err => {
            console.log(err);
        })
    }, [aboutData])

    return (
        <div className="about">
            <div className="container">
                <div className={about.img}>
                    <img src={aboutData.cover} alt="" />
                </div>
                <div className="text" dangerouslySetInnerHTML={{ __html: aboutData.html! }}></div>
            </div>

            {dataMsgboard.length > 0 && <Comment hostId={aboutData.id!} page={page} pageSize={pageSize} pageChange={(index: number) => { setPage(index) }} dataMsgboard={dataMsgboard} />}
            <div className="aboutList">
                <div className="aboutMain">
                    <h3>推荐阅读</h3>
                    <div>
                        <List articleList={articleList} start={0} end={6} />
                    </div>
                </div>
            </div>
        </div>
    )
}
export default About;
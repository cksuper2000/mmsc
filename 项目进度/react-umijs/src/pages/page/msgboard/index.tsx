import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'umi'
import { Name_Msgboard } from "@/models/msgboard";
import Comment from "@/components/Comment"
import { IRootState } from '@/types';
import { Types_Msgboard } from "@/types/msgboard"
import List from '@/components/List';
import styles from './index.less';
import { getComment } from '@/services'


interface Props {

}
const Msgboard = (props: Props) => {
    //留言数据
    const [msgboard, setMsgboard] = useState<Types_Msgboard>();
    //获取总的数据
    const { id } = useSelector((state: IRootState) => state.msgboard.msgboardData);
    // 创建dispatch
    const dispatch = useDispatch()
    //每页留言长度
    const pageSize = 5;
    //当前留言页数
    const [page, setPage] = useState(1);
    useEffect(()=>{
        let time = setInterval(()=>{
            (window as any)._hmt.push(['_trackEvent', "留言板", "获取id", "hostId", id]);
        },500);
        return ()=>{
            clearInterval(time);
        }
    },[])
    useEffect(() => {// 发送dispatch
        dispatch({
            type: "msgboard/Msgboard"
        })
    }, [])
    useEffect(() => {
        dispatch({
            type: "articleList/getList"
        })
    }, [])
    // 获取数据
    //发送获取数据的请求
    useEffect(() => {
        //发送请求
        getComment({ page: page, pageSize, id: (id as string) }).then(res => {
            setMsgboard(res.data)
        })
    }, [page, id])
    const pageChange = (index: number) => {
        setPage(index);
    }
    const { msgboardData } = useSelector((state: Name_Msgboard) => state.msgboard);
    let { articleList } = useSelector((state: IRootState) => state.articleList);
    return (
        <div className={styles.bg}>
            <div className={styles.top} dangerouslySetInnerHTML={{ __html: msgboardData.html! }}></div>
            <div className={styles.msgboard}>
                <Comment pageChange={pageChange} dataMsgboard={msgboard!} pageSize={pageSize} page={page} hostId={id!} />
                <div className={styles.message}>
                    <div className={styles.messageMain}>
                        <h3 style={{ textAlign: "center", padding: "18px 0" }}>推荐阅读</h3>
                        <div style={{ margin: "0 auto" }}>
                            <List articleList={articleList} start={0} end={6} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Msgboard;

import React, { useEffect, useRef, useState } from 'react'
import { detail } from '@/services'
import hljs from 'highlight.js';
import 'highlight.js/styles/github.css';
import { createRef } from 'react';
import { copyText } from '@/utits/copy';
import './index.less'
import Myck from './ck';
import Context from '@/utits/comment';
export default function Ck() {
  const [ mdetail,setDetail ] = useState<any>({});
  const dom = createRef<HTMLDivElement>();
  useEffect(()=>{
    detail('a03634b2-69ef-4d6e-8674-753c64c2456f').then(res=>{
      setDetail(res.data);
    })
  },[])
  useEffect(()=>{
    let block = dom.current!.querySelectorAll("pre code");
    console.log(block);
    block.forEach((item:any,index)=>{
      hljs.highlightElement(item);
      //添加复制按钮
      let btn = document.createElement("button");
      btn.textContent="复制";
      btn.classList.add('copy-btn');
      btn.onclick=function(){
        copyText(block[index].textContent!);
      }
      block[index].parentNode?.appendChild(btn);
    })
  })
  const dom1 = useRef();
  const a = (ref:any)=>{
    // console.log(ref);
  }
  return (
    <div ref={dom}>
      <Context.Provider value={{a}}>
      </Context.Provider>
      <Myck/>
      {/* {
        <div>
          {mdetail.cover&&<img src={mdetail.cover} style={{width:"100%",}}/>}
          <h1>{mdetail.title}</h1>   
          <div dangerouslySetInnerHTML={{__html:mdetail.html}}>
          </div>
        </div>
      } */}
    </div>
  )
}

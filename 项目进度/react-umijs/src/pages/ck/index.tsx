import React,{memo, useCallback, useContext, useMemo} from 'react';
import { useState } from 'react';
import Text1 from './Text1';
import useWinSize from '@/hook/useWinSize'
import { useReducer } from 'react';
import MyContext from '@/utits/comment';

const PureText1 = memo(Text1);
const initialState = {num: 0};
const reducer = (state:{num:number}, action:{[index:string]:any}) => {
  switch(action.type) {
    case 'decrement':
      return {...state, num: state.num - 1}
    case 'increment':
      return {...state, num: state.num + 1}
    default:
      return state;
  }
}
export default memo(function Home() {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <div>
      <MyContext.Provider value={{title:"三笠"}}>
        {/* <PureText1
          title={"是哪里"}
        /> */}
        <Text1/>
        <button onClick={()=>dispatch({type:"increment"})}>+</button>
        <span>{state.num}</span>
        <button onClick={()=>dispatch({type:'decrement'})}>-</button>
      </MyContext.Provider>
    </div>
  )
})

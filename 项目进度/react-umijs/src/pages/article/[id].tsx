import React, { useEffect, useRef, useState } from "react";
import { connect, IRouteProps, useDispatch, useSelector } from "umi";
import { Detail } from "@/types/model";
import Heig from "@/components/Height";
import DetailPage from "@/components/Detail"
import Recommend from "@/components/Module1"
import Floor from "@/components/Floor"
import Picture from '@/components/Picture'
import Collection from '@/components/Collection'
import Comment from '@/components/Comment'

interface Arrcoeent {
    level: string
    id: string
    text: string
};

const Article: React.FC<IRouteProps> = (props) => {
    let Div = useRef<HTMLDivElement>(null);
    let HTop = [...document.querySelectorAll("#LeftList>h2,h3,h4,h5")].filter(item=>{
        return item.id
    }); //获取所有的h2-3-4-5标签
    let id = props.match.params.id; // 获取id 
    let dispatch = useDispatch();
    let [page] = useState(1);
    let { detailList, detailComment, detailCommentnum } = useSelector((state: Detail) => state.detail);
    let [coeent, Setcoeent] = useState(``);
    let [Arrcoeent, SetArrcoeent] = useState<Array<Arrcoeent>>([])
    let [messagePage, setMessagePage] = useState<number>(1);
    let H4Ref = useRef<any>(null);
    useEffect(() => {
        Setcoeent(detailList.toc!)
    }, [detailList.toc]);
    useEffect(() => {
        if (coeent) {
            SetArrcoeent(JSON.parse(coeent))
        }
    }, [coeent]);
    useEffect(() => {
        dispatch({
            type: "detail/detail",
            payload: id
        });
    }, [id]); //调用接口
    useEffect(() => {
        dispatch({
            type: "detail/detailComment",
            payload: {
                id,
                page
            }
        })
    }, [page]);
    const pageChange = (index: number) => {
        setMessagePage(index);
    }
    const childRef = (ref: any) => {
        H4Ref.current = ref;
    }
    return (
        <div>
            <Heig
                renderLeft={() => (
                    
                    <Picture>
                        <DetailPage>
                            {detailList.cover && <img src={detailList.cover} />}
                            <h1>{detailList.title}</h1>
                            <span>发布于{detailList.createAt}阅读量{detailList.views}</span>
                            <div className="di" ref={Div} dangerouslySetInnerHTML={{ __html: detailList.html! }} />
                        </DetailPage>   
                        <Comment h4Ref={childRef} dataMsgboard={[detailComment,detailCommentnum]} hostId={detailComment[0]?.id} page={messagePage} pageSize={5} pageChange={pageChange}/>       
                    </Picture>
                    
                    
                )}
                renderRight={(slide) => (
                    <div className="you">
                        <Recommend id={id} />
                        <Floor Arrco={Arrcoeent} slide={slide} Hc={HTop} />
                    </div>
                )}
            />
            <Collection detailList={detailList} H4Ref={H4Ref} />
        </div>
    )
}

export default Article
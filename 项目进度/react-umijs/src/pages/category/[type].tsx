import React, { useEffect, useState } from 'react'
import style from './index.less';
import { useParams } from 'react-router-dom'
import Count from '@/components/Count'
import Heig from '@/components/Height'
import { Affix } from 'antd';
import Recommend from '@/components/Module1';
import Essay from '@/components/Essay'
import { useSelector } from '@/.umi/plugin-dva/exports';
import { IRootState } from '@/types';

export default function Category() {
  let { type } = useParams<{ type: string }>();
  //获取标题数据
  let { title, articleList } = useSelector((state: IRootState) => state.articleList);
  const [top] = useState(10);
  return (
    <div className={style.category}>
      <Heig
        renderLeft={() => (
          <>
            <div style={{ background: "#fff", padding: "16px 0", marginBottom: "10px", textAlign: "center" }}>
              <p style={{ marginBottom: "0" }}>
                <span style={{ fontSize: "1.5rem", color: "#ff0064" }}>{title.filter(item => item.value === type)[0]?.label}</span>
              </p>
              <p style={{ marginBottom: "0" }}>共搜索到<span style={{ fontSize: "1.5rem", color: "#ff0064" }}>{articleList.length}</span>篇文章</p>
            </div>
            <Count titleColor="red" type={type} />
          </>
        )}
        renderRight={() => (
          <div>
            <Recommend />
            <Essay />
          </div>
        )}
      />
    </div>
  )
}

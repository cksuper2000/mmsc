import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector, history } from 'umi'
import { Knowledid, knnwledgeList } from '@/types'
import { withRouter, RouteComponentProps, NavLink, } from 'react-router-dom'
import Heig from '@/components/Height'
import styles from './index.less'
import Kon from '@/components/Knowledgelist'

interface Props {

}


const knowledgeid: React.FC<RouteComponentProps<{ id: string }>> = (props) => {
  const idd = props.match.params.id
  const dispatch = useDispatch()
  const { Getknowledgeid } = useSelector((state: Knowledid) => state.Knowledgeid)
  console.log(Getknowledgeid)
  useEffect(() => {
    dispatch({
      type: 'Knowledgeid/getknowledgeid',
      payload: idd
    })
  }, [idd]);
  const { knowledgeList } = useSelector((state: knnwledgeList) => state.knowledgeList)
  useEffect(() => {
    console.log(idd);

    let index = knowledgeList.findIndex(item => item.id === idd)
    console.log(index)
    knowledgeList.splice(index, 1)
  }, [knowledgeList])
  useEffect(() => {
    dispatch({
      type: 'knowledgeList/getknowledge'
    })
  }, [])
  return (
    <div>
      <div className={styles.home}>
        <div className={styles.left}>
          <div className={styles.crumbstop}>
            <div className={styles.crumbst}>
              <span onClick={() => {
                history.push("/knowledge")
              }}>
                知识小册/
              </span>
              <span>
                <span>{Getknowledgeid.title}</span>
              </span>
            </div>
          </div>
          <div className={styles.crumbsLeft}>
            <div className={styles.lefttop}>
              <header>{Getknowledgeid.title}</header>
            </div>
            <div className={styles.leftcenter}>
              <div className={styles.centerframe}>
                <div className={styles.frametop}>
                  <img src={Getknowledgeid.cover} alt="" />
                </div>
                <div className={styles.framebottom}>
                  <p>{Getknowledgeid.title}</p>
                  <p>{Getknowledgeid.summary}</p>
                  <p><span>{Getknowledgeid.views} 次阅读</span><span> · </span><span>{Getknowledgeid.publishAt}</span></p>
                  <div>
                    <button>开始阅读</button>
                  </div>
                </div>
              </div>
              <ul className={styles.ul}>
                {
                  Getknowledgeid.children?.map(item => {
                    return <li key={item.id}>
                      <NavLink to={`/knowledge/${idd}/${item.id}`}>
                        <span>{item.title}</span>
                        <span>{item.publishAt + ">"}</span>
                      </NavLink>
                    </li>
                  })
                }
              </ul>
            </div>
          </div>
        </div>
        {/* <Heig
          renderLeft={() => (
            
          )}
          renderRight={() => (
           
          )}
        /> */}
        <div className={styles.right}>
          <div className={styles.knowledgeRight}>
            <span
              className={styles.rightTitle}>其他知识笔记</span>
            <div className={styles.Right}>
              <div className={styles.all}>
                <Kon />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default withRouter(knowledgeid);

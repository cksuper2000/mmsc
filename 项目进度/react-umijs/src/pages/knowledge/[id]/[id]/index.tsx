import React, { useContext, useEffect, useRef, useState } from 'react'
import Heig from "../../../../components/Height"
import { IRouteProps, useDispatch, useSelector } from 'umi';
import Picture from "../../../../components/Picture";
import Detail from '../../../../components/Detail';
import Comment from '../../../../components/Comment';
import Recommend from "../../../../components/Module1";
import Floor from "../../../../components/Floor"
import Collent from "../../../../components/Collection"
import { Know } from '../../../../types/knowdetail';
import {Types_Msgboard} from "../../../../types/knowdetail"


interface Arrcoeent {
    level: string
    id: string
    text: string
};
const Indexknow: React.FC<IRouteProps> = (props) => {
    const dispatch = useDispatch() //获取dispatch;
    let id = props.match.params.id; //获取文章内容id
    let recoId=props.match.url.split("/")[2];  //获取推荐文章id
    const {knowObject,knowComment}=useSelector((state:Know)=>state.knowdetail); //从仓库获取数据
    let HTop = [...document.querySelectorAll("#LeftList>h2,h3,h4,h5")].filter(item=>{
        return item.id
    }); //获取所有h2,h3标签
    HTop.pop()
    console.log(HTop,"heheheheheehehe")
    let [page,setPage]=useState(1); // 分页数据
    let [coeent, Setcoeent] = useState(``);
    let [Arrcoeent, SetArrcoeent] = useState<Array<Arrcoeent>>([]);
    let H4Ref = useRef<any>(null);
    useEffect(() => {
        dispatch({
            type: "knowdetail/knowDetail",
            payload: id
        })
        dispatch({
            type:"knowdetail/knowreview",
            payload:{
                id,
                page
            }
        })
    }, [id]);
    
    useEffect(() => {
        Setcoeent(knowObject.toc!)
    }, [knowObject.toc]);
    useEffect(() => {
        if (coeent) {
            SetArrcoeent(JSON.parse(coeent))
        }
    }, [coeent]);
    const pageChange = (index: number) => {
        setPage(index);
    }
    const childRef = (ref: any) => {
        H4Ref.current = ref;
    }
    return (
        <div>
            <Heig
                renderLeft={() => {
                    return <Picture>
                        <Detail>
                            {knowObject.cover && <img src={knowObject.cover} />}
                            <h1>{knowObject.title}</h1>
                            <span>发布于{knowObject.createAt}阅读量{knowObject.views}</span>
                            <div id="LeftList" dangerouslySetInnerHTML={{ __html:knowObject.html! }} />
                        </Detail>
                        <Comment h4Ref={childRef}  dataMsgboard={(knowComment.length?knowComment:[[],0]) as unknown as Types_Msgboard} hostId={id} page={page} pageSize={6} pageChange={pageChange} />
                    </Picture>
                }}
                renderRight={(slide) => {
                    return <div>
                        <Recommend id={id} source="knowledge" recoId={recoId} />
                        <Floor Arrco={Arrcoeent} slide={slide} Hc={HTop} />
                    </div>
                }}
            />
            <Collent detailList={knowObject} H4Ref={H4Ref} />
        </div>
    )
}

export default Indexknow;

import React, { useState, useEffect } from 'react'
import styles from './index.less'
import Heig from '@/components/Height'
import Recommend from '@/components/Module1'
import Knowledgelist from '@/components/Knowledgelist'
import Classifylist from '@/components/Classifylist'


const Knowledge = () => {
    return (
        <div className={styles.home}>
            <Heig
                renderLeft={() => (
                    <div className={styles.left}>
                        <Knowledgelist />
                    </div>
                )}
                renderRight={() => (
                    <div className={styles.right}>
                        <Recommend/>
                        <Classifylist slide={1}/>
                    </div>
                )}
            />
        </div>
    )
}
export default Knowledge;
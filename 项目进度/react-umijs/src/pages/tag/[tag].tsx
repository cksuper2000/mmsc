import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector, useParams, NavLink } from 'umi'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import { TagLi, essaystate } from '@/types'
import Heig from '@/components/Height'
import styles from './index.less'
import Recommend from '@/components/Module1';
import Classifylist from '@/components/Classifylist'
import moment from "../../utits/momen";
import { HeartOutlined, EyeOutlined, DeploymentUnitOutlined } from '@ant-design/icons';
import share from '../../components/Share';


interface Props {

}

const Tag: React.FC<RouteComponentProps<{ tag: string }>> = (props) => {
  const dispatch = useDispatch()
  let tag = props.match.params.tag;

  const { tagList } = useSelector((state: TagLi) => state.TagList)
  console.log(tagList)
  useEffect(() => {
    dispatch({
      type: 'TagList/gettag',
      payload: tag
    })
  }, [tag])
  const { essayList } = useSelector((state: essaystate) => state.eassayList)
  console.log(essayList)
  useEffect(() => {
    dispatch({
      type: 'eassayList/getessay'
    })
  }, []);

  const [active, activestate] = useState(0)
  return (
    <div className={styles.home}>
      <Heig
        renderLeft={() => (
          <div className={styles.left}>
            <div className={styles.lefttop}>
              <p>与 <span>{essayList.filter(item => item.value === tag)[0]?.label}</span> 标签有关的文章</p>
              <p>共搜索到 <span>{tagList.length}</span> 篇</p>
            </div>
            <div className={styles.leftcenter}>
              <div className={styles.top}>
                <span>文章标签</span>
              </div>
              <ul className={styles.bottom}>
                {
                  essayList.map((item, index) => {
                    return <li className={styles.li} key={index}>
                      <NavLink to={`/tag/` + item.value}>{item.label} [{item.articleCount}]</NavLink>
                    </li>
                  })
                }
              </ul>
            </div>
            <div className={styles.leftbottom}>
              {
                tagList.length !== 0 ? tagList.map(item => {
                  return <div key={item.id} className={styles.bottomlist}>
                    <header className={styles.listtop}>
                      <div>{item.title}</div>
                      <div><span>超过{moment(item.createAt).fromNow()}</span></div>
                    </header>
                    <div className={styles.listbottom}>
                      <div className={styles.img}>
                        <img src={item.cover} alt="" />
                      </div>
                      <div className={styles.size}>
                        <p>{item.summary}</p>
                        <p className={styles.p}><span> <HeartOutlined /> {item.likes} ·</span><span> <EyeOutlined /> {item.views} ·</span><span onClick={(e) => {
                          e.stopPropagation()
                          share(item)
                        }}> <DeploymentUnitOutlined /> 分享</span></p>
                      </div>
                    </div>
                  </div>
                }) : <span>暂无数据</span>
              }

            </div>
          </div>
        )}
        renderRight={() => (
          <div className={styles.right}>
            <Recommend />
            <Classifylist slide={1} />
          </div>
        )}
      />
    </div>
  )
}

export default withRouter(Tag)

import React, { useState, useEffect,memo } from "react";
import Recommend from "@/components/Module1";
import styles from './index.less'
import Count from '@/components/Count'
import Swiper from '@/components/Swiper'
import Essay from '@/components/Essay'
import Heig from "@/components/Height";
const PureHeig = React.memo(Heig);
const Page = () => {
  
  return (
    <div className={styles.home}>
      <PureHeig
        renderLeft={() => (
          <div className="left">
            <Swiper />
            <Count
              titleColor={"red"}
            />
          </div>
        )}
        renderRight={() => (
          <div className={styles.Right}>
            <Recommend/>
            <Essay/>
          </div>
        )}
      />
    </div>
  )
}

export default Page;
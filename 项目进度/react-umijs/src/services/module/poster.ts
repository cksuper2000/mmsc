import { request } from "@/.umi/plugin-request/request";

interface Idata {
  height: number
  html: string
  name: string
  pageUrl: string
  width: number
}

export function genePoster (data:Idata){
  return request('/api/poster',{
    method:"POST",
    data
  })
}
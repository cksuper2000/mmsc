import { request } from 'umi';


// 获取推荐文章
export function getRecommend(id: string) {
  return request(`/api/article/recommend${id ? `?articleId=${id}` : ``}`)
};


//获取Web性能指南
export function WEBnature(id: string) {
  return request(`/api/knowledge/${id}`)
}
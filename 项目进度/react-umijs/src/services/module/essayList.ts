import { request } from 'umi'

//获取文章标签
export function getessay(articleStatus='publish'){
    return request('/api/tag',{
        params:{
            articleStatus
        }
    })
}

//获取文章分类
export function getclassify(articleStatus='publish'){
    return request('/api/category',{
        params:{
            articleStatus 
        }
    })
}

//获取轮播图
export function getswiper(){
    return request('/api/article/all/recommend')
}

//文章标签内容
export function gettag(type:string,page=1,pageSize=12,status='publish'){
    return request(`/api/article/tag/${type}`,{
        params:{
            page,
            pageSize,
            status
        }
    })
}

//知识小册详情页
export function getknowledgeid(id:string){
    return request(`/api/knowledge/${id}`)
}
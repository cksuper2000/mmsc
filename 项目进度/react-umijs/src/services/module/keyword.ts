import { request } from 'umi';

export function getSerchData(keyword:string) {
  return request(`/api/search/article?keyword=${keyword}`);
}

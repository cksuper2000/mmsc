import {request} from "umi";

//详情文章
export function detail(id:string){
    return request(`/api/article/${id}/views`,{
        method:"post"
    })
};

//详情评论
export function detailComment(id:string,page=1,pageSize=6){
    return request(`/api/comment/host/${id}`,{
        params:{
            page,
            pageSize
        }
    })
};

//详情推荐文章
export function detailRecommend(id:string){
    return request(`/api/article/recommend?articleId=${id}`)
}

//收藏请求
export function like({id,type}:{id:string,type:string}){
    return request(`/api/article/${id}/likes`,{
        method:"POST",
        data:{
            type
        }
    })
}

import { request } from 'umi';

//获取知识小测详情数据
export function knowDetail(id:string){
    return request(`/api/knowledge/${id}`)
}

//获取评论数据
export function knowreview(id:string,page=1,pageSize=6){
    return request(`/api/comment/host/${id}`,{
        params:{
            page,
            pageSize
        }
    })
}
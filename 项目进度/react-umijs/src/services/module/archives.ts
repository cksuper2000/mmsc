import {request} from "umi"

// 归档接口请求
export function archives(){
    return request("/api/article/archives")
}
import { request } from 'umi'
//首页列表的请求
interface IgetArticleList {
  page: number, 
  pageSize:number, 
  status:'publish'
}
export function getArticleList({page=1, pageSize=12, status='publish'}:IgetArticleList,type:string){
  return request(`/api/article/${type?'category/'+type:""}`,
  {
    params: {
      page,
      pageSize,
      status
    },
  })
}

export function getknowledge(page=1,pageSize=12,status='publish'){
  return request('/api/knowledge',{
    params:{
      page,
      pageSize,
      status
    }
  })}
// 获取列表标题
export function getTitle(){
  return request('/api/category?articleStatus=publish')
}
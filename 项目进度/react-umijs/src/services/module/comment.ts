import { request } from 'umi'
interface IComment{
  page:number
  pageSize:number
  id:string
}
export function getComment({page,pageSize,id}:IComment){
  return request('/api/comment/host/'+id,{
    params:{
      page,
      pageSize
    }
  })
}
//接口
interface Idata{
  content: string
  email: string
  hostId: string
  name: string
  url: string
}
export function addComment(data:Idata){
  return request('/api/comment',{
    method:"POST",
    data
  })
}
interface IReply {
  content: string
  email: string
  hostId: string
  name: string
  parentCommentId: string
  replyUserEmail: string
  replyUserName: string
  url: string
}
//发送回复
export function addReply(data:IReply){
  return request('/api/comment',{
    method:"POST",
    data
  })
}
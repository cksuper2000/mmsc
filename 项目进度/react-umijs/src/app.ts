import { RequestConfig, useSelector } from 'umi';
import { message } from 'antd';
import { createLogger } from 'redux-logger';
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
// import { createLogger } from 'redux-logger';
//禁用线上的console.log()
process.env.NODE_ENV === 'production'?console.log = ()=>{}: null;
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
Sentry.init({
  dsn: "https://1ea917f94d024e3f8a20af6185875926@o974169.ingest.sentry.io/5928520",
  integrations: [new Integrations.BrowserTracing()],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

message.config({
  duration: 2,
  maxCount: 3,
});
export const dva = {
  config: {
    // onAction: createLogger(),
    onError(e: Error) {
      message.destroy();
      message.error(e.message, 3);
    },
  },
};
//在初始化加载路由或者切换路由时做点事情
export function onRouteChange() {
  // NProgress.start();
  document.documentElement.scrollTop = 0;

  document.title = '小楼又清风'; //切换title
}

const baseUrl = 'https://creation.shbwyz.com';
export const request: RequestConfig = {
  timeout: 10000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    (url, options) => {
      NProgress.start();
      return {
        url: `${baseUrl}${url}`,
        options,
      };
    },
  ],
  responseInterceptors: [
    (response) => {
      const codeMaps: { [key: number]: string } = {
        400: '错误的请求',
        403: '禁止访问',
        404: '找不到资源',
        500: '服务器内部错误',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      NProgress.done();
      if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
        NProgress.done();
        message.destroy();
        message.error({
          content: codeMaps[response.status],
        });
      }

      return response;
    },
  ],
};

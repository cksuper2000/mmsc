import React, { useState, useEffect } from 'react'
import classNames from 'classnames'
import { BulbFilled, DownOutlined, MenuOutlined, AudioOutlined, StarFilled } from '@ant-design/icons';
import { Menu, Dropdown, message, Input, Space } from 'antd';
import header from "./index.less"
import "./index.less"
import { history, NavLink, setLocale, useDispatch, useIntl, useSelector } from "umi"
import { Keyword } from "@/models/keyword"
const Header: React.FC = () => {
    // 定义暗黑模式状态
    const [darkFlag, setDark] = useState(false)
    // 头部导航
    const [headNav, setHeadNav] = useState([
        {
            title: "article",
            path: "/"
        },
        {
            title: "archives",
            path: "/archives"
        },
        {
            title: "knowledge",
            path: "/knowledge"
        },
        {
            title: "msgboard",
            path: "/page/msgboard"
        },
        {
            title: "about",
            path: "/page/about"
        },
    ])
    // 引入dispatch
    const dispatch = useDispatch()
    // 默认选中下标
    let [currentIndex, setCurrentIndex] = useState(sessionStorage.index ? JSON.parse(JSON.stringify(sessionStorage.index)) : 0)
    // 导航下拉菜单状态
    let [visible, setvisible] = useState(false)
    // 语言
    const [languages, setLanguages] = useState([{ language: "英文", internationalization: "en-US" }, { language: "汉语", internationalization: "zh-CN" }])
    // 绑定语言
    const intl = useIntl();
    // 语言下拉菜单
    const languageMenu = (
        <Menu>
            {
                languages.map((item, index) => {
                    return <Menu.Item key={index} onClick={() => {
                        setLocale(`${item.internationalization}`, false);
                    }}>
                        {item.language}
                    </Menu.Item>
                })
            }
        </Menu>
    );
    // 表头列表
    const menu = (
        <Menu>
            {
                headNav.map((item, index) => {
                    return <Menu.Item key={index}
                        className={classNames(index == currentIndex ? "active" : '')}
                        onClick={() => changeIndex(index, item)}
                    >
                        {
                            intl.formatMessage({
                                id: `${item.title}`
                            })
                        }
                    </Menu.Item>
                })

            }
            <Menu.Item key="language">
                <Dropdown overlay={languageMenu}>
                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z" fill="currentColor"></path></svg>
                </Dropdown>
            </Menu.Item>
            <Menu.Item key="dark">
                <span onClick={() => {
                    if (darkFlag) {
                        setDark(false)
                        document.body.className = ""
                    } else {
                        setDark(true)
                        document.body.className = "dark"
                    }
                    setvisible(false)
                }}>
                    {
                        darkFlag ? <StarFilled style={{ color: "#e684af" }} /> : <BulbFilled style={{ color: "#eec413" }} />
                    }
                </span>
            </Menu.Item>
            <Menu.Item key="search" >
                <svg onClick={() => {
                    setvisible(false)
                    setSearchFlag(true);

                    document.querySelector("body")!.style.overflowY = "hidden";
                }} viewBox="64 64 896 896" focusable="false" data-icon="search" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path></svg>
            </Menu.Item>
        </Menu>
    );

    // 定义搜索框状态
    const [searchFlag, setSearchFlag] = useState(false)
    // 结构搜索事件
    const { Search } = Input;
    const [searchLoading, setSearchLoading] = useState(false)
    // 搜索后的值
    const onSearch = (value: string) => {
        setSearchLoading(true)
        if (value) {
            dispatch({
                type: "keyword/getSerchData",
                payload: {
                    value: encodeURI(value)
                }
            })
        } else {
            dispatch({
                type: "keyword/getSerchData",
                payload: {
                    value: encodeURI("YYDS")
                }
            })
        }
    };
    // 取搜索后的值
    const { searchData } = useSelector((state: Keyword) => state.keyword);
    // 监听
    useEffect(() => {
        setSearchLoading(false)
    }, [searchData])
    // 键盘事件关闭搜索框页面
    useEffect(() => {
        document.documentElement.onkeydown = (e) => {
            if (e.keyCode == 27) {
                setSearchFlag(false)
            }
        }
    }, [searchFlag]);
    return (
        <div className={header.head}>
            <div className={header.left}>
                <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png" alt="" />
            </div>
            <div className={header.right}>
                <ul>
                    {
                        headNav.map((item, index) => {
                            return <li key={index}
                                className={classNames(index == currentIndex ? header.active : '')}
                                onClick={() => changeIndex(index, item)}
                            >
                                {
                                    intl.formatMessage({
                                        id: `${item.title}`
                                    })
                                }
                            </li>
                        })
                    }
                </ul>
                {/* 操作菜单 */}
                <div className={header.action}>
                    <Dropdown overlay={languageMenu}>
                        <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z" fill="currentColor"></path></svg>
                    </Dropdown>
                    <span onClick={() => {
                        if (darkFlag) {
                            setDark(false)
                            document.body.className = ""
                        } else {
                            setDark(true)
                            document.body.className = "dark"
                        }
                        setvisible(false)
                    }}>
                        {
                            darkFlag ? <StarFilled style={{ color: "#e684af" }} /> : <BulbFilled style={{ color: "#eec413" }} />
                        }
                    </span>

                    <svg onClick={() => {
                        setSearchFlag(true);
                        document.querySelector("body")!.style.overflowY = "hidden";
                    }} viewBox="64 64 896 896" focusable="false" data-icon="search" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path></svg>
                </div>
            </div>
            {/* 响应式菜单 */}
            <div className={header.menu} >
                <Dropdown overlay={menu} trigger={['click']} visible={visible} onVisibleChange={() => {
                    setvisible(!visible)
                }}>
                    <MenuOutlined />
                </Dropdown>
            </div>
            {//搜索页面
                searchFlag && <div className={header.search} id="search">
                    <div className={header.main}>
                        <div className={header.main_top}>
                            <h1>文章搜索</h1>
                            <span onClick={() => {
                                setSearchFlag(false)
                                document.querySelector("body")!.style.overflowY = "auto";
                            }}>ESC</span>
                        </div>
                        <Space direction="vertical">
                            <Search placeholder="输入关键字，搜索文章" loading={searchLoading} onSearch={onSearch} style={{ width: "100%" }} />
                        </Space>
                        <div className="list" style={{ height: "300px", overflow: 'hidden', flexShrink: 0 }}>
                            {
                                searchData.map(item => {
                                    return <p key={item.id}>
                                        <NavLink to={`/article/${item.id}`} onClick={() => {
                                            setSearchFlag(false);
                                            document.querySelector("body")!.style.overflowY = "auto";

                                        }}>{item.title}</NavLink>
                                    </p>
                                })
                            }
                        </div>
                    </div>
                </div>
            }
        </div>
    )
    // 改变下标
    function changeIndex(index: number, item: any) {
        setCurrentIndex(currentIndex = index)
        // 存入本地存储
        sessionStorage.index = index
        // 跳转路由
        history.push(item.path)
        // 关闭下拉菜单
        setvisible(false)
        console.log(item);
        window._hmt.push(['_trackEvent', "头部导航", "点击跳转", "导航id", item.id]);
    }
}
export default Header

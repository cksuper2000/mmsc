import React, { useEffect, useState } from 'react'
import styles from './index.less'
import { useDispatch, useSelector } from 'umi'
import { IRootState } from '@/types/model'
import List from '../List'
import { useHistory  } from 'react-router-dom'
import InfiniteScroll from "react-infinite-scroll-component";


interface Iprops {
  titleColor: string,
  type?:string
}
const Count:React.FC<Iprops> = ({titleColor,type=""})=> {
  //取出数据
  const { articleList, leng,title,page,pageSize ,currIndex} = useSelector((state: IRootState) => state.articleList);
  console.log(currIndex);
  const dispatch = useDispatch();
  const history = useHistory();
  // 请求标题
  useEffect(()=>{
    dispatch({
      type:'articleList/getTitle',
      payload:type
    })
  },[type])
  const fetchMoreData = () => {
    console.log("我请求数据了");
    //发送请求
    dispatch({
      type: 'articleList/getList',
      payload:type
    });
  };
  //请求列表
  useEffect(() => {
    dispatch({
      type: 'articleList/getList',
      payload:type
    });
  }, [type])
  
  //接受参数
  const hanldTitle = (val:any) => {
    //跳转路由
    history.push(val?'/category/'+val:"");
  }
  return (
    <div className={styles.count}>
      <header>{
        title.map((item, index) => (
          <span
            key={index}
            style={{ color: currIndex === index ? titleColor : "" }}
            onClick={() => hanldTitle(item.value)}
          >{item.label}</span>
        ))
      }</header>
      {/*列表部分渲染*/}
      <InfiniteScroll
        dataLength={leng}
        next={(fetchMoreData as any)}
        hasMore={Math.ceil(leng / pageSize) >= page}
        loader={<h4>Loading...</h4>}
      >
        <div className={styles.list}>
          <List 
            start={0}
            articleList={articleList} 
          />
        </div>
      </InfiniteScroll>
    </div>
  )
}
export default Count;
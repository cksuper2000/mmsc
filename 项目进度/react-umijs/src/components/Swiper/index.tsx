import React, { useState, useEffect } from "react";
import styles from './index.less'
import { Carousel } from 'antd';
import { useDispatch, useSelector,useHistory } from 'umi'
import { Swiperim } from '@/types'
import moment from "../../utits/momen";


const classNames = require('classnames');
const IndexPage: React.FC = () => {
    const dispatch = useDispatch()
    const { swiperimg } = useSelector((state: Swiperim) => state.Swiperimg)
    useEffect(() => {
        dispatch({
            type: 'Swiperimg/getswiper'
        })
    }, [])
    const history=useHistory()
    return (
        <div className={classNames(styles.home, styles.title)}>
            <div className={classNames(styles.all)}>
                <div className={styles.frame}>
                    <div className={styles.left}>
                        <div className={styles.swiperframe}>
                            <div className={styles.swiper}>
                                <Carousel autoplay>
                                    {
                                        swiperimg.map(item => {
                                            return <div key={item.id} className={styles.swiperi} onClick={()=>{
                                                history.push(`/article/${item.id}`)
                                            }}>
                                                <img src={item.cover} alt="" />
                                                <div className={styles.mc}></div>
                                                <div className={styles.sizeframe}>
                                                    <a href="">
                                                        <h2>{item.title}</h2>
                                                        <p>
                                                            <span>{moment(item.createAt).fromNow()} </span> 
                                                            <span> · </span> 
                                                            <span> {item.views} 次阅读</span>
                                                        </p>

                                                    </a>
                                                </div>
                                            </div>
                                        })
                                    }
                                </Carousel>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
}

export default IndexPage

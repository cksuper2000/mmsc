import React,{useEffect, useState} from "react";
import "./iconfont/iconfont.css";
import classNames from "classnames";
import styles from "./index.less";

interface Top{
    slide:number
}

const Backtop:React.FC<Top>=({slide})=>{
    
    //返回顶部按钮
    let GoTop=()=>{
        document.documentElement.scrollTop=0;  //返回顶部
    }
    
    return(
        <div onClick={()=>GoTop()} className={slide>=400 ? classNames(styles.top,styles.xian) : styles.top }>
            <span className="iconfont">&#xe630;</span>
        </div>
    )

}

export default Backtop
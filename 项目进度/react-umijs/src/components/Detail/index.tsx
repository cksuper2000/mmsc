import React, { useEffect, useRef } from "react";
import hljs from "highlight.js"
import copy from "copy-to-clipboard"
import {message} from "antd"
import  "./index.less"


const detail:React.FC=(props)=>{
    let Div=useRef<HTMLDivElement>(null);
    useEffect(()=>{
        let ob = new MutationObserver(()=>{
            let block = Div.current?.querySelectorAll("pre code");
            block!.forEach(item=>{
                hljs.highlightElement(item as HTMLElement);  //给元素添加高亮
                let btn=document.createElement("button");
                btn.innerHTML="复制";
                btn.className="copy-posation"
                item.parentElement?.insertBefore(btn,item); //插入标签
                btn.onclick=()=>{
                    //复制文本
                    copy(item.textContent!);
                    message.success("复制成功!!")
                }
            })
        });
        ob.observe(Div.current!.children[Div.current!.children.length-1],{
            "childList" : true,//子节点的变动
        });
    },[])
    useEffect(()=>{
        //获取所有的h2标签
        let H2=Div.current!.querySelectorAll("h2");
        let H3=Div.current!.querySelectorAll("h3");
        let H4=Div.current!.querySelectorAll("h4");
        let H=[...H2].concat([...H3],[...H4]);
        H.forEach((item,index)=>{
            item.id=`${item.textContent}` //给每一个h2标签添加类名
        });
    },[props.children])
    return(
        <div ref={Div} className="markdown" style={{background:"var(--bg)",borderRadius:"4px",padding:"1rem",boxSizing:"border-box"}}>
            {props.children}
        </div>
    )
}
export default detail
import React, { useEffect } from 'react'
import styles from './index.less';
import { ArchivesState } from "@/models/archives"
import { useDispatch, useSelector,history } from 'umi';


// 定义命名空间验证
interface Archives {
  archives: ArchivesState
}
// 定义props验证
interface Props {

}
const Time: React.FC<Props> = (props) => {
  // 创建dispatch
  const dispatch = useDispatch();
  // 发送dispatch
  useEffect(() => {
    dispatch({
      type: "archives/archives"
    })
  }, [])
  // 获取数据
  const { archivesList } = useSelector((state: Archives) => state.archives);
  console.log(archivesList);
  // 计算总数量
  let sum = Object.values(archivesList).map(item => {
    return Object.values(item).reduce((total, item) => {
      return total += item.length
    }, 0)
  }).reduce((total, item) => total += item, 0)
  // 取key值
  let year = Object.keys(archivesList);
  return (
    <div className={styles.time}>
      <div className={styles.top}>
        <p>归档</p>
        <p className={styles.sum}>
          共计
          <span>{sum}</span>
          篇
        </p>
      </div>
      {
        year.sort((prev, next) => Number(next) - Number(prev)).map((item, index) => {
          //继续获取键名
          return (
            <div key={index} className={styles.main}>
              <h2>{item}</h2>
              {
                Object.keys(archivesList[item]).map((val: string, index) => {
                  return (
                    <li key={index} >
                      <h3 style={styles.h3}>{val}</h3>
                      {
                        archivesList[item][val].map(element => {
                          // console.log(archivesList[item])
                          return (
                            <ul key={element.id}>
                              <li onClick={()=>{
                                history.push(`/article/${element.id}`)
                              }}>
                                <span>
                                  {element.createAt.substr(5, 5)}
                                </span>
                                <span>
                                  {element.title}
                                </span>
                              </li>
                            </ul>
                          )
                        })
                      }
                    </li>
                  )
                })
              }
            </div>
          )
        })
      }
    </div>
  )
}
export default Time;
import React, { useState, useEffect, useRef } from 'react'
import classify from './index.less'
import { useDispatch, useSelector, NavLink } from 'umi'
import { Classify } from '@/types'

interface Iprops {
    slide: number
}

const Classifylist: React.FC<Iprops> = ({ slide }) => {

    const dispatch = useDispatch()
    const { ClassifyList } = useSelector((state: Classify) => state.ClassifyList)
    useEffect(() => {
        dispatch({
            type: 'ClassifyList/getclassify'
        })
    }, []);
    const div = useRef<HTMLDivElement>(null);
    //吸顶
    if (div.current && div.current?.offsetTop) {
        if (slide >= div.current.offsetTop) {
            div.current.classList.add(classify.fixed)
        } else {
            div.current.classList.remove(classify.fixed)
        }
    }
    return (
        <div className={classify.frame} ref={div}>
            <div className={classify.frametop}>
                <span>文章分类</span>
            </div>
            <ul className={classify.ul}>
                {
                    ClassifyList.map(item => {
                        return <li key={item.id}>
                            <NavLink to={`/category/` + item.value}>
                                <span>{item.label}</span>
                                <span>
                                    共计 {item.articleCount} 篇文章
                                </span>
                            </NavLink>

                        </li>
                    })
                }

            </ul>
        </div>
    )
}

export default Classifylist;

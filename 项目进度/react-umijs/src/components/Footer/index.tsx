import React from 'react'
import footer from './index.less'
import { GithubOutlined, WifiOutlined } from '@ant-design/icons';

interface Props {
    
}

const Footer = () => {
    return (
        <div className={footer.bottomframe}>
            <ul className={footer.ul}>
                <li><WifiOutlined style={{color:'#c1bdbd',fontSize:'22px'}} /> </li>
                <li><GithubOutlined style={{color:'#c1bdbd',fontSize:'22px'}} /></li>
            </ul>
            <div className={footer.size}>
                <p>
                    <a href="">Designed by Fantasticit . 后台管理</a>
                </p>
                <p>Copyright © 2021. All Rights Reserved.</p>
                <p>皖ICP备18005737号</p>
            </div>
        </div>
    )
}

export default Footer;

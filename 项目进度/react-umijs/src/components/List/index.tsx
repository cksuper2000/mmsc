import React from 'react'
import { Datum } from '@/types/index'
import styles from './index.less';
import share from '../Share';
import {
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined                                                                                    
} from '@ant-design/icons';
import { useHistory } from 'umi';
interface Iprops {
  articleList: Array<Datum>
  start: number
  end?: number
}
const style = {
  padding: 15,
  cursor:"pointer"
};

const List: React.FC<Iprops> = ({ articleList, start, end }) => {
  const router = useHistory();
  return (
    <div className={styles.list}>{
      articleList.slice(start,end).map(item => (
        <div style={style} className={styles.item} key={item.id}>
          <header onClick={()=>router.push('/article/'+item.id)} style={{cursor:"pointer"}}>
            <div className={styles.title}>
              {item.title}
            </div>
            <div className={styles.time}>
              {item.updateAt}
            </div>
          </header>
          <main>
            {item.cover ? (
              <div className={styles.img}>
                <img src={item.cover} alt="" />
              </div>
            ) : null}
            <div className={styles.message}>
              <div className={styles.mestitle}>
                {item.summary}
              </div>
              <div className={styles.action}>
                <span><HeartOutlined />{item.likes}</span>
                <span style={{margin:"0 10px"}}><EyeOutlined />{item.views}</span>
                <span onClick={()=>share(item)}><ShareAltOutlined />分享</span>
              </div>
            </div>
          </main>
        </div>
      ))
    }</div>
  )
}
export default List;
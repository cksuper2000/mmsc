import React, { useState, useEffect, useRef } from "react";
import styles from './index.less'
import { useDispatch, useSelector, NavLink } from 'umi'
import { essaystate } from '@/types'
const classNames = require('classnames');


const Essay: React.FC = () => {
  const dispatch = useDispatch()
  const { essayList } = useSelector((state: essaystate) => state.eassayList)
  useEffect(() => {
    dispatch({
      type: 'eassayList/getessay'
    })
  }, []);
  

  return (
      <div className={classNames(styles.lowrigh)}>
        <div className={styles.articletop}>
          <span>文章标签</span>
        </div>
        <div className={styles.articlenext}>
          <ul className={styles.ul}>
            {
              essayList.map(item => {
                return <li className={styles.li} key={item.id}>
                  <NavLink to={`/tag/`+item.value}>{item.label} [{item.articleCount}]</NavLink>
                </li>
              })
            }
          </ul>
        </div>
      </div>
  );
}

export default Essay;
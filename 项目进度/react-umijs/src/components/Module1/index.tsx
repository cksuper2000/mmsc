import React, { useState, useEffect, useRef } from "react";
import { Recommend } from "@/types/recommend";
import { useDispatch, useSelector } from "umi";
import { NavLink } from "react-router-dom";
import classNames from "classnames";
import moment from "../../utits/momen";
import { Spin } from "antd"
import styles from "./index.less";

interface Input {
    /**这里应该传文章id */
    id?: string
    /**用来识别是从哪里跳转而来的 */
    source?: "knowledge"
    /**知识小测详情id */
    recoId?: string
};
let Recomm: React.FC<Input> = ({ id, source, recoId }) => {
    let { recomm } = useSelector((state: Recommend) => state.loading.models);
    const dispatch = useDispatch();
    const { recommend, WEBrecommend } = useSelector((state: Recommend) => state.recomm); //从state里拿取数据
    useEffect(() => {
        !source ? dispatch({
            type: "recomm/Recommend",
            payload: { id }
        }) : dispatch({
            type: "recomm/WEBnature",
            payload: recoId
        })
    }, [id]);
    return <div className={classNames(styles.box)} >
        <div className={styles.shang}>
            {!source ? <span className={styles.h2}>推荐阅读</span> : <span>{WEBrecommend.title}</span>}
        </div>
        <div className={styles.inne}>
            <div className={!source ? styles.antSpinContainer_essay : styles.antSpinContainer_knowledge}>
                <ul>
                    {
                        recomm ? <div className={styles.example}><Spin /></div> :
                            !source ?
                                recommend.length != 0 ? recommend.map((item, index) => {
                                    return <li key={item.id} className={(recomm == false && !source) && styles.li}>
                                        <NavLink to={`/article/${item.id}`}>
                                            <span>{item.title + " . "}</span>
                                            <span>{moment(item.createAt).fromNow()}</span>
                                        </NavLink>
                                    </li>
                                }) : <span>暂无数据</span> :
                                WEBrecommend.children?.length != 0 ?
                                    WEBrecommend.children?.map(item => {
                                        return <li key={item.id}>
                                            <NavLink to={`/knowledge/${recoId}/${item.id}`}>
                                                <span>{item.title}</span>
                                            </NavLink>
                                        </li>
                                    })
                                    : <span>暂时没有数据啊</span>
                    }
                </ul>
            </div>
        </div>
    </div>
}
export default Recomm


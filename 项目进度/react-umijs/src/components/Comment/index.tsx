import React, { ChangeEvent, useRef, useState ,MouseEvent} from 'react'
import { Input, Button, Modal, Form, message, Pagination ,Popover} from 'antd';
import comment from "./index.less"
import "./index.less"
import { addComment } from '@/services'
import { useLocation } from 'react-router-dom'
import {  Types_Msgboard } from '@/types/msgboard'
import { addReply } from '@/services'
import { emojis } from '@/utits/emojis'
import { Datum } from '@/types/msgboard';
import CommentItem from '@/components/CommentItem'

const { TextArea } = Input;
interface Props {
    /**留言板的数据 */
    dataMsgboard:Types_Msgboard;
    /**留言的条数 */
    pageSize:number
    /**留言的页码 */
    page:number
    /**留言hostId */
    hostId:string,
    /**点击页面改变的事件 */
    pageChange(index:number):void
    h4Ref?:any
}
const Comment = (props: Props) => {
    const { pathname } = useLocation();
     // 定义文本框初始状态
     const [value, setValue] = useState("");
     // 定义对话框初始状态
     const [isModalVisible, setIsModalVisible] = useState(false);
    const onFinish = (values: any) => {// 点击获取对话框内的值
        //保存本地
        localStorage.user = JSON.stringify(values);
        //关闭显示
        setIsModalVisible(false);
    };
    const hangldRelease = async () => {
        let { name, email, } = JSON.parse(localStorage.user);
        let result = await addComment({ name, email, hostId: '8d8b6492-32e5-44e5-b38b-9a479d1a94bd', url: pathname, content: value });
        if (result.statusCode === 201) {
            message.success({ content: '评论成功，已提交审核!' });
        } else {
            message.error("评论失败,请重新提交")
        }
    }
    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };
    const hanldParentRelease = async (value:string,item:any)=>{
        let content = value;
        if(!content){
            message.success('输入框不能为空');
            return;
        }
        let {
            email,
            name,
            id,
        } = item;
        // 判断本地储存是否有数据
        let user = JSON.parse(localStorage?.user??"{}");
        if(user.email){
            //发送请求
            let result = await addReply({
                email:user.email,
                name:user.name,
                hostId:props.hostId,
                parentCommentId:id,
                replyUserEmail:email,
                replyUserName:name,
                url:pathname,
                content
            })
            //判断是否发送成功
            if(result.statusCode===201){
                message.success('评论成功,以提交审核');
            }else {
                message.error('发送错误');
            }
        }else {
            message.error("你还没登陆");
        }
    }
    const hanldChildRelease = async (value:string,item:any)=>{
        let content = value;
        if(!content){
            message.success('输入框不能为空');
            return;
        }
        // //获取参数
        let { parentCommentId,email,replyUserName } = item;
        let user = JSON.parse(localStorage?.user??"{}");
        if(user.email){
            let result = await addReply({
                content,
                email:user.email,
                hostId:props.hostId,
                name:user.name,
                parentCommentId,
                replyUserEmail:email,
                replyUserName,
                url:pathname
            });
            //判断是否发送成功
            if(result.statusCode===201){
                message.success('评论成功,以提交审核');
            }else {
                message.error('发送错误');
            }
        }else {
            message.error("您还没登陆");
        }
    }
    return (
        <>
            <div className={comment.main}>
                <h4 ref={props.h4Ref} style={{ textAlign: "center", padding: "18px 0" }} id="commentTop">评论</h4>
                <div className={comment.comment}>
                    <TextArea
                        id="leaveMessage"
                        value={value}
                        placeholder="请输入评论内容 (支持 Markdown)"
                        autoSize={{ minRows: 5, maxRows: 20 }}
                        style={{ cursor: "pointer" }}
                        onClick={onChang}
                        onChange={(e: ChangeEvent<HTMLTextAreaElement>) => setValue(e.target.value)}
                    />
                    <div className={comment.footer}>
                        <li onClick={onChang}>
                            <Popover placement="bottomRight" trigger="click" content={(
                                <ul style={{display:"flex",maxWidth: "480px",height: "240px",flexWrap: "wrap",overflow: "auto",msOverflowStyle:"-moz-initial"}}>
                                    {
                                        Object.entries(emojis).map((item,index)=>{
                                            return (
                                                <li style={{
                                                    width: "32px",
                                                    height: "32px",
                                                    fontSize: "20px",
                                                    textAlign: "center",
                                                    cursor: "pointer"}}
                                                    key={index}
                                                    onClick={(e:any)=>{
                                                        let dom:any = document.getElementById('leaveMessage');
                                                        dom.value = dom.value+item[1];
                                                    }}
                                                >{item[1]}</li>
                                            )
                                        })
                                    }
                                </ul>
                            )}>
                            <svg viewBox="0 0 1024 1024" width="18px" height="18px"><path d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z" fill="currentColor"></path></svg>
                                <span>表情</span>
                            </Popover>
                        </li>
                        <li>
                            <Button
                                type="primary"
                                disabled={value ? false : true}
                                onClick={hangldRelease}
                            >发布</Button>
                        </li>
                    </div>
                </div>
                <ul style={{margin:"0",padding:"0"}}>
                    {
                        props.dataMsgboard? (props.dataMsgboard[0] as Datum[]).map((item, index: number) => (
                            <div key={item.id} className={comment.message}>
                                <header>
                                    <span className={comment.antAvatar}>
                                        <span className={comment.antAvatarString}>{item.name.slice(0, 1).toLocaleLowerCase()}</span>
                                    </span>
                                    <span className={comment.username}>
                                        <strong>{item.name}</strong>
                                    </span>
                                </header>
                                <main>
                                    <div dangerouslySetInnerHTML={{ __html: item.html }}>
                                    </div>
                                </main>
                                <footer>
                                    <CommentItem item={item} hanlRelease={hanldParentRelease}/>
                                </footer>
                                {
                                    item.children.map(val=>(
                                       <div key={val.id} style={{boxSizing:"border-box",paddingLeft:"2.5rem"}}>
                                           <header>
                                                <span className={comment.antAvatar}>
                                                    <span className={comment.antAvatarString}>{val.name.slice(0, 1).toLocaleLowerCase()}</span>
                                                </span>
                                                <span className={comment.username}>
                                                    <strong>{val.name}</strong>
                                                    <span style={{marginLeft:"1rem",marginRight:"1rem"}}>回复</span>
                                                    <strong>{item.name}</strong>
                                                </span>
                                            </header>
                                            <main>
                                                <div dangerouslySetInnerHTML={{ __html: val.html }}>
                                                </div>
                                            </main>
                                            <footer>
                                                <CommentItem item={val} hanlRelease={hanldChildRelease} />
                                            </footer>
                                       </div>
                                    ))
                                }
                            </div>
                        )) : null
                    }
                    <div style={{ textAlign: "center" }} className="message">
                        <Pagination size="small" current={props.page} pageSize={props.pageSize} total={props.dataMsgboard?props.dataMsgboard[1] as unknown as number:0} onChange={props.pageChange} />
                    </div>
                </ul>
            </div>
            <Modal title="请设置你的信息"
                visible={isModalVisible}
                onOk={() => setIsModalVisible(false)}
                onCancel={() => setIsModalVisible(false)}
                footer={[]}
            >
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    labelAlign={"left"}
                >
                    <Form.Item
                        label="名称"
                        name="name"
                        rules={[{ required: true, message: '请输入您的称呼' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="邮箱"
                        name="email"
                        rules={[{ required: true, message: '输入合法邮箱地址，以便在收到回复时邮件通知' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 17, span: 16 }}>
                        <Button onClick={() => setIsModalVisible(false)}>取消</Button>
                        <Button type="primary" htmlType="submit" danger>设置</Button>
                    </Form.Item>
                </Form>
            </Modal>
        </>
    )
    function onChang() {// 点击文本框判断有无登录状态
        if (!localStorage.user) {
            setIsModalVisible(true);
        }
    }
}
export default Comment;
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector, useHistory} from 'umi'
import list from './index.less'
import { knnwledgeList } from '@/types'
import moment from "../../utits/momen";
import { EyeOutlined, DeploymentUnitOutlined } from '@ant-design/icons';
import share from '../Share';

interface Props {

}

const Knowledgelist: React.FC = () => {
    const dispatch = useDispatch()
    const { knowledgeList } = useSelector((state: knnwledgeList) => state.knowledgeList)
    useEffect(() => {
        dispatch({
            type: 'knowledgeList/getknowledge'
        })
    }, [])
    const history=useHistory()
    function btnbind(id:string){
        history.push(`/knowledge/${id}`)
        // window._hmt.push(['_trackEvent', '知识小册', '小册详情','详情id', id]);
    }

    return (
        <div className={list.all}>
            {
                knowledgeList.length>0&&knowledgeList.map((item, index) => {
                    return <div key={index} className={list.frame} onClick={()=>btnbind(item.id)}>
                            <header className={list.frametop}>
                                <div>{item.title}</div>
                                <div className={list.k}></div>
                                <span>{moment(item.createAt).fromNow()}</span>
                            </header>
                        <div className={list.framebottom}>
                            <div className={list.img}>
                                <img src={item.cover} alt="" />
                            </div>
                            <div>
                                <div style={{ fontSize: '15px',WebkitLineClamp: 3,overflow:"hidden",textOverflow:"ellipsis",display:"-webkit-box",WebkitBoxOrient:"vertical" }}>
                                    {item.summary}
                                </div>

                                <div className={list.frameright}>
                                    <span><EyeOutlined />{item.views}</span>
                                    <span>·</span>
                                    <span onClick={(e) => {
                                        e.stopPropagation()
                                        share(item)
                                    }}><DeploymentUnitOutlined />分享</span>

                                </div>
                            </div>
                        </div>

                    </div>
                })
            }
        </div>
    )
}

export default Knowledgelist;

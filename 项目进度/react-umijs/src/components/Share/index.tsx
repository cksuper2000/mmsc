import React,{useState} from 'react'
import {DetailList,knowledgeli} from '@/types';
import {Modal} from 'antd';
import QRCode from 'qrcode';
import ReactDOM from 'react-dom';
import styles from './index.less';
import { useEffect } from 'react';
import { message } from 'antd'
import { genePoster } from '@/services/module/poster';

interface IProps {
  detailList:Partial<DetailList> | Partial<knowledgeli>
}

export const Share:React.FC<IProps> = (props)=>{
  console.log(props);
  
  console.log(props);
  //报存二维码
  const [qrSrc, setQrSrc] = useState('');
  //保存下载的地址
  const [downloadSrc, setDownloadSrc] = useState('');
  function cancelClick() {
    ReactDOM.unmountComponentAtNode(document.querySelector('#share-dialog')!);
  }
  const url = `https://blog.wipi.tech/article/${props.detailList.id}`;
  useEffect(()=>{
    if(Object.keys(props.detailList)){
      //生成二维码
      QRCode.toDataURL(url)
        .then((res)=>{
          console.log(res);
          
          setQrSrc(res);
        })
        .catch(err=>{
          message.error(err);
        })
    }
  },[props.detailList])
  //生成海报
  useEffect(()=>{
    if(Object.keys(props.detailList).length&&qrSrc){
      //发送请求下载
      genePoster({
        width:861,
        height:391,
        name:props.detailList.title as string,
        pageUrl: "/article/"+props.detailList.id,
        html:document.querySelector('.ant-modal-content')?.innerHTML!
      }).then(res=>{
        console.log(res);
        
        setDownloadSrc(res.data.url);
      })
    }
  },[props.detailList,qrSrc])
  return (
    <Modal 
      title="分享海报"
      width="400px"
      visible={true} 
      onOk={()=>window.location.href=downloadSrc} 
      onCancel={cancelClick}
      cancelText={"关闭"}
      okText={'下载'}
      style={{width:"50%"}}
    >
      {props.detailList.cover && <img src={props.detailList.cover} style={{width:"100%",height:"100%"}}/>}
      <div>{props.detailList.title}</div>
      <div>{props.detailList.summary}</div>
      <div style={{display:"flex"}}>
        <img src={qrSrc} style={{width:"6rem",height:"6rem"}}/>
          <div style={{display:"flex",flexDirection:"column",justifyContent:"space-between"}}>
            <p style={{marginBottom:"0",marginTop:"1rem"}}>识别二维码查看文章</p>
            <p>原文分享自
                <a style={{color:"red"}} href={url}>小楼又清风</a>
            </p>
          </div>
      </div>
    </Modal>
  )
}

export default function share(detailList:Partial<DetailList>|Partial<knowledgeli>) {
  
  
  let shareDialog = document.querySelector('#share-dialog');
  if (!shareDialog) {
    shareDialog = document.createElement('div');
    shareDialog.id = 'share-dialog';
    document.body.appendChild(shareDialog);
  }
  ReactDOM.render(<Share detailList={detailList} />, shareDialog);
}

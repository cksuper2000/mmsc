import classNames from "classnames";
import React, { useEffect, useState } from "react"
import styles from "./index.less"

// interface Arr {
//     id?: string
//     text?: string
//     level?: string
//     children?: Array<Arr>
// }
interface IFloorItem{
    id: string;
    text: string;
    level: string;
    children?: IFloorItem[];
}
interface Floor {
    /**要传递的toc楼层数据 */
    // Arrco: Array<Arrco>
    /**滚动条高度 */
    slide: number
    /**标签 */
    Hc:Array<any>
}
interface IProps{
    arr: IFloorItem []
}
function transformArr(arr:IFloorItem[]) {
    const newArr = [];
    for (let i = 0; i < arr.length; i++) {
        let parent = findItem(newArr, arr[i]);
        if (parent) {
            if (parent.children) {
                parent.children.push(arr[i])
            } else {
                parent.children = [arr[i]]
            }
        } else {
            newArr.push(arr[i])
        }
    }
    return newArr;
}
function findItem(arr:IFloorItem[], item:IFloorItem) {
    for (let i = arr.length - 1; i >= 0; i--) {
        if (Number(item.level) - 1 === Number(arr[i].level)) {
            return arr[i]
        }

        if (arr[i].children) {
            let result:any = findItem(arr[i].children!, item);
            if (result) {
                return result
            }
        }
    }
}

function RenderItem({arr}:IProps){
    return <div>{
        arr.map(item=>{
            console.log('item...', item);
            if (!item.children){
                return React.createElement('li', {}, item.text);
                // return <li>{item.text}</li>
            }else{
                return <div>
                    {React.createElement('li', {}, item.text)}
                    {RenderItem({arr:item.children})}
                </div>
            }
        })    
    }</div>
}
const index: React.FC<Floor> = ({ Arrco, slide ,Hc}) => {

    const [floorHight, setFloorHight] = useState(0);
    let scrollview = (i: string, v: number) => {
        Hc.forEach((item) => {
            if (item.id === `${i}`) {
                setFloorHight(v)
                item.scrollIntoView()
            }
        })
    };
    useEffect(() => {
        Hc.forEach((item, index) => {
            if ((item.offsetTop)<=slide) {
                setFloorHight(index)
            }
        })
    }, [slide]);


    let newArr = transformArr(Arrco);
    return (
        <div className={styles.dd} >
            <RenderItem arr={Arrco}/>
            {/* {
                Arrco.map((item, index) => {
                    switch (item.level) {
                        case "2":
                            return <h2 key={index} onClick={()=>scrollview(item.text!,index)} className={index==floorHight ? classNames(styles.h2,styles.Hight) : styles.h2}>{item.text}</h2>

                        case "3":
                            return <h3 key={index} onClick={()=>scrollview(item.text!,index)} className={index==floorHight ? classNames(styles.h3,styles.Hight) : styles.h3}>{item.text}</h3>

                        case "4":
                            return <h4 key={index} onClick={()=>scrollview(item.text!,index)} className={index==floorHight ? classNames(styles.h4,styles.Hight) : styles.h4}>{item.text}</h4>

                    }
                })
            } */}

        </div>
    )
}

export default index
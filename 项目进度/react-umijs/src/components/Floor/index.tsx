import classNames from "classnames";
import React, { useEffect, useState } from "react"
import styles from "./index.less"

interface Arrco{
    id?: string
    text?: string
    level?: string
}
interface Floor {
    /**要传递的toc楼层数据 */
    Arrco: Array<Arrco>
    /**滚动条高度 */
    slide: number
    /**标签 */
    Hc:Array<any>
}

const index: React.FC<Floor> = ({ Arrco, slide ,Hc}) => {
    console.log(Hc,"hchc")
    const [floorHight, setFloorHight] = useState(0);
    let scrollview = (i: string, v: number) => {
        Hc.forEach((item) => {
            if (item.id === `${i}`) {
                document.documentElement.scrollTop=item.offsetTop+1;
                window._hmt.push(['_trackEvent', "点击跳转", "滑动", "第几位",i]);
            }
        })
    };
    useEffect(() => {
        Hc.forEach((item, index) => {
            if(slide>=item.offsetTop-80){
                setFloorHight(index)
            }
        })
    }, [slide]);

    return (
        <div className={styles.dd} >
            {
               (Arrco.length!=0) && Arrco.map((item, index) => {
                    switch (item.level) {
                        case "2":
                            return <h2 key={index} onClick={()=>scrollview(item.text!,index)} className={index==floorHight ? classNames(styles.h2,styles.Hight) : styles.h2}>{item.text}</h2>

                        case "3":
                            return <h3 key={index} onClick={()=>scrollview(item.text!,index)} className={index==floorHight ? classNames(styles.h3,styles.Hight) : styles.h3}>{item.text}</h3>

                        case "4":
                            return <h4 key={index} onClick={()=>scrollview(item.text!,index)} className={index==floorHight ? classNames(styles.h4,styles.Hight) : styles.h4}>{item.text}</h4>

                    }
                })
            }

        </div>
    )
}

export default index
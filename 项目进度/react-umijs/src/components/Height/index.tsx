import React, { useEffect } from 'react'
import content from '@/utits/content';
import styles from './index.less';

interface Iprops {
  renderLeft(): any
  renderRight(slide:number): any
  collection?:false
}
/**接受两个组件 */
const Heig: React.FC<Iprops> = ({ renderLeft, renderRight }) => {
  return (
      <content.Consumer>
      {
        (slide) => {
          return (
            <div className={styles.height}>
              <div className={styles.left}>
                {renderLeft()}
              </div>
              <div className={styles.right}>
                {renderRight(slide)}
              </div>
            </div>
          )
        }
      }
      </content.Consumer>
  )
}
export default Heig;
import React, { useState } from 'react'
import styles from './index.less';
import { useDispatch, useSelector } from 'umi'
import { Detail, IRootState } from '@/types';
import { DetailList } from '@/types/iddetail'
import share from '../Share';

interface Iprops {
    detailList: Partial<DetailList>
    H4Ref: any
}
export default function Collection(props: Iprops) {
    const { likes, id } = useSelector((state: Detail) => state.detail.detailList);
    const dispatch = useDispatch();
    //点击收藏
    const hanldCollection = () => {
        //首先判断本地是否有这个数据
        let LIKE: string[] = JSON.parse(localStorage?.LIKE ?? "[]");
        //查找下标
        let index = LIKE.findIndex((item) => item === id);
        if (index === -1) {
            //添加数据
            dispatch({
                type: 'detail/getLike',
                payload: {
                    id,
                    type: "like"
                }
            });
        } else {
            //添加数据
            dispatch({
                type: 'detail/getLike',
                payload: {
                    id,
                    type: "dislike"
                }
            });
        }
    }
    const shareArticle = (e: React.MouseEvent, item: Partial<DetailList>) => {
        e.stopPropagation();
        share(item);
    }

    return (
        <div className={styles.collection}>
            <div className={styles.like}>
                <span>
                    <div
                        className={styles.picture}
                        onClick={hanldCollection}
                        style={{ color: JSON.parse(localStorage?.LIKE ?? "[]").findIndex((item: string) => item === id) != -1 ? "#ff0064" : "" }}
                    >
                        <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M859.8 191.2c-80.8-84.2-212-84.2-292.8 0L512 248.2l-55-57.2c-81-84.2-212-84.2-292.8 0-91 94.6-91 248.2 0 342.8L512 896l347.8-362C950.8 439.4 950.8 285.8 859.8 191.2z" fill="currentColor"></path></svg>
                    </div>
                    <sup
                        data-show="true"
                    >
                        <span>
                            <p>{likes}</p>
                        </span>
                    </sup>
                </span>
            </div>
            <div className={styles.message}>
                <div onClick={() => props.H4Ref.current.scrollIntoView()}>
                    <div >
                        <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M988.8 512a348.8 348.8 0 0 1-144.96 278.72v208.32l-187.84-131.52a387.2 387.2 0 0 1-56 4.8A408.64 408.64 0 0 1 384 811.84l-36.8-23.04a493.76 493.76 0 0 0 52.8 3.2 493.44 493.44 0 0 0 51.2-2.88c221.44-23.04 394.24-192 394.24-400a365.12 365.12 0 0 0-4.16-51.84 373.44 373.44 0 0 0-48.96-138.56l18.88 11.2A353.6 353.6 0 0 1 988.8 512z m-198.72-128c0-192-169.6-349.76-378.24-349.76h-24C192 47.04 33.92 198.72 33.92 384a334.08 334.08 0 0 0 118.4 253.12v187.52l86.08-60.48 66.24-46.4a396.16 396.16 0 0 0 107.52 16C620.48 734.08 790.08 576 790.08 384z" fill="currentColor"></path></svg>
                    </div>
                </div>
            </div>
            <div className={styles.share}>
                <span>

                    <div onClick={(e) => shareArticle(e, props.detailList)}>
                        <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"><path d="M753.607 584.7c-48.519 0-91.596 23.298-118.66 59.315l-233.123-116.96c3.684-12.936 5.657-26.591 5.657-40.71 0-15.465-2.369-30.374-6.76-44.391l232.241-116.52c26.916 37.549 70.919 62.017 120.644 62.017 81.926 0 148.34-66.412 148.34-148.34 0-81.926-66.413-148.34-148.34-148.34-81.927 0-148.34 66.413-148.34 148.34 0 5.668 0.33 11.258 0.948 16.762l-244.945 122.892c-26.598-25.259-62.553-40.762-102.129-40.762-81.926 0-148.34 66.412-148.34 148.34s66.413 148.34 148.34 148.34c41.018 0 78.144-16.648 104.997-43.555l242.509 121.668c-0.904 6.621-1.382 13.374-1.382 20.242 0 81.927 66.412 148.34 148.34 148.34s148.34-66.413 148.34-148.34c-0.001-81.925-66.409-148.339-148.336-148.339l0 0z" fill="currentColor"></path></svg>
                    </div>
                </span>
            </div>
        </div>
    )
}

import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getswiper } from '@/services';
import { SwiperObject } from '@/types'
//设置接口
export interface SwiperimgState {
  swiperimg:Array<SwiperObject>
}

//仓库接口
interface SwiperimgType {
  namespace: 'Swiperimg';
  state: SwiperimgState;
  effects: {
    getswiper: Effect;
  }
  reducers: {
    save: Reducer<SwiperimgState>;
  };
}
const SwiperimgModel:SwiperimgType = {
  namespace:"Swiperimg",
  state:{
    swiperimg:[]
  },
  effects:{
    *getswiper({ payload }, { call, put}){
        let result=yield call(getswiper)
        console.log('...eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',result)
        if(result.statusCode==200){
            yield put({
                type:'save',
                payload:{swiperimg:result.data}
            })
        }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  }
}

export default SwiperimgModel;
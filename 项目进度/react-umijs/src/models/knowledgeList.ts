import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getknowledge } from '@/services';
import { knowledgeli } from '@/types'
//设置接口
export interface KnowledgeState {
    knowledgeList:Array<knowledgeli>
}

//仓库接口
interface KnowledgeType {
  namespace: 'knowledgeList';
  state: KnowledgeState;
  effects: {
    getknowledge: Effect;
  }
  reducers: {
    save: Reducer<KnowledgeState>;
  };
}
const KnowledgeModel:KnowledgeType = {
  namespace:"knowledgeList",
  state:{
    knowledgeList:[]
  },
  effects:{
    *getknowledge({ payload }, { call, put}){
        let result=yield call(getknowledge)
        // console.log('...eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',result)
        if(result.statusCode===200){
            yield put({
                type:'save',
                payload:{knowledgeList:result.data[0]}
            })
        }
       
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  }
}

export default KnowledgeModel;
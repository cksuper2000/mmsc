import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getknowledgeid } from '@/services';
import { knowledgeid } from '@/types'
//设置接口
export interface KnowledgeidState {
  Getknowledgeid:Partial<knowledgeid>
}

//仓库接口
interface KnowledgeidType {
  namespace: 'Knowledgeid';
  state: KnowledgeidState;
  effects: {
    getknowledgeid: Effect;
  }
  reducers: {
    save: Reducer<KnowledgeidState>;
  };
}
const KnowledgeidModel:KnowledgeidType = {
  namespace:"Knowledgeid",
  state:{
    Getknowledgeid:{}
  },
  effects:{
    *getknowledgeid({ payload }, { call, put}){
        let result=yield call(getknowledgeid,payload)
        // console.log('...iddddddddddddddddd',result)
        if(result.statusCode==200){
            yield put({
                type:'save',
                payload:{Getknowledgeid:result.data}
            })
        }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  }
}

export default KnowledgeidModel;
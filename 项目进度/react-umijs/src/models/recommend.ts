import { getRecommend, WEBnature } from '@/services/module/article';
import { RootObject, WEBObject } from '@/types/article';
import { Effect, ImmerReducer, Reducer} from 'umi';

export interface RecoModelState {
  recommend:Array<RootObject>
  WEBrecommend: Partial<WEBObject>
}

export interface RecoModelType {
  namespace: 'recomm';
  state: RecoModelState;
  effects: {
    Recommend:Effect
    WEBnature:Effect
  };
  reducers: {
    save: Reducer<RecoModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const IndexModel: RecoModelType = {
  namespace: 'recomm',

  state: {
    recommend:[],
    WEBrecommend:{}
  },

  effects: {
    
   *Recommend({ payload }, { call, put }) {
        let result=yield call(getRecommend,payload.id);
        
        if(result.statusCode==200){
          yield put({
            type:"save",
            payload:{recommend:result.data.slice(0,6)}
          })
        }
    },
    *WEBnature({ payload }, { call, put }) {
      
      let result=yield call(WEBnature,payload);
      console.log(result,"qweqweqweqweqweqweqweqweqw")
      if(result.statusCode==200){
        yield put({
          type:"save",
          payload:{WEBrecommend:result.data}
        })
      }
  }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  }
};

export default IndexModel;
import { detail, detailComment, detailRecommend,like } from '@/services/module/iddetail';
import { DetailComment, DetailList } from '@/types/iddetail';
import { Effect, ImmerReducer, Reducer } from 'umi';


export interface IDetailModelState {
  detailList: Partial<DetailList>   // pattial为可选属性
  detailComment:Array<DetailComment>
  detailCommentnum:number
}

export interface IDetailModelType {
  namespace: 'detail';
  state: IDetailModelState;
  effects: {
    detail:Effect
    detailComment:Effect
    getLike:Effect
  };
  reducers: {
    save: Reducer<IDetailModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const IndexModel: IDetailModelType = {
  namespace: 'detail',

  state: {
   detailList:{},
   detailComment:[],
   detailCommentnum:0
  },

  effects: {
    //详情列表
    *detail({ payload }, { call, put }) {
      let result=yield call(detail,payload);
      if(result.statusCode==200){
        yield put({
          type:"save",
          payload:{detailList:result.data}
        })
      }
    },
    //详情评论
    *detailComment({ payload }, { call, put }) {
      let result=yield call(detailComment,payload.id,payload.page);
      if(result.statusCode==200){
        yield put({
          type:"save",
          payload:{
            detailCommentnum:result.data[1],
            detailComment:result.data[0]
          }
        })
      }
    },
    //点击收藏
    *getLike({payload},{call,put}){
      let { type ,id} = payload;
      //发送收藏请求
      let result = yield call(like,payload);
      if(result.statusCode===200){
        //判断类型是添加还是删除
        if(type==="like"){
          //添加本地储存
          //判断本地是否有数据
          let LIKE = JSON.parse(localStorage?.LIKE??"[]");
          if(LIKE.length===0){
            //没有数据直接覆盖
            localStorage.LIKE=JSON.stringify([result.data.id]);
          }else{
            //有的话添加
            localStorage.LIKE=JSON.stringify([...LIKE,result.data.id]);
          }
        }else{
          //删除本地储存
          //判断本地是否有数据
          let LIKE = JSON.parse(localStorage?.LIKE??"[]");
          if(LIKE.length===0){
            //没有数据直接覆盖
            localStorage.removeItem("LIKE");
          }else{
            //有的话获取下标删除
            let index = LIKE.findIndex((item:string)=>item===id);
            LIKE.splice(index,1);
            localStorage.LIKE=JSON.stringify([...LIKE]);
          }
        }
        
        //更新数据
        yield put({
          type:"save",
          payload:{
            detailList:result.data
          }
        })
      }
    }
    //详情推荐文章
    // *detailRecommend({ payload }, { call, put }) {
    //   let result=yield call(detailRecommend,payload);
    //   console.log(result,"我丢")
      // if(result.statusCode==200){
      //   yield put({
      //     type:"save",
      //     payload:{detailList:result.data}
      //   })
      // }
    // }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  }
};

export default IndexModel;
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {about} from "@/services/module/about"
import {Types_About} from "@/types/about"
export interface AboutState {
    aboutData:Types_About
}
export interface Name_About {
    about:AboutState
}
export interface AboutType {
  namespace: 'about';
  state: AboutState;
  effects: {
    about: Effect;
  };
  reducers: {
    save: Reducer<AboutState>;
    // 启用 immer 之后
    
  };
}

const About: AboutType = {
  namespace: 'about',

  state: {
      aboutData:{}
  },

  effects: {
    *about({ payload }, { call, put }) {
        let result=yield call(about);
        console.log(result);
        if(result.statusCode==200){
             yield put({
                 type:"save",
                 payload:{aboutData:result.data}
             })
        }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  }
};

export default About;
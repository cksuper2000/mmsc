import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getclassify } from '@/services';
import { classifyli } from '@/types'
//设置接口
export interface ClassifyState {
    ClassifyList:Array<classifyli>
}

//仓库接口
interface ClassifyType {
  namespace: 'ClassifyList';
  state: ClassifyState;
  effects: {
    getclassify: Effect;
  }
  reducers: {
    save: Reducer<ClassifyState>;
  };
}
const ClassifyModel:ClassifyType = {
  namespace:"ClassifyList",
  state:{
    ClassifyList:[]
  },
  effects:{
    *getclassify({ payload }, { call, put}){
        let result=yield call(getclassify)
        console.log('...eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',result)
        if(result.statusCode===200){
            yield put({
                type:'save',
                payload:{ClassifyList:result.data}
            })
        }
       
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  }
}

export default ClassifyModel;
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getSerchData } from '@/services/module/keyword';
import { Ikeyword } from '@/types/keyword';
export interface IndexModelState {
  searchData: Ikeyword[];
}

export interface Keyword {
  keyword: IndexModelState;
}

export interface IndexModelType {
  namespace: 'keyword';
  state: IndexModelState;
  effects: {
    getSerchData: Effect;
  };
  reducers: {
    save: Reducer<IndexModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const IndexModel: IndexModelType = {
  namespace: 'keyword',

  state: {
    searchData: [],
  },

  effects: {
    *getSerchData({ payload }, { call, put }) {
      let result = yield call(getSerchData, payload.value);
      console.log(result);
      if (result.statusCode == 200) {
        yield put({
          type: 'save',
          payload: {
            searchData: result.data,
          },
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },
};

export default IndexModel;

import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { gettag } from '@/services';
import { tagtype,TagLi } from '@/types'
//设置接口
export interface TagListState {
  tagList:Array<tagtype>,
  page:number,
  pageSize:number,
  status:string
}

//仓库接口
interface TagListType {
  namespace: 'TagList';
  state: TagListState;
  effects: {
    gettag: Effect;
  }
  reducers: {
    save: Reducer<TagListState>;
  };
}
const TagListModel:TagListType = {
  namespace:"TagList",
  state:{
    tagList:[],
    page:1,
    pageSize:12,
    status:'publish'
  },
  effects:{
    *gettag({ payload }, { call, put,select}){
        console.log('2111111111111111111p',payload)
        // let {page,pageSize,status}:TagListState=yield select((state:TagLi)=>state.TagList)
        let result=yield call(gettag,payload)
        console.log('...aaaaaaaaaaaaaaaaaaaaaaaaaaa',result)
        if(result.statusCode==200){
            yield put({
                type:'save',
                payload:{tagList:result.data[0]}
            })
        }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  }
}

export default TagListModel;
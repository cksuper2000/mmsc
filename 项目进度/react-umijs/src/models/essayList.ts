import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getessay } from '@/services';
import { essayObject } from '@/types'
//设置接口
export interface EassayListState {
  essayList:Array<essayObject>
}

//仓库接口
interface EassayListType {
  namespace: 'eassayList';
  state: EassayListState;
  effects: {
    getessay: Effect;
  }
  reducers: {
    save: Reducer<EassayListState>;
  };
}
const EassayListModel:EassayListType = {
  namespace:"eassayList",
  state:{
    essayList:[]
  },
  effects:{
    *getessay({ payload }, { call, put}){
        let result=yield call(getessay)
        // console.log('...eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',result)
        if(result.statusCode==200){
            yield put({
                type:'save',
                payload:{essayList:result.data}
            })
        }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  }
}

export default EassayListModel;
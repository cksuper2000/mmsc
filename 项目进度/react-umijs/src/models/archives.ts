import { Effect, ImmerReducer, Reducer } from 'umi';
import { archives } from '@/services/module/archives';
import { ArchivesList } from '@/types/archives';

export interface ArchivesState {
  archivesList: ArchivesList;
}

export interface ArchivesType {
  namespace: 'archives';
  state: ArchivesState;
  effects: {
    archives: Effect;
  };
  reducers: {
    save: Reducer<ArchivesState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const IndexModel: ArchivesType = {
  namespace: 'archives',

  state: {
    archivesList: {},
  },

  effects: {
    *archives({ payload }, { call, put }) {
      let result = yield call(archives);
      console.log(result, '***');
      if (result.statusCode == 200) {
        yield put({
          type: 'save',
          payload: { archivesList: result.data },
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },
};

export default IndexModel;

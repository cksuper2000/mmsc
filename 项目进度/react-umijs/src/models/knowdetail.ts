import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { KnDetailtObject, Types_Msgboard } from '@/types/knowdetail';
import { knowDetail, knowreview } from '@/services/module/knowledgeDetail';

export interface IndexModelState {
    knowObject:Partial<KnDetailtObject>
    knowComment:Array<Types_Msgboard>
}



export interface IndexModelType {
  namespace: 'knowdetail';
  state: IndexModelState;
  effects: {
    knowDetail: Effect;
    knowreview:Effect
  };
  reducers: {
    save: Reducer<IndexModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const IndexModel: IndexModelType = {
  namespace: 'knowdetail',

  state: {
    knowObject:{},
    knowComment:[]
  },

  effects: {
    *knowDetail({ payload }, { call, put }) {
      let result = yield call(knowDetail, payload);
      if (result.statusCode == 200) {
        yield put({
          type: 'save',
          payload: {
            knowObject: result.data
          },
        });
      };
    },
    *knowreview({ payload }, { call, put }) {
      let result = yield call(knowreview, payload.id,payload.page);
      console.log(result,"芜湖起飞芜湖起飞芜湖起飞");
      if (result.statusCode == 200) {
        yield put({
          type: 'save',
          payload: {
            knowComment: result.data
          },
        });
      };
    }

  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },
};

export default IndexModel;
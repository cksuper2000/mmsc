import { Effect, ImmerReducer, Reducer } from 'umi';
import { Msgboard } from '@/services/module/msgboard';
import { IMsgboard } from '@/types/msgboard';

export interface MsgboardState {
  msgboardData: IMsgboard;
}

export interface Name_Msgboard {
    msgboard:MsgboardState
}
export interface MsgboardType {
  namespace: 'msgboard';
  state: MsgboardState;
  effects: {
    Msgboard: Effect;
  };
  reducers: {
    save: Reducer<MsgboardState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}

const IndexModel: MsgboardType = {
  namespace: 'msgboard',

  state: {
    msgboardData:{} 
  },

  effects: {
    *Msgboard({ payload }, { call, put }) {
      let result = yield call(Msgboard);
      console.log(result);
      if(result.statusCode==200){
          yield put({
              type:"save",
              payload:{msgboardData:result.data}
          })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },
};

export default IndexModel;

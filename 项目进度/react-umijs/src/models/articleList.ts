import { Effect, ImmerReducer, Reducer, Subscription, } from 'umi';
import { getArticleList ,getTitle} from '@/services';
import { Datum, IRootState ,Ititle} from '@/types'
//设置接口
export interface ArticleListState {
  articleList:Array<Datum>
  leng:number
  title:Array<Ititle>
  page: number,
  pageSize: number,
  status: string
  currIndex:number
}

//仓库接口
interface ArticleListType {
  namespace: 'articleList';
  state: ArticleListState;
  effects: {
    getList: Effect,
    getTitle:Effect
  }
  reducers: {
    save: Reducer<ArticleListState>;
  };
}
const ArticleListModel:ArticleListType = {
  namespace:"articleList",
  state:{
    articleList:[],
    leng:0,
    title:[],
    page: 1,
    pageSize: 12,
    status: "publish",
    currIndex:0
  },
  effects:{
    *getList({ payload }, { call, put ,select}){
      let {articleList,page,pageSize,status,currIndex,title}:ArticleListState = yield select((state:IRootState)=>state.articleList);
      if(payload) page=1
      //发送请求
      let result = yield call(getArticleList,{page,pageSize,status},payload);
      articleList = page===1?result.data[0]:[...articleList,...result.data[0]];
      if(result.statusCode===200){
        yield put({
          type:"save",
          payload:{
            articleList,
            leng:result.data[1],
            page:payload?1:page+1
          }
        })
      }
    },
    *getTitle({payload},{call,put,select}){
      //获取总的标题数据
      let {title} = yield select((state:IRootState)=>state.articleList);
      let result = title.length?title:yield call(getTitle);
      //有数据使用原来的数据
      if(title.length){
        let currIndex = title.findIndex((item:Ititle)=>item.value===payload);
        //更新数据
        yield put({
          type:"save",
          payload:{
            currIndex
          }
        })
      }else{
        if(result.statusCode===200){
          result.data.unshift({
            label:'全部',
            value:''
          });
          let currIndex = result.data.findIndex((item:Ititle)=>item.value===payload);
          yield put({
            type:"save",
            payload:{
              title:result.data,
              currIndex
            }
          });
        }
      }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  }
}

export default ArticleListModel;
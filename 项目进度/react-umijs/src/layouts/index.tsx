import React, { useContext, useEffect, useRef, useState } from 'react'
import { IRouteComponentProps } from 'umi'
import Context from "@/utits/content"
import Header from "@/components/Header"
import Footer from '@/components/Footer'
import Backtop from '@/components/Top';
import styles from './index.less';
import "@/global.less";
export default function Layout({ children, location, route, history, match }: IRouteComponentProps) {
  let [slideNum, setslideNum] = useState(0); //设置初始值
  useEffect(()=>{
   document.body.onscroll = () => {
      setslideNum(slideNum => slideNum = Math.floor(document.documentElement.scrollTop));
    }; //设置滚动事件
    // return ()=>{}
  },[]);
  return (
    <div className={styles.main}>
      <nav className={styles.header}>
        <Header />
      </nav>
      <Context.Provider value={slideNum}>
      {children}
      </Context.Provider>
      
      <footer>
        <Footer/>
      </footer>
      <Backtop slide={slideNum}/>
      
    </div>
  )
}
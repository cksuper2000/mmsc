import {ArticleListState} from '@/models/articleList'
import {EassayListState} from '@/models/essayList'
import {KnowledgeState} from '@/models/knowledgeList'
import {ClassifyState} from '@/models/classifyList'
import {SwiperimgState} from '@/models/swiperimg'
import {TagListState} from '@/models/taglist'
import { IDetailModelState } from '@/models/iddetail'
import {KnowledgeidState} from '@/models/knowledgeid'
import { MsgboardState } from '@/models/msgboard'
export interface IRootState{
    articleList:ArticleListState
    Knowledgeid:KnowledgeidState
    msgboard:MsgboardState
}

export interface essaystate{
    eassayList:EassayListState
}

export interface knnwledgeList{
    knowledgeList:KnowledgeState
}

export interface Classify{
    ClassifyList:ClassifyState
}

export interface Swiperim{
    Swiperimg:SwiperimgState
}

export interface TagLi{
    TagList:TagListState
}
export interface Detail{
    detail:IDetailModelState
    
}

export interface Knowledid{
    Knowledgeid:KnowledgeidState
}

export interface IMsgboard {
  id?: string;
  cover?: any;
  name?: string;
  path?: string;
  order?: number;
  content?: string;
  html?: string;
  toc?: string;
  status?: string;
  publishAt?: string;
  views?: number;
  createAt?: string;
  updateAt?: string;
}

export type Types_Msgboard = Array<Array<Datum>|number>
export interface Datum {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: any;
  replyUserName?: any;
  replyUserEmail?: any;
  createAt: string;
  updateAt: string;
  children: Child[];
}

interface Child {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
  createAt: string;
  updateAt: string;
}
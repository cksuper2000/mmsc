export interface Datum {
  id: string;
  title: string;
  cover: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: Tag[];
  category?: Tag;
}

interface Tag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}

export interface knowledgeli{
  id: string,
  parentId: null
  order: number
  title: string
  cover: string
  summary: string
  content: null
  html: null
  toc: null
  status: string
  views: number
  likes: number
  isCommentable: boolean,
  publishAt: string,
  createAt: string,
  updateAt: string
}
//标题类型
export interface Ititle {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}

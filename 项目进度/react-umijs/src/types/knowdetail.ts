import { IndexModelState } from "@/models/knowdetail";

export interface KnDetailtObject {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}



export interface Know{
    knowdetail:IndexModelState
}

export type Types_Msgboard = Array<Array<Datum>|number>
export interface Datum {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: any;
  replyUserName?: any;
  replyUserEmail?: any;
  createAt: string;
  updateAt: string;
  children: Child[];
};

interface Child {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
  createAt: string;
  updateAt: string;
}


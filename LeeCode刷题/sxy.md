## 苏晓雨
- 8.11
  - [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)
  - [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)

- 8.12
  - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
  - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

- 8.13
  - [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
  - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)  

- 8.15
  - [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)  
  - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

- 8.16
  - [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
  - [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)  

- 8.17
  - [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)
  - [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

- 8.18
  - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
  - [最长公共前缀](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnmav1/)

- 8.20
  - [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)
  - [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)

- 8.23
  - [最大数](https://leetcode-cn.com/leetbook/read/top-interview-questions/xa1401/)  
  - [寻找重复数](https://leetcode-cn.com/leetbook/read/top-interview-questions/xabtn6/)

- 8.24
  - [寻找峰值](https://leetcode-cn.com/leetbook/read/top-interview-questions/xacqw5/)

- 8.26
  - [缺失数字](https://leetcode-cn.com/leetbook/read/top-interview-questions/x27sii/)
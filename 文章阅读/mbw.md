## 2021.8.11
 - 1.文章阅读
  - [JavaScript深拷贝和浅拷贝 及 JSON.parse(JSON.stringify()) 的缺陷](https://blog.csdn.net/m0_46846526/article/details/119513663?utm_medium=distribute.pc_category.none-task-blog-hot-3.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-3.nonecase)
  - [JavaScript 事件冒泡、事件捕获、事件委托](https://blog.csdn.net/m0_46846526/article/details/119548982)
## 2021.8.12
 - 1.文章阅读
  - [JavaScript知识——JS怎么写、变量、逻辑运算符](https://blog.csdn.net/xiaotangyu7dong/article/details/119518125?utm_medium=distribute.pc_category.none-task-blog-hot-8.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-8.nonecase)
  - [javascript——原型、原型对象、原型链](https://blog.csdn.net/h_jQuery/article/details/119570333?utm_medium=distribute.pc_category.none-task-blog-hot-5.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-5.nonecase)
## 2021.8.13
 - 1.文章阅读
  - [javascript 内置对象 数组(Array)一些属性方法](https://blog.csdn.net/weixin_45802907/article/details/119612486?utm_medium=distribute.pc_category.none-task-blog-hot-1.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-1.nonecase)
  - [javascript async awit 和 Promise 概述](https://blog.csdn.net/weixin_39753511/article/details/119581004?utm_medium=distribute.pc_category.none-task-blog-hot-9.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-9.nonecase)
## 2021.8.15
 - 1.文章阅读
  - [javascript——原型与原型链](https://blog.csdn.net/qq_40183760/article/details/116117954?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EOPENSEARCH%7Edefault-2.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EOPENSEARCH%7Edefault-2.control)
  - [javascript系列 ————变量声明提升（一）](https://blog.csdn.net/qq_27628085/article/details/84628181?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EOPENSEARCH%7Edefault-8.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EOPENSEARCH%7Edefault-8.control)
## 2021.8.16
 - 1.文章阅读
  - [JavaScript事件绑定进阶认识](https://blog.csdn.net/weixin_40981660/article/details/119696673?utm_medium=distribute.pc_category.none-task-blog-hot-4.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-4.nonecase)
  - [node中的ES6模块化](https://blog.csdn.net/weixin_43266406/article/details/119706575?utm_medium=distribute.pc_category.none-task-blog-hot-1.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-1.nonecase)
## 2021.8.17
 - 1.文章阅读
  - [<Javascript>浅谈js“三元表达式”](https://blog.csdn.net/liu_jiachen/article/details/73251172?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162924196016780255282107%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=162924196016780255282107&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-27-73251172.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
  - [Node.js与JavaScript的区别](https://blog.csdn.net/u014171091/article/details/91058302?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162924196016780255282107%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=162924196016780255282107&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-10-91058302.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
## 2021.8.18
 - 1.文章阅读
  - [最通俗易懂的JavaScript](https://blog.csdn.net/jiahuan_/article/details/105895421?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162932875616780271513486%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162932875616780271513486&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-5-105895421.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
  - [node.js和JavaScript的关系](https://blog.csdn.net/weixin_41160054/article/details/89509957?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162932875616780271537207%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=162932875616780271537207&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-17-89509957.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
## 2021.8.19
 - 1.文章阅读
  - [rem详解及使用方法](https://blog.csdn.net/qq_27674439/article/details/91490068?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162941468816780265463890%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162941468816780265463890&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-91490068.ecpm_v1_rank_v29&utm_term=rem&spm=1018.2226.3001.4187)
  - [CSS中的em运用详解](https://blog.csdn.net/jingru2017/article/details/79099464?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162941550416780274146698%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=162941550416780274146698&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v29_ecpm-8-79099464.ecpm_v1_rank_v29&utm_term=em&spm=1018.2226.3001.4187)
## 2021.8.20
 - 1.文章阅读
  - [react中阻止事件冒泡](https://blog.csdn.net/w799766/article/details/82591372?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162950245716780274187761%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=162950245716780274187761&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~rank_v29_ecpm-24-82591372.ecpm_v1_rank_v29&utm_term=react&spm=1018.2226.3001.4187)
  - [react 项目生成二维码](https://blog.csdn.net/shalihcy/article/details/108823894?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162950891116780269817709%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=162950891116780269817709&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~hot_rank-7-108823894.ecpm_v1_rank_v29&utm_term=react%E7%94%9F%E6%88%90%E4%BA%8C%E7%BB%B4%E7%A0%81&spm=1018.2226.3001.4187)
## 2021.8.22
 - 1.文章阅读
  - [Javascript：谈谈JS的全局变量跟局部变量](https://blog.csdn.net/zyz511919766/article/details/7276089?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162967736216780262528951%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=162967736216780262528951&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~hot_rank-11-7276089.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
  - [JavaScript清空数组](https://blog.csdn.net/eclipse9527/article/details/80384941?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162967736216780262528951%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=162967736216780262528951&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~hot_rank-10-80384941.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
## 2021.8.23
 - 1.文章阅读
  - [JavaScript进阶（一）JS事件机制](https://blog.csdn.net/qq_21046965/article/details/83785541?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162976174616780265493463%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=162976174616780265493463&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v29_ecpm-17-83785541.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
  - [React Hooks 常用钩子](https://blog.csdn.net/chenzhizhuo/article/details/104159910)
## 2021.8.24
 - 1.文章阅读
  - [javascript的宏任务和微任务](https://blog.csdn.net/lc237423551/article/details/79902106?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162984697916780274116067%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162984697916780274116067&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-79902106.ecpm_v1_rank_v29&utm_term=%E5%AE%8F%E4%BB%BB%E5%8A%A1%E5%92%8C%E5%BE%AE%E4%BB%BB%E5%8A%A1&spm=1018.2226.3001.4187)
  - [JavaScript 页面自动执行（加载）](https://blog.csdn.net/wzp6010625/article/details/53171604?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162984677116780265439407%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=162984677116780265439407&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~rank_v29_ecpm-28-53171604.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
## 2021.8.25
 - 1.文章阅读
  - [JavaScript的事件队列(Event Queue)](https://blog.csdn.net/weixin_34346099/article/details/91456564?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162987381316780265468769%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162987381316780265468769&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-1-91456564.ecpm_v1_rank_v29&utm_term=event+queue&spm=1018.2226.3001.4187)
## 2021.8.26
 - 1.文章阅读
  - [hooks的基本用法](https://blog.csdn.net/zhaileilei1/article/details/106327314?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163002205916780255228376%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163002205916780255228376&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-106327314.ecpm_v1_rank_v29&utm_term=hooks%27&spm=1018.2226.3001.4187)
## 2021.8.27
 - 1.文章阅读
  - [JS的七种内置类型（六种原始类型+引用类型）](https://blog.csdn.net/lilele0227/article/details/88210194?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163028209316780274186819%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=163028209316780274186819&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~times_rank-23-88210194.first_rank_v2_pc_rank_v29&utm_term=js&spm=1018.2226.3001.4449)
## 2021.8.29
 - 1.文章阅读
  - [javascript中的常量](https://blog.csdn.net/czh500/article/details/100059513?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163028178416780269834141%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=163028178416780269834141&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~times_rank-6-100059513.first_rank_v2_pc_rank_v29&utm_term=js&spm=1018.2226.3001.4449)
  - [前端路由的原理？两种实现方式有什么区别？](https://blog.csdn.net/weixin_43912756/article/details/107461229?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163028035316780357237967%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163028035316780357237967&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-1-107461229.first_rank_v2_pc_rank_v29&utm_term=%E8%B7%AF%E7%94%B1%E7%9A%84%E4%B8%A4%E7%A7%8D%E6%96%B9%E6%B3%95%E5%92%8C%E5%8C%BA%E5%88%AB&spm=1018.2226.3001.4449)
## 2021.8.30
 - 1.文章阅读
  - [js 数据类型 存储位置](https://blog.csdn.net/wang_gongzi/article/details/106819591?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163037304216780265453721%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=163037304216780265453721&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~times_rank-13-106819591.first_rank_v2_pc_rank_v29&utm_term=js%E5%BC%95%E7%94%A8%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B&spm=1018.2226.3001.4449)
## 2021.8.31
 - 1.文章阅读
  - [mobx在react hooks中的应用](https://blog.csdn.net/qq_36676237/article/details/105068520?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163036692716780261919303%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=163036692716780261919303&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~times_rank-1-105068520.first_rank_v2_pc_rank_v29&utm_term=mbox%2Bhooks&spm=1018.2226.3001.4449)
## 2021.9.1
 - 1.文章阅读
  - [Promise](https://blog.csdn.net/weixin_43638968/article/details/105258289?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163006333216780271535079%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163006333216780271535079&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-4-105258289.first_rank_v2_pc_rank_v29&utm_term=promise&spm=1018.2226.3001.4449)
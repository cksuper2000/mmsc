# 程凯
## 2021.9.8
  - 文章阅读
    - [默认465端口](https://blog.51cto.com/xiaogongju/2072089)
  - leecode刷题
    - [两数之和](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
    - [多数元素](https://leetcode-cn.com/problems/majority-element/)
  - 源码阅读
  - 项目进度
    - 编辑器完成
    - 发布文章
    - 系统设置
## 2021.9.1
  - 文章阅读
    - [防抖节流](https://www.cnblogs.com/momo798/p/9177767.html)
  - leecode刷题
    - [重复元素](https://leetcode-cn.com/problems/contains-duplicate/submissions/)
  - 源码阅读
  - 项目进度
    - 登陆注册
    - 表格的增删改查
## 2021.8.31
  - 文章阅读
    - [对象继承](https://www.cnblogs.com/pingzx/p/10654163.html)
    - [包装类](https://www.jianshu.com/p/a3292f1582e5)
  - leecode刷题
  - 源码阅读
    - [apply源码](https://www.jianshu.com/p/856bfa3a5e5d)
  - 项目进度
    - 文章列表
    - 标签列表
## 2021.8.30
  - 文章阅读
    - [什么是可枚举属性](https://blog.csdn.net/statham_li/article/details/78241002)
    - [属性描述符](http://c.biancheng.net/view/5775.html)
  - leecode刷题
  - 源码阅读
  - 项目进度
    - 组件的划分,登陆注册,退出登陆
    - 文章管理
    - 面包屑
    - 下拉菜单
## 2021.8.29
  - 文章阅读
    - [vue/react直接操作dom元素](https://blog.csdn.net/lqlqlq007/article/details/95175987)
    - [js创建对象的方法]
  - leecode刷题
  - 源码阅读
  - 项目进度
    - 左边布局的完成
    - 左边的样式
    - 源码阅读右边的面包屑
    - 页面路由的完成
## 2021.8.27
  - 文章阅读
    - [async/await原理](https://blog.csdn.net/xgangzai/article/details/106536325)
    - [generator使用](https://es6.ruanyifeng.com/#docs/generator)
  - leecode刷题
    - [计数质数](https://leetcode-cn.com/problems/count-primes/)
  - 源码阅读
  - 项目进度
    - 项目自动化
## 2021.8.26
  - 文章阅读
    [对象的代理](https://blog.csdn.net/cksuper2000/article/details/119939923?spm=1001.2014.3001.5501)
    [任务对列](https://blog.csdn.net/cksuper2000/article/details/119898076?spm=1001.2014.3001.5501)
  - leecode刷题
    [位1的个数](https://leetcode-cn.com/problems/number-of-1-bits/)
    [二进制相加](https://leetcode-cn.com/problems/add-binary/)
  - 源码阅读
    - promise源码阅读
    - router源码
  - 项目进度
## 2021.8.25
  - 文章阅读
    - [订阅发布模式]
    - [策略模式]
  - leecode刷题
    - [位1的数字](https://leetcode-cn.com/problems/number-of-1-bits/)
    - [快乐数](https://leetcode-cn.com/problems/happy-number/)
  - 源码阅读
    - vue的模板的渲染
    - vue的指令
  - 项目进度
    - [√]项目懒加载
    - [√]百度统计
## 2021.8.24
  - 文章阅读
    - [调用栈](https://segmentfault.com/a/1190000010360316)
    - [宏任务与微任务](https://blog.csdn.net/cksuper2000/article/details/119898076?spm=1001.2014.3001.5502)
  - leecode刷题
  - [多数元素](https://leetcode-cn.com/problems/majority-element/)
  - 源码阅读
    - react的usestate实现方式
    - 任务对列,异步加载
  - 项目进度
    - [√]打包上线
    - [√]拉到服务器
## 2021.8.23
- 文章阅读
  - [父组件内获取子组件ref](https://www.jianshu.com/p/b0d667c02a9a)
- leecode刷题
  - [两数之和||输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
  [只出现一次的数字](https://leetcode-cn.com/problems/single-number/)
- 源码阅读
- 项目进度
  - [√]页面的主题
  - [√]其他其他已完成
## 2021.8.21
- 文章阅读
  - [pc端改变滚动条样式](https://blog.csdn.net/HDdgut/article/details/112511443)
- leecode刷题
  - [验证回文字符](https://leetcode-cn.com/problems/valid-palindrome/)
- 源码阅读
- 项目
  - [√]留言功能完善,表情
  - [√]页面的响应式
## 2021.8.20
- 文章阅读
  - [rem](https://zhuanlan.zhihu.com/p/30413803)
  - [rem计算](https://www.cnblogs.com/xiaozhumaopao/p/8260447.html)
- leecode刷题
  - [加一](https://leetcode-cn.com/problems/plus-one/)
  - [合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array/)
- 源码阅读
- 项目
  - [√]国际化
  - [√]点赞收藏
## 2021.8.19
- 文章阅读
  - [迭代器](https://es6.ruanyifeng.com/#docs/iterator)
  - [!类型断言](https://blog.csdn.net/fredricen/article/details/109625269)
- leecode刷题
- 源码阅读
  - react使用diff算法渲染子节点
- 项目
  - [√]页面跳转详情
  - [√]详情页渲染,复制,大图
## 2021.8.18
- 文章阅读
  - [react页面渲染字符标签](https://www.cnblogs.com/videring/articles/7788174.html)
  - [antd使用技巧](https://www.jianshu.com/p/120fb4771aef)
- Leecode刷题
  -[最后一个单词长度](https://leetcode-cn.com/problems/length-of-last-word/)
- 项目
  - [√]关于页面完成
  - [√]留言板完成
  - [√]知识小册完成
- 源码阅读
  - diff算法渲染外层元素
## 2021.8.17
- 文章阅读
  - [解决跨域](https://jasonandjay.github.io/study/zh/book/Ajax%E7%AF%87.html#%E5%A6%82%E4%BD%95%E8%A7%A3%E5%86%B3%E8%B7%A8%E5%9F%9F%E9%97%AE%E9%A2%98)
  - [栅格布局](https://material-ui.com/zh/components/grid/#basic-grid)
- leecode刷题

- 项目
  - [√]留言组件分装
  - [√]知识小册渲染
  - [√]关于组件的渲染
- 源码阅读
  - 组件生命周期的实现
  - 组件的更新
## 2021.8.15
- 文章阅读
  - [pormise源码解析](https://blog.csdn.net/sinat_17775997/article/details/83376452)
  - [async await了解](https://blog.csdn.net/xgangzai/article/details/106536325)
- leecode刷题
- 项目
  - [x]列表分装筛选
  - [x]列表样式完成
  - [x]文章列表完成
- 源码阅读
  - react源码解析jsx虚拟dom
  - react组件的分装

## 2021.8.13
- 文章阅读
  - [阅读react源码](https://github.com/hujiulong/blog)
  - [阅读parceljs](https://www.parceljs.cn/)
- leecode刷题
  - [元素插入位置](https://leetcode-cn.com/problems/search-insert-position/)
  - [实现 strStr](https://leetcode-cn.com/problems/implement-strstr/)
- 项目
  - [x]头部导航栏
  - [x]下拉列表
- 项目源码
## 2021.8.12
- 文章阅读
  - [阅读material-ui](https://material-ui.com/zh/components/grid/#basic-grid);
  - [阅读react-hook](https://react.docschina.org/docs/hooks-intro.html)
- leecode刷题
  - [移除元素](https://leetcode-cn.com/problems/remove-element/)
  - [有效括号](https://leetcode-cn.com/problems/valid-parentheses/)
- 项目
## 2021.8.11
- 文章阅读
  - es6新增语法，链式判断符
- leecode刷题
  - 数组的去重,在不创建新的内存空间的情况下,我们要实现数组的去重;
- [删除有序数组重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
  - 求和:两个数,求数组中所有数的结果等于目标数的结果的两个值,并返回
- [两数之和](https://leetcode-cn.com/problems/two-sum/)
- 堆栈内容解析
- 项目进度
- 遇到的问题
# 苏晓雨
## 2021.8.30
- 文章阅读
  - [Promise](https://juejin.cn/post/6844903607968481287)
## 2021.8.26
- LeetCode刷题
  - [缺失数字](https://leetcode-cn.com/leetbook/read/top-interview-questions/x27sii/)
## 2021.8.24
- 文章阅读
  - [React Hooks(useState,useEffect)](https://juejin.cn/post/6844903709927800846)
- LeetCode刷题
  - [寻找峰值](https://leetcode-cn.com/leetbook/read/top-interview-questions/xacqw5/)
## 2021.8.23
- 文章阅读
  - [TypeScript 与 React 实战(组件篇上)](https://juejin.cn/book/6844733813021491207/section/6844733813134721037)
  - [高级类型之索引类型、映射类型](https://juejin.cn/book/6844733813021491207/section/6844733813138915335)
- leetCode刷题
  - [最大数](https://leetcode-cn.com/leetbook/read/top-interview-questions/xa1401/)  
  - [寻找重复数](https://leetcode-cn.com/leetbook/read/top-interview-questions/xabtn6/)
## 2021.8.20
- 文章阅读
  - [js javaScript object对象属性和所有方法及es6对象的新增方法](https://blog.csdn.net/pujun1201/article/details/119779042?utm_medium=distribute.pc_category.none-task-blog-hot-2.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-2.nonecase)
- leetCode刷题
  - [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)
  - [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)
## 2021.8.19
- 文章阅读
  - [TS 几个内置工具类型（Partial & Required & Readonly & Pick & Exclude & Extract & Omit）的使用](https://juejin.cn/post/6905928813452984327)
## 2021.8.18
- 文章阅读
  - [Javascript学习笔记](https://blog.csdn.net/weixin_46264622/article/details/119759769?utm_medium=distribute.pc_category.none-task-blog-hot-9.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-9.nonecase)
- LeetCode刷题
  - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
  - [最长公共前缀](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnmav1/)
## 2021.8.17
- 文章阅读
  - [什么是跨域？跨域解决方法](https://blog.csdn.net/qq_38128179/article/details/84956552?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162919976716780262559577%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162919976716780262559577&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-84956552.first_rank_v2_pc_rank_v29&utm_term=%E8%B7%A8%E5%9F%9F&spm=1018.2226.3001.4187)
  - [umi：根据umi-request开发文档封装请求参数携带请求头](https://blog.csdn.net/weixin_41753520/article/details/98317567?ops_request_misc=&request_id=&biz_id=102&utm_term=umi%E4%B8%8A%E4%BC%A0%E6%96%87%E4%BB%B6&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-9-.first_rank_v2_pc_rank_v29&spm=1018.2226.3001.4187)
- leetCode刷题
  - [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)
  - [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
## 2021.8.16
- 文章阅读
  - [浅谈前端开发过程中使用的代理方法](https://blog.csdn.net/qq_41114603/article/details/82528811?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162912411916780271583420%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162912411916780271583420&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-2-82528811.first_rank_v2_pc_rank_v29&utm_term=%E5%89%8D%E7%AB%AF%E4%BB%A3%E7%90%86&spm=1018.2226.3001.4187)
  - [js中字符串转为DOM对象和DOM对象转为字符串](https://blog.csdn.net/weixin_38098192/article/details/80235280?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162912550516780269854561%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162912550516780269854561&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-2-80235280.first_rank_v2_pc_rank_v29&utm_term=js%E5%AD%97%E7%AC%A6%E4%B8%B2%E8%BD%ACdom&spm=1018.2226.3001.4187) 
- LeetCode刷题
  - [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
  - [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/) 
## 2021.8.15
- 文章阅读
  - [深入理解js对象排序-sort()](https://blog.csdn.net/a546598185/article/details/80140374?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control)  
  - [JavaScript 中的迭代器](https://blog.csdn.net/m0_50855872/article/details/119535128?utm_medium=distribute.pc_category.none-task-blog-hot-15.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-15.nonecase)
  - [umi配合dva实现状态管理](https://blog.csdn.net/Leonardo_Zhu/article/details/96482371?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control)
- LeetCode刷题
  - [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)  
  - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
## 2021.8.13
- 文章阅读
  - [React + UmiJS: 实现简单登录鉴权](https://blog.csdn.net/tonydz0523/article/details/108099407?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162886444016780255217199%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162886444016780255217199&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-108099407.first_rank_v2_pc_rank_v29&utm_term=umijs&spm=1018.2226.3001.4187)   
  - [umiJS学习](https://blog.csdn.net/weixin_33768153/article/details/83148661?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162886444016780265452637%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=162886444016780265452637&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-13-83148661.first_rank_v2_pc_rank_v29&utm_term=umijs&spm=1018.2226.3001.4187)
  - [UmiJS的使用](https://blog.csdn.net/qq_41579104/article/details/98481720?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162886444016780265452637%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=162886444016780265452637&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-15-98481720.first_rank_v2_pc_rank_v29&utm_term=umijs&spm=1018.2226.3001.4187)
- LeetCode刷题
  - [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
  - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)  
- 项目源码
## 2021.8.12
- leetcode刷题
   - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
   - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
- 文章阅读
   - [JavaScript 事件冒泡、事件捕获、事件委托](https://blog.csdn.net/m0_46846526/article/details/119548982?utm_medium=distribute.pc_category.none-task-blog-hot-5.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-5.nonecase)
   - [JavaScript同步与异步](https://blog.csdn.net/DengZY926/article/details/119545447?utm_medium=distribute.pc_category.none-task-blog-hot-2.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-2.nonecase)
   - [Node.js 模块加载及包的管理](https://blog.csdn.net/weixin_45654582/article/details/119522603?utm_medium=distribute.pc_category.none-task-blog-hot-10.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-10.nonecase)
 ## 2021.8.11
 - leecode刷题
    - [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)
    - [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
- 文章阅读
     - [JavaScript深拷贝和浅拷贝 及 JSON.parse(JSON.stringify()) 的缺陷](https://blog.csdn.net/m0_46846526/article/details/119513663?utm_medium=distribute.pc_category.none-task-blog-hot-3.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-3.nonecase)
  - [JavaScript 二维数组转一维数组](https://blog.csdn.net/m0_46846526/article/details/119085567)
  - [JavaScript递归 经典案例题详解](https://blog.csdn.net/m0_46846526/article/details/118417014)
# 孟博文
## 2021.9.1
- leetcode刷题
  - [剑指 Offer 50. 第一个只出现一次的字符](https://leetcode-cn.com/problems/di-yi-ge-zhi-chu-xian-yi-ci-de-zi-fu-lcof/)
- 文章阅读
  - [Promise](https://blog.csdn.net/weixin_43638968/article/details/105258289?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163006333216780271535079%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163006333216780271535079&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-4-105258289.first_rank_v2_pc_rank_v29&utm_term=promise&spm=1018.2226.3001.4449)
## 2021.8.31
- leetcode刷题
  - [剑指 Offer 65. 不用加减乘除做加法](https://leetcode-cn.com/problems/bu-yong-jia-jian-cheng-chu-zuo-jia-fa-lcof/)
- 文章阅读
  - [mobx在react hooks中的应用](https://blog.csdn.net/qq_36676237/article/details/105068520?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163036692716780261919303%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=163036692716780261919303&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~times_rank-1-105068520.first_rank_v2_pc_rank_v29&utm_term=mbox%2Bhooks&spm=1018.2226.3001.4449)
## 2021.8.30
- leetcode刷题
  - [剑指 Offer 03. 数组中重复的数字](https://leetcode-cn.com/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof/)
- 文章阅读
  - [js 数据类型 存储位置](https://blog.csdn.net/wang_gongzi/article/details/106819591?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163037304216780265453721%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=163037304216780265453721&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~times_rank-13-106819591.first_rank_v2_pc_rank_v29&utm_term=js%E5%BC%95%E7%94%A8%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B&spm=1018.2226.3001.4449)
## 2021.8.29
- 文章阅读
  - [javascript中的常量](https://blog.csdn.net/czh500/article/details/100059513?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163028178416780269834141%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=163028178416780269834141&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~times_rank-6-100059513.first_rank_v2_pc_rank_v29&utm_term=js&spm=1018.2226.3001.4449)
  - [前端路由的原理？两种实现方式有什么区别？](https://blog.csdn.net/weixin_43912756/article/details/107461229?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163028035316780357237967%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163028035316780357237967&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-1-107461229.first_rank_v2_pc_rank_v29&utm_term=%E8%B7%AF%E7%94%B1%E7%9A%84%E4%B8%A4%E7%A7%8D%E6%96%B9%E6%B3%95%E5%92%8C%E5%8C%BA%E5%88%AB&spm=1018.2226.3001.4449)
## 2021.8.27
- 文章阅读
  - [javascript中的常量](https://blog.csdn.net/czh500/article/details/100059513?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163028178416780269834141%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=163028178416780269834141&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~times_rank-6-100059513.first_rank_v2_pc_rank_v29&utm_term=js&spm=1018.2226.3001.4449)
## 2021.8.26
- leetcode刷题
  - [各位相加](https://leetcode-cn.com/problems/add-digits/)
- 文章阅读
  - [hooks的基本用法](https://blog.csdn.net/zhaileilei1/article/details/106327314?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163002205916780255228376%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163002205916780255228376&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-106327314.ecpm_v1_rank_v29&utm_term=hooks%27&spm=1018.2226.3001.4187)
## 2021.8.25
- leetcode刷题
  - [剑指 Offer 11. 旋转数组的最小数字](https://leetcode-cn.com/problems/xuan-zhuan-shu-zu-de-zui-xiao-shu-zi-lcof/)
- 文章阅读
  - [JavaScript的事件队列(Event Queue)](https://blog.csdn.net/weixin_34346099/article/details/91456564?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162987381316780265468769%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162987381316780265468769&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-1-91456564.ecpm_v1_rank_v29&utm_term=event+queue&spm=1018.2226.3001.4187)
## 2021.8.24
- leetcode刷题
  - [有效的完全平方数](https://leetcode-cn.com/problems/valid-perfect-square/)
- 文章阅读
  - [javascript的宏任务和微任务](https://blog.csdn.net/lc237423551/article/details/79902106?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162984697916780274116067%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162984697916780274116067&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-79902106.ecpm_v1_rank_v29&utm_term=%E5%AE%8F%E4%BB%BB%E5%8A%A1%E5%92%8C%E5%BE%AE%E4%BB%BB%E5%8A%A1&spm=1018.2226.3001.4187)
  - [JavaScript 页面自动执行（加载）](https://blog.csdn.net/wzp6010625/article/details/53171604?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162984677116780265439407%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=162984677116780265439407&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~rank_v29_ecpm-28-53171604.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
## 2021.8.23
- leetcode刷题
  - [分发饼干](https://leetcode-cn.com/problems/assign-cookies/)
- 文章阅读
  - [JavaScript进阶（一）JS事件机制](https://blog.csdn.net/qq_21046965/article/details/83785541?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162976174616780265493463%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=162976174616780265493463&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v29_ecpm-17-83785541.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
  - [React Hooks 常用钩子](https://blog.csdn.net/chenzhizhuo/article/details/104159910)
## 2021.8.22
- leetcode刷题
  - [赎金信](https://leetcode-cn.com/problems/ransom-note/)
- 文章阅读
  - [Javascript：谈谈JS的全局变量跟局部变量](https://blog.csdn.net/zyz511919766/article/details/7276089?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162967736216780262528951%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=162967736216780262528951&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~hot_rank-11-7276089.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
  - [JavaScript清空数组](https://blog.csdn.net/eclipse9527/article/details/80384941?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162967736216780262528951%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=162967736216780262528951&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~hot_rank-10-80384941.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
 ## 2021.8.20
- leetcode刷题
  - [有效的括号](https://leetcode-cn.com/problems/valid-parentheses/)
- 文章阅读
  - [react中阻止事件冒泡](https://blog.csdn.net/w799766/article/details/82591372?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162950245716780274187761%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fnavwordall.%2522%257D&request_id=162950245716780274187761&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~navwordall~first_rank_ecpm_v1~rank_v29_ecpm-24-82591372.ecpm_v1_rank_v29&utm_term=react&spm=1018.2226.3001.4187)
  - [react 项目生成二维码](https://blog.csdn.net/shalihcy/article/details/108823894?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162950891116780269817709%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=162950891116780269817709&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~hot_rank-7-108823894.ecpm_v1_rank_v29&utm_term=react%E7%94%9F%E6%88%90%E4%BA%8C%E7%BB%B4%E7%A0%81&spm=1018.2226.3001.4187)
## 2021.8.19
- leetcode刷题
  - [丢失的数字](https://leetcode-cn.com/problems/missing-number/)
  - [Nim 游戏](https://leetcode-cn.com/problems/nim-game/)
- 文章阅读
  - [rem详解及使用方法](https://blog.csdn.net/qq_27674439/article/details/91490068?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162941468816780265463890%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162941468816780265463890&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-91490068.ecpm_v1_rank_v29&utm_term=rem&spm=1018.2226.3001.4187)
  - [CSS中的em运用详解](https://blog.csdn.net/jingru2017/article/details/79099464?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162941550416780274146698%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=162941550416780274146698&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v29_ecpm-8-79099464.ecpm_v1_rank_v29&utm_term=em&spm=1018.2226.3001.4187)
## 2021.8.18
- leetcode刷题
  - [一维数组的动态和](https://leetcode-cn.com/problems/running-sum-of-1d-array/)
  - [剑指 Offer 16. 数值的整数次方](https://leetcode-cn.com/problems/shu-zhi-de-zheng-shu-ci-fang-lcof/)
- 文章阅读
  - [最通俗易懂的JavaScript](https://blog.csdn.net/jiahuan_/article/details/105895421?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162932875616780271513486%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162932875616780271513486&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-5-105895421.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
  - [node.js和JavaScript的关系](https://blog.csdn.net/weixin_41160054/article/details/89509957?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162932875616780271537207%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=162932875616780271537207&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-17-89509957.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
## 2021.8.17
- leetcode刷题
  - [69. x 的平方根](https://leetcode-cn.com/problems/sqrtx/)
  - [剑指 Offer II 072. 求平方根](https://leetcode-cn.com/problems/jJ0w9p/)
- 文章阅读
  - [<Javascript>浅谈js“三元表达式”](https://blog.csdn.net/liu_jiachen/article/details/73251172?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162924196016780255282107%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=162924196016780255282107&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-27-73251172.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
  - [Node.js与JavaScript的区别](https://blog.csdn.net/u014171091/article/details/91058302?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162924196016780255282107%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=162924196016780255282107&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-10-91058302.ecpm_v1_rank_v29&utm_term=js&spm=1018.2226.3001.4187)
## 2021.8.16
- leetcode刷题
  - [最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
  - [实现 strStr](https://leetcode-cn.com/problems/implement-strstr/)
- 文章阅读
  - [JavaScript事件绑定进阶认识](https://blog.csdn.net/weixin_40981660/article/details/119696673?utm_medium=distribute.pc_category.none-task-blog-hot-4.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-4.nonecase)
  - [node中的ES6模块化](https://blog.csdn.net/weixin_43266406/article/details/119706575?utm_medium=distribute.pc_category.none-task-blog-hot-1.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-1.nonecase)
## 2021.8.15
- leetcode刷题
  - [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
  - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
- 文章阅读
  - [javascript——原型与原型链](https://blog.csdn.net/qq_40183760/article/details/116117954?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EOPENSEARCH%7Edefault-2.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EOPENSEARCH%7Edefault-2.control)
  - [javascript系列 ————变量声明提升（一）](https://blog.csdn.net/qq_27628085/article/details/84628181?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EOPENSEARCH%7Edefault-8.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EOPENSEARCH%7Edefault-8.control)
## 2021.8.13
- leetcode刷题
  - [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)
  - [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
- 文章阅读
  - [javascript 内置对象 数组(Array)一些属性方法](https://blog.csdn.net/weixin_45802907/article/details/119612486?utm_medium=distribute.pc_category.none-task-blog-hot-1.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-1.nonecase)
  - [javascript async awit 和 Promise 概述](https://blog.csdn.net/weixin_39753511/article/details/119581004?utm_medium=distribute.pc_category.none-task-blog-hot-9.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-9.nonecase)
## 2021.8.12
- leetcode刷题
  - [最大子序和](https://leetcode-cn.com/problems/maximum-subarray/)
  - [搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
  - [在排序数组中查找元素的第一个和最后一个位置](https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/)
- 文章阅读
  - [JavaScript知识——JS怎么写、变量、逻辑运算符](https://blog.csdn.net/xiaotangyu7dong/article/details/119518125?utm_medium=distribute.pc_category.none-task-blog-hot-8.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-8.nonecase)
  - [javascript——原型、原型对象、原型链](https://blog.csdn.net/h_jQuery/article/details/119570333?utm_medium=distribute.pc_category.none-task-blog-hot-5.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-5.nonecase)
## 2021.8.11
- leetcode刷题
  - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
  - [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
- 文章阅读
  - [JavaScript深拷贝和浅拷贝 及 JSON.parse(JSON.stringify()) 的缺陷](https://blog.csdn.net/m0_46846526/article/details/119513663?utm_medium=distribute.pc_category.none-task-blog-hot-3.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-3.nonecase)
 - [JavaScript 事件冒泡、事件捕获、事件委托](https://blog.csdn.net/m0_46846526/article/details/119548982)
 - [ES6面向对象与class类的基本语法](https://es6.ruanyifeng.com/#docs/class)
 # 孟令辉
## 2021.9.1
leetcode刷题
- [实现strStr()](https://leetcode-cn.com/problems/implement-strstr)
- [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)
-文章阅读
- [深浅拷贝](https://download.csdn.net/download/weixin_38587155/13131426?utm_medium=distribute.pc_relevant_t0.none-task-download-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control&depth_1-utm_source=distribute.pc_relevant_t0.none-task-download-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control)


## 2021.8.31
- leetcode刷题
- [搜索选择排序数组](https://leetcode-cn.com/problems/search-in-rotated-sorted-array)
- 文章阅读
- [深浅拷贝](https://download.csdn.net/download/weixin_38587155/13131426?utm_medium=distribute.pc_relevant_t0.none-task-download-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control&depth_1-utm_source=distribute.pc_relevant_t0.none-task-download-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-1.control)

## 2021.8.30
- leetcode刷题
- [数组中的第K个最大元素](https://leetcode-cn.com/problems/kth-largest-element-in-an-array)
- [删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array)
- 文章阅读
- [Jqury](https://www.runoob.com/jquery/jquery-css-classes.html)

## 2021.8.29
- leetcode刷题
- [面试题 10.05. 稀疏数组搜索](https://leetcode-cn.com/problems/sparse-array-search-lcci)
- [基于排列构建数组](https://leetcode-cn.com/problems/build-array-from-permutation)
- 文章阅读
- [大白话讲解promis(一)](https://www.cnblogs.com/lvdabao/p/es6-promise-1.html)

## 2021.8.27
- leetcode刷题
- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays)

- 文章阅读
- [浅谈深浅拷贝](https://www.baidu.com/link?url=lBeQwoT0BiIK3UMn4ql5Nqq0yehoLP9XHXbK2FrCWq-G3i-6Rehb9pcJFOi9wBNYR4YriZIr05iin-QAolLM7ks3CR-XNcIH1Vx-VMmPe9G&wd=&eqid=bba95dca00223baf00000002612c34c0)

## 2021.8.26
- leetcode刷题
- [两数之和-输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted)
- [两个数组的交集||](https://leetcode-cn.com/problems/intersection-of-two-arrays-ii)
- [数组串联](https://leetcode-cn.com/problems/concatenation-of-array)
- 文章阅读
- [路由优缺点](https://jasonandjay.github.io/study/zh/standard/Spa.html#hash%E6%A8%A1%E5%BC%8F)
- []

## 2021.8.25
- leetcode刷题
- [加一](https://leetcode-cn.com/problems/plus-one)
- 文章阅读
- [宏任务和微任务](https://www.jianshu.com/p/75107522813f)
## 2021.8.24
- leetcode刷题
- [实现strStr()](https://leetcode-cn.com/problems/implement-strstr)
- 文章阅读
- [promise](https://www.liaoxuefeng.com/wiki/1022910821149312/1023024413276544)

## 2021.8.23
- leetcode刷题
- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)
- 文章阅读
- [浅谈hooks](https://www.jianshu.com/p/89f2cf94a7c2)
 
## 2021.8.22
- leetcode刷题
- [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)
- 文章阅读
- [常用Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B8%B8%E7%94%A8hooks)


## 2021.8.20
- leetcode刷题
- [两个数组的交集||](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
- 文章阅读
- [rem](http://caibaojian.com/web-app-rem.html)
- 项目
- [x]详情楼层

## 2021.8.19
- leetcode刷题
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
- 文章阅读
- [rem](http://caibaojian.com/web-app-rem.html)
- 项目
- [x]详情

## 2021.8.18
- leetcode刷题
- [搜索插入位置](https://leetcode-cn.com/problems/search-insert-position)
- [移出元素](https://leetcode-cn.com/problems/remove-element)
- 文章阅读
- [阅读react-hook](https://react.docschina.org/docs/hooks-intro.html)

## 2021.8.16
- leetcode刷题
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)
- 文章阅读
- [跨域资源共享](http://www.ruanyifeng.com/blog/2016/04/cors.html)

## 2021.8.15
- leetcode刷题
- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
- 文章阅读
- [JavaScript异步函数async/await](https://juejin.cn/post/6996253288102363143)
- 项目
- [x]右边推荐列表

## 2021.8.13
- leetcode刷题
- [整数反转](https://leetcode-cn.com/problems/reverse-integer)
- [删除字符串使字符串变好](https://leetcode-cn.com/problems/delete-characters-to-make-fancy-string)
- 文章阅读
- [javascript——原型、原型对象、原型链](https://blog.csdn.net/h_jQuery/article/details/119570333?utm_medium=distribute.pc_category.none-task-blog-hot-5.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-5.nonecase)

## 2021.8.12
- leetcode刷题
  - [回文数](https://leetcode-cn.com/problems/palindrome-number)
  - [最长公共前缀](https://leetcode-cn.com/problems/longest-common-prefix)
  - [删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array)
- 文章阅读
  - [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)

## 2021.8.11
- leetcode刷题
 - [两数之合](https://leetcode-cn.com/problems/two-sum/)
- 文章阅读
 - [ES6面向对象与class类的基本语法](https://es6.ruanyifeng.com/#docs/class)














